Shader "Effects/DistortNormal"
{
	Properties
	{
		_MainTex("Normalmap", 2D) = "bump" {}
		_Color("Main Color", Color) = (1,1,1,1)
		_Intensity("Intensity", Float) = 1.0
		_Threshold("Normal Threshold", Float) = 0.0
	}

	SubShader
	{
		Tags
		{
			"Queue" = "Transparent"
			"RenderType" = "Transparent"
		}

		LOD 100
		Cull Off
		Lighting Off
		ZWrite On
		Blend SrcAlpha OneMinusSrcAlpha

		GrabPass
		{
			"_BackgroundTex"
		}

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
				float4 color : COLOR;
			};

			struct v2f
			{
				float4 vertex   : SV_POSITION;
				fixed4 color : COLOR;
				float2 uv : TEXCOORD0;
				float4 grabUv : TEXCOORD1;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			sampler2D _BackgroundTex;
			uniform float4 _BackgroundTex_TexelSize;
			float _Intensity;
			float4 _Color;
			float _Threshold;

			v2f vert(appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				o.grabUv = ComputeGrabScreenPos(o.vertex);
				o.color = v.color * _Color;
				return o;
			}

			fixed4 SampleBackgroundTexture(float2 uv, float4 grabUv)
			{
				half2 normal = UnpackNormal(tex2D(_MainTex, uv));

				if(normal.x > -_Threshold && normal.x < _Threshold && normal.y > -_Threshold && normal.y < _Threshold)
					return fixed4(0, 0, 0, 0);

				normal *= _Intensity * 100 * _BackgroundTex_TexelSize.xy;

				fixed4 c = tex2Dproj(_BackgroundTex, grabUv + fixed4(normal, 0, 0));
				return c;
			}

			fixed4 frag(v2f i) : SV_Target
			{
				fixed4 c = SampleBackgroundTexture(i.uv, i.grabUv);
				c = c * i.color;

				return c;
			}
			ENDCG
		}
	}
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shockwave
{
	[ExecuteInEditMode]
	[RequireComponent(typeof(MeshRenderer))]
	public class ShockwaveEffect: MonoBehaviour
	{
		private MeshRenderer m_MeshRenderer = null;
		private MeshRenderer meshRenderer {
			get {
				if(m_MeshRenderer == null)
					m_MeshRenderer = GetComponent<MeshRenderer>();
				return m_MeshRenderer;
			}
		}

		private SpriteRenderer m_SpriteRenderer = null;
		private SpriteRenderer spriteRenderer {
			get {
				if(m_SpriteRenderer == null)
					m_SpriteRenderer = GetComponent<SpriteRenderer>();
				return m_SpriteRenderer;
			}
		}

		public float intensity = 3f;
		public Color color = Color.white;
		public float normalThreshold = 0f;


		public Texture2D normalMap = null;
		private Material m_Material = null;

		[UnityToolbag.SortingLayer]
		[SerializeField]
		private int m_SortingLayer = 0;
		[SerializeField]
		private int m_OrderInLayer = 0;

		void OnEnable()
		{
			Initialize();
		}

		void Update()
		{
			if(meshRenderer != null)
			{
				meshRenderer.sortingLayerID = m_SortingLayer;
				meshRenderer.sortingOrder = m_OrderInLayer;
			}

			if(m_Material != null)
			{
				if(meshRenderer.sharedMaterial != m_Material)
					meshRenderer.sharedMaterial = m_Material;

				m_Material.SetTexture("_MainTex", normalMap);
				m_Material.SetFloat("_Intensity", intensity);
				m_Material.SetColor("_Color", color);
				m_Material.SetFloat("_Threshold", normalThreshold);
			}
		}

		void Initialize()
		{
			if(meshRenderer != null)
			{
				Shader shader = Shader.Find("Effects/DistortNormal");
				m_Material = new Material(shader);
				meshRenderer.sharedMaterial = m_Material;
			}
		}
	}
}
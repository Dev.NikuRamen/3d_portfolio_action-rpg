using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Shockwave;

namespace Shockwave.Example
{
	public class Firewave: MonoBehaviour
	{
		[SerializeField]
		private float startScale = 0f;
		[SerializeField]
		private float endScale = 1f;
		[SerializeField]
		private float intensity = 0.5f;
		[SerializeField]
		private float duration = 0.5f;

		private float _currentTime = 0f;
		private ShockwaveEffect _shockwaveEffect;
		private Transform _viewCameraTransform;

		private void Awake()
		{
			_shockwaveEffect = GetComponent<ShockwaveEffect>();
			_viewCameraTransform = Camera.main.transform;
		}

		private void OnEnable()
		{
			transform.localScale = Vector3.one * startScale;
		}

		private void Update()
		{
			if ( _currentTime >= duration )
			{
				_currentTime = 0;
				gameObject.SetActive(false);
			}

			transform.LookAt(_viewCameraTransform);
			_currentTime += Time.deltaTime;

			var factor = Mathf.Clamp01(_currentTime / duration);
			transform.localScale = Vector3.one * Mathf.Lerp(startScale, endScale, factor);

			_shockwaveEffect.intensity = Mathf.Sin(factor * Mathf.PI) * intensity;
		}
	}
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shockwave.Example
{
	public class UserController: MonoBehaviour
	{
		[SerializeField]
		private GameObject m_Energyshield = null;
		[SerializeField]
		private GameObject m_DistortEffect = null;

		void Update()
		{
			if(Input.GetKeyUp(KeyCode.Alpha1))
				m_Energyshield.SetActive(!m_Energyshield.activeSelf);

			if(Input.GetKeyUp(KeyCode.Alpha2))
				m_DistortEffect.SetActive(!m_DistortEffect.activeSelf);
		}
	}
}
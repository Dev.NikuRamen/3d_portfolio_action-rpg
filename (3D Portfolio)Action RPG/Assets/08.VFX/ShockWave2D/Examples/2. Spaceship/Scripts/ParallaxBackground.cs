using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shockwave.Example
{
	public class ParallaxBackground: MonoBehaviour
	{
		[SerializeField]
		private float m_Speeed = 0;
		[SerializeField]
		private List<SpriteRenderer> m_Sprites = new List<SpriteRenderer>();

		private SpriteRenderer m_SpriteRenderer = null;
		public SpriteRenderer spriteRenderer {
			get {
				if(m_SpriteRenderer == null)
					m_SpriteRenderer = GetComponent<SpriteRenderer>();

				return m_SpriteRenderer;
			}
		}

		void LateUpdate()
		{
			transform.Translate(0, -m_Speeed * Time.deltaTime, 0);

			if(m_Sprites[0].transform.position.y < -9f)
			{
				SpriteRenderer sprite = m_Sprites[0];
				m_Sprites.RemoveAt(0);

				Vector3 lastPosition = m_Sprites[m_Sprites.Count - 1].transform.position;
				Vector3 position = lastPosition + new Vector3(0f, 4.8f, 0f);

				sprite.transform.position = position;
				m_Sprites.Add(sprite);
			}
		}
	}
}
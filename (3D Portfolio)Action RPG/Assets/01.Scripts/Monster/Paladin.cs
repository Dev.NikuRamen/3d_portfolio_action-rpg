﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using Random = UnityEngine.Random;

public class Paladin : Monster
{
    /// <summary>
    /// Animation Parameter
    /// </summary>
    private enum AnimationName
    {
        IsBattle,
        Hit_int,
        Hit,
        HitEnd,
        DifensiveMove,
        DifensiveMove_int,
        IsGuard,
        Guard,
        Block,
        Parrying,
        Evade_int,
        Evasion,
        Step_int,
        Step,
        IsMove,
        Attack,
        Attack1,
        Attack2,
        Attack3,
        AttackEnd,
        DashBack,
        Die
    }

    [Header("SFX Audio Clip")]
    
    [SerializeField]
    private ParryingFx parryingFx;
    [SerializeField]
    private AudioClip parryingSfx;
    [SerializeField]
    private AudioClip guardingSfx;
    [SerializeField]
    private AudioClip evasionSfx;

    [SerializeField] [Range(0,100)]
    private float parryingPercentage;
    // ------------------------------------- //
    private bool _isGuarding;
    private Queue<int> _attackQueue = new Queue<int>();
    private StringBuilder _filePath = new StringBuilder();
    private const string _SFX_FILE_PATH = "SFX/Monster";

    #region Game Process

    protected override void OnAwake()
    {
        StateData = XMLController.LoadData("OneHandSwordData", "Status", StatusName);
        base.OnAwake();
    }

    private void Start()
    {
        EnqueueAttackQueue();
        hpProgressBar.gameObject.SetActive(false);
    }

    protected override void OnUpdate()
    {
        base.OnUpdate();
        switch( state )
        {
            case State.Idle:
                if ( GetDistanceBetweenPlayer() < 10 )
                {
                    SetAiState(AiState.Offensive);
                    SetState(State.Battle);
                    hpProgressBar.gameObject.SetActive(true);
                    Animator.SetBool(nameof( AnimationName.IsBattle ), true);
                    Level.Instance.OnBattleStart();
                }
                break;
            case State.Battle:
                if ( !IsBattle )
                {
                    IsBattle = true;
                }

                break;
            case State.Attack:
                if ( Animator.GetCurrentAnimatorStateInfo(0)
                             .IsName("03_Frank_ActionRPG_Sword_Combat_Idle") ) 
            {
                SetState(State.Battle);
                SetAiState(AiState.Offensive);
            }

                if ( aiState != AiState.Attacking )
                {
                    SetAiState(AiState.Attacking);
                }
                break;
            case State.Evasion:
                break;
            case State.Hit:
                break;
            case State.Die:
                SetAiState(AiState.Nothing);
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }
    protected override void AiProcess()
    {
        var playerPos = PlayerController.transform.position;
        switch( aiState )
        {
            // Guarding 
            case AiState.Defensive:
                if ( !_isGuarding )
                {
                    _isGuarding = true;
                    // Guard
                    Animator.SetTrigger(nameof(AnimationName.Guard));
                    Animator.SetBool(nameof(AnimationName.IsGuard),true);
                    NavMeshAgent.isStopped = true;
                }
                LookAtTarget(playerPos);
                SetState(State.Evasion);
                break;
            // Ai do nothing
            case AiState.Nothing:
                _isGuarding = false;
                Animator.SetInteger(nameof( AnimationName.DifensiveMove_int ), 0);
                Animator.SetBool(nameof( AnimationName.IsGuard ), false);
                Animator.SetBool(nameof(AnimationName.IsMove),false);
                if ( GetDistanceBetweenPlayer() < 4 )
                {
                    SetAiState(AiState.Offensive);
                }
                break;
            // Trace player, when close with player, attack
            case AiState.Offensive:
                if ( _isGuarding )
                {
                    _isGuarding = false;
                    Animator.SetBool(nameof( AnimationName.IsGuard ), false);
                }
                NavMeshAgent.isStopped = false;

                NavMeshAgent.SetDestination(PlayerController.transform.position);
                Animator.SetBool(nameof( AnimationName.IsMove ), true);
                LookAtTarget(playerPos);
                if ( GetDistanceBetweenPlayer() < 1.5f )
                {
                    NavMeshAgent.isStopped = true;
                    Animator.SetBool(nameof( AnimationName.IsMove ), false);
                    if ( _attackQueue.Count <= 0 )
                    {
                        EnqueueAttackQueue();
                    }

                    AttackStart(nameof( HumanDataManager.DataKeyName.OneHandSword )
                                ,_attackQueue.Dequeue());
                    Animator.SetInteger(nameof( AnimationName.DifensiveMove_int ), 0);
                    SetAiState(AiState.Attacking);
                    SetState(State.Attack);
                }
                break;
            case AiState.Attacking:
                if ( _isGuarding )
                {
                    _isGuarding = false;
                    Animator.SetBool(nameof( AnimationName.IsGuard ), false);
                }
                SetState(State.Attack);
                NavMeshAgent.isStopped = true;
                Animator.SetInteger(nameof( AnimationName.DifensiveMove_int ), 0);
                Animator.SetBool(nameof( AnimationName.IsMove ), false);
                break;
            case AiState.MAX:
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }
    
    // End of Game Process
    #endregion
    
    
    #region Event Method

    /// <summary>
    /// When humanoid hit, Play Action.
    /// </summary>
    /// <param name="attackerTransform_">Attacker position</param>
    /// <param name="attackIndex_">Attack animation index</param>
    /// <param name="damage_">Get damage value</param>
    public override void OnHit(Transform attackerTransform_, int attackIndex_,float damage_)
    {
        base.OnHit(attackerTransform_, attackIndex_,damage_);
        NavMeshAgent.isStopped = true;
        if ( state == State.Hit )
        {
            SetAiState(AiState.Nothing);
            SetState(State.Hit);
            Animator.SetTrigger(nameof( AnimationName.Hit ));
            Animator.SetBool(nameof( AnimationName.IsBattle ), IsBattle);
            AudioSource.PlayOneShot(hitSfx);
            var data = HumanDataManager.Instance.GetData(nameof( HumanDataManager.DataKeyName.Player ),
                                                          attackIndex_.ToString());
            
            if ( int.Parse(data.isKnockDown) != 0 )
            {
                // If Is Knock down
                Animator.SetInteger(nameof( AnimationName.Hit_int ), (int) HitIndex.FallDown);
                AudioSource.PlayOneShot(fallDonwClip);
            }
            else
            {
                Animator.SetInteger(nameof( AnimationName.Hit_int ), (int) GetDirectionAsTarget(attackerTransform_));
                AudioSource.PlayOneShot(hitSfx);
            }
        }
    }

    protected override void OnHitEnd()
    {
        base.OnHitEnd();
        _isGuarding = false;
        NavMeshAgent.isStopped = false;
        Animator.SetInteger(nameof(AnimationName.Hit_int),0);
        SetState(State.Battle);
        if ( Util.IsSuccessInRandom(40) )
        {
            Animator.SetTrigger(nameof( AnimationName.DashBack ));
            SetAiState(AiState.Defensive);
        }
        else
        {
            Animator.SetTrigger(nameof( AnimationName.HitEnd ));
            SetAiState(AiState.Offensive);
        }
    }


    public override void OnAttackerAttackStart()
    {
        if(state == State.Die)
            return;
        if ( Util.IsSuccessInRandom(15) )
        {
            _isGuarding = true;
            Animator.SetTrigger(nameof( AnimationName.Guard ));
            Animator.SetBool(nameof( AnimationName.IsGuard ), _isGuarding);
            SetAiState(AiState.Nothing);
            SetState(State.Evasion);
        }
    }

    public override void OnEvade(Vector3 hitPosition_)
    {
        SetAiState(AiState.Defensive);
        SetState(State.Evasion);
        if ( _isGuarding )
        {
            if ( Util.IsSuccessInRandom(parryingPercentage) )
            {
                Animator.SetTrigger(nameof(AnimationName.Parrying));
                AudioSource.PlayOneShot(parryingSfx);
            }
            else
            {
                Animator.SetTrigger(nameof( AnimationName.Block ));
                EffectManager.Instance.Create(nameof( EffectManager.Effects.Guard ),
                                              hitPosition_,
                                              Quaternion.LookRotation(transform.forward),
                                              Vector3.one, .3f);
                AudioSource.PlayOneShot(guardingSfx);
            }
        }
        else
        {
            Animator.SetTrigger(nameof(AnimationName.Evasion));
            Animator.SetInteger(nameof(AnimationName.Evade_int),Random.Range(1,4));
            AudioSource.PlayOneShot(evasionSfx);
        }
    }

    protected override void OnEvasionFinish()
    {
        base.OnEvasionFinish();
        SetState(State.Attack);
        SetAiState(AiState.Offensive);
    }

    public override void OnEscape()
    {
        Animator.SetTrigger(nameof(AnimationName.Evasion));
        Animator.SetInteger(nameof(AnimationName.Evade_int),Random.Range(1,4));
    }

    protected override void OnAttackStart(int attackIndex_)
    {
        base.OnAttackStart(attackIndex_);
        var data = HumanDataManager.Instance.GetData(nameof( HumanDataManager.DataKeyName.OneHandSword ), attackIndex_.ToString());
        if ( !AttackAudioClips.ContainsKey(data.attackSFX) )
        {
            _filePath.Append(_SFX_FILE_PATH + "/Attack/" + data.attackSFX);
            var clip = Resources.Load<AudioClip>(_filePath.ToString());
            AttackAudioClips.Add(data.attackSFX,clip);
            _filePath.Clear();
        }
        AudioSource.PlayOneShot(AttackAudioClips[data.attackSFX]);
    }

    protected override void OnAttack()
    {
        AttackProcess(nameof( HumanDataManager.DataKeyName.OneHandSword ));
        NavMeshAgent.isStopped = true;
    }

    protected override void OnAttackFinish()
    {
        base.OnAttackFinish();
        Animator.SetTrigger(nameof(AnimationName.AttackEnd));
        SetAiState(AiState.Offensive);
    }
    
    private void OnParrying()
    {
        parryingFx.gameObject.SetActive(true);
        StartCoroutine(GameManager.Instance.StartSlowMotionTime(0.3f, .6f));
    }

    // End of Event Method
    #endregion

    #region Public Method

    
    // End of Public Method
    #endregion

    #region Protected Method

    protected override void SetDie()
    {
        base.SetDie();
        // Stop BGM
        AudioSource.PlayOneShot(dieSfx);
        SetAiState(AiState.Nothing);
        Animator.SetTrigger(nameof(AnimationName.Die));
        StartCoroutine(StartDieCutScene());
    }

    protected override void AttackStart(string attackerName_, int attackIndex_)
    {
        switch( attackIndex_ )
        {
            case 1:
                Animator.SetTrigger(nameof(AnimationName.Attack1));
                break;
            case 11:
                Animator.SetTrigger(nameof(AnimationName.Attack2));
                break;
            case 21:
                Animator.SetTrigger(nameof(AnimationName.Attack3));
                break;
        }
    }

    // End of Protected Method
    #endregion

    #region Private Method

    private IEnumerator StartDieCutScene()
    {
        yield return Util.YieldInstructionOnCache.WaitForSeconds(3f);
        yield return StartCoroutine(GameManager.Instance.getDefeatui.StartShowUi());
        StartCoroutine(PlayerController.StartOnCutScene(
                           () =>actionCamera.ActiveCutScene(
                               nameof(ActionCamera.CutSceneIndex.OnMonsterDie_CutScene))));
    }
    
    /// <summary>
    /// Queueing Ai attack index
    /// </summary>
    /// <param name="enqueueCount_"></param>
    private void EnqueueAttackQueue(int enqueueCount_ = 20)
    {
        for( var i = 0; i < enqueueCount_; i++ )
        {
            var rand = Random.Range(0, 3);
            switch( rand )
            {
                case 0:
                    _attackQueue.Enqueue(1);
                    break;
                case 1:
                    _attackQueue.Enqueue(11);
                    break;
                case 2:
                    _attackQueue.Enqueue(21);
                    break;
            }
        }
    }
    
    // End of Private Method
    #endregion
}

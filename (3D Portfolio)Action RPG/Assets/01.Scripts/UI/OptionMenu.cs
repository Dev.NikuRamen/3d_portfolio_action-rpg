﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OptionMenu : MonoBehaviour
{
    [SerializeField]
    private LobbyMenu lobbyMenu;

    private void Update()
    {
#if UNITY_EDITOR || PLATFORM_STANDALONE
	    if ( Input.GetButtonDown("Cancel") )
	    {
		    SetDisable();
	    }
#endif
    }
	
    /// <summary>
    /// Disable option menu
    /// </summary>
    public void SetDisable()
    {
	    lobbyMenu.gameObject.SetActive(true);
	    gameObject.SetActive(false);
    }
}

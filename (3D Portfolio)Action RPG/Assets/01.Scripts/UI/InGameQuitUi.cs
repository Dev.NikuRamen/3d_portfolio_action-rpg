﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InGameQuitUi : MonoBehaviour
{
    private void Start()
    {
        gameObject.SetActive(false);
    }

    public void OnExitButtonDown()
    {
        StartCoroutine(GameManager.Instance.StartLoadAsync(Loading.SceneName.StartScene));
    }

    public void OnCancelButtonDown()
    {
        gameObject.SetActive(false);   
    }
}

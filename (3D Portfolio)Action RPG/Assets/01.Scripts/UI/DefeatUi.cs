﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DefeatUi : MonoBehaviour
{
    [SerializeField]
    private Image backGroundImage;
    [SerializeField]
    private Image middleGroundImage;
    [SerializeField]
    private Image frontImage;

    [SerializeField] [Range(0, 2f)]
    private float alphaDamp;
    [SerializeField] [Range(0,1f)]
    private float backGroundAlpha;
    [SerializeField] [Range(0,1f)]
    private float middleGroundAlpha;
    // -------------------------- //

    /// <summary>
    /// Reset ui parameter
    /// </summary>
    public void ResetUi()
    {
        var backColor = backGroundImage.color;
        backColor.a = 0f;
        backGroundImage.color = backColor;
        var middleColor = middleGroundImage.color;
        middleColor.a = 0f;
        middleGroundImage.color = middleColor;
        var frontColor = frontImage.color;
        frontColor.a = 0f;
        frontImage.color = frontColor;
        gameObject.SetActive(false);
    }

    
    /// <summary>
    /// Show ui with fade - in process
    /// </summary>
    /// <returns></returns>
    public IEnumerator StartShowUi()
    {
        gameObject.SetActive(true);
        var backColor = backGroundImage.color;
        var middleColor = middleGroundImage.color;
        var frontColor = frontImage.color;
        while( backColor.a <= backGroundAlpha || middleColor.a <= middleGroundAlpha || frontColor.a <= 1f )
        {
            var delta = Time.deltaTime * alphaDamp;
            if ( backColor.a <= backGroundAlpha )
            {
                backColor.a += delta;
            }

            if ( middleColor.a <= middleGroundAlpha )
            {
                middleColor.a += delta;
            }

            frontColor.a += delta;
            backGroundImage.color = backColor;
            middleGroundImage.color = middleColor;
            frontImage.color = frontColor;
            yield return Util.YieldInstructionOnCache.WaitForEndOfFrame;
        }

        yield return Util.YieldInstructionOnCache.WaitForSeconds(3f);
        
        while( backColor.a >= 0 || middleColor.a >= 0 || frontColor.a>= 0f )
        {
            var delta = Time.deltaTime * alphaDamp;
            backColor.a -= delta;
            middleColor.a -= delta;
            frontColor.a -= delta;
            backGroundImage.color = backColor;
            middleGroundImage.color = middleColor;
            frontImage.color = frontColor;
            yield return Util.YieldInstructionOnCache.WaitForEndOfFrame;
        }
        ResetUi();
    }
}

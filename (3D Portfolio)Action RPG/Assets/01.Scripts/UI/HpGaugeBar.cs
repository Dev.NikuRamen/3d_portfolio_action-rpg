﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;
using UnityEngine.UI;

public class HpGaugeBar : MonoBehaviour
{
    public enum HpGaugeType
    {
        Player,
        Monster
    }
    
    
    [SerializeField]
    private Text remainHpText;
    [SerializeField]
    private Text nameText;
    [SerializeField]
    private Image dampGaugeImage;

    [SerializeField][Range(0,2f)]
    private float reduceDamp;

    public HpGaugeType gaugeType = HpGaugeType.Monster;
    // --------------------------------------------------- //
    private IEnumerator _dampReduceCoroutine;
    private UIProgressBar _hpGaugeBar;
    private float _remainHp;
    private float _beforeHp;
    private float _maxHp;
    
    /// <summary>
    /// Initialize hp Gauge
    /// </summary>
    /// <param name="startHp_">Start hp</param>
    /// <param name="maxHp_">Max hp</param>
    /// <param name="humanName_">Human Name</param>
    public void SetHpGauge(float startHp_,float maxHp_,string humanName_ = null)
    {
        _hpGaugeBar = GetComponent<UIProgressBar>();
        if ( humanName_ != null )
        {
            nameText.text = humanName_;
        }

        _remainHp = startHp_;
        _beforeHp = _remainHp;
        _maxHp = maxHp_;
        _hpGaugeBar.fillAmount = 1f;
        dampGaugeImage.fillAmount = 1f;
        if(remainHpText)
            remainHpText.text = _remainHp.ToString(CultureInfo.InvariantCulture);
        _hpGaugeBar.targetImage.fillAmount = 1f;
    }
    
    /// <summary>
    /// When hp value is change, Change ui
    /// </summary>
    /// <param name="amount_">Hp changed value amount</param>
    public void OnValueChange(int amount_)
    {
        if ( _dampReduceCoroutine != null )
        {
            StopCoroutine(_dampReduceCoroutine); 
        }
        _beforeHp = _remainHp;
        _remainHp += amount_;
        if ( _remainHp < 0 )
            _remainHp = 0;
        _dampReduceCoroutine = StartDampReduce();
        StartCoroutine(_dampReduceCoroutine);
        _hpGaugeBar.fillAmount = _remainHp / _maxHp;
        if(remainHpText)
            remainHpText.text = _remainHp.ToString(CultureInfo.InvariantCulture);
    }
    
    /// <summary>
    /// Start coroutine damp hp bar change 
    /// </summary>
    private IEnumerator StartDampReduce()
    {
        var remain = _remainHp;
        var currentValue = _beforeHp;
        yield return Util.YieldInstructionOnCache.WaitForSeconds(2);
        while( currentValue > remain )
        {
            currentValue -= Time.deltaTime * reduceDamp;
            dampGaugeImage.fillAmount = currentValue / _maxHp;
            yield return Util.YieldInstructionOnCache.WaitForEndOfFrame;
        }

        dampGaugeImage.fillAmount = _hpGaugeBar.fillAmount;
        _beforeHp = remain;
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(AudioSource))]
public class LevelShowUi : MonoBehaviour
{
    [SerializeField]
    private Text backGroundText;
    [SerializeField]
    private Text frontText;
    [SerializeField]
    private Image bottomLineImage;

    [SerializeField]
    private AudioClip levelShowClip;
    [SerializeField] [Range(0, 2f)]
    private float damp;
    
    // ------------------------------------ //

    private AudioSource _audioSource;

    #region Unity Process

    private void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
    }
    
    // End of Unity Process
    #endregion
    
    

    /// <summary>
    /// Reset Ui parameter
    /// </summary>
    public void ResetUi()
    {
        var backColor = backGroundText.color;
        backColor.a = 0f;
        backGroundText.color = backColor;
        var frontColor = frontText.color;
        frontColor.a = 0f;
        frontText.color = frontColor;
        var lineColor = bottomLineImage.color;
        lineColor.a = 0f;
        bottomLineImage.color = lineColor;
        gameObject.SetActive(false);
    }
    
    /// <summary>
    /// Start level name show coroutine
    /// </summary>
    /// <param name="levelName_">Name of level to string</param>
    public IEnumerator StartShowLevel(string levelName_)
    {
        gameObject.SetActive(true);
        _audioSource.PlayOneShot(levelShowClip);
        backGroundText.text = levelName_;
        frontText.text = levelName_;
        var backColor = backGroundText.color;
        var frontColor = frontText.color;
        var lineColor = bottomLineImage.color;

        yield return Util.YieldInstructionOnCache.WaitForSeconds(1.5f);
        
        while( backColor.a <= 1f || frontColor.a <= 1f || lineColor.a <= 1f )
        {
            var alpha = Time.deltaTime * damp;
            backColor.a += alpha;
            frontColor.a += alpha;
            lineColor.a += alpha;
            backGroundText.color = backColor;
            frontText.color = frontColor;
            bottomLineImage.color = lineColor;
            yield return Util.YieldInstructionOnCache.WaitForEndOfFrame;
        }

        yield return Util.YieldInstructionOnCache.WaitForSeconds(1.5f);
        
        while( backColor.a >= 0f || frontColor.a >= 0f || lineColor.a >= 0f )
        {
            var alpha = Time.deltaTime * damp;
            backColor.a -= alpha;
            frontColor.a -= alpha;
            lineColor.a -= alpha;
            backGroundText.color = backColor;
            frontText.color = frontColor;
            bottomLineImage.color = lineColor;
            yield return Util.YieldInstructionOnCache.WaitForEndOfFrame;
        }
        
        ResetUi();
    }
}

﻿using UnityEngine;
using UnityEngine.UI;

public class OptionDisplayButton : UiGridButton
{
	[SerializeField]
	private Dropdown resolutionDropDown;

	[SerializeField]
	private Toggle fullScreenToggle;
	// ------------------------------------- //
	private bool _isFullScreen;
	private void Start()
	{
		selectedButton.gameObject.SetActive(false);
		resolutionDropDown.onValueChanged.AddListener(OnDisplayResolutionValueChange);
		fullScreenToggle.onValueChanged.AddListener(OnFullScreenValueChange);
		resolutionDropDown.value = InitResolutionDropDown();
		fullScreenToggle.isOn = GameManager.Instance.GetFullScreen();
	}
	
	/// <summary>
	/// When FullScreen toggle button value change
	/// </summary>
	/// <param name="value_">Changed value</param>
	private void OnFullScreenValueChange(bool value_)
	{
		DEBUG.Log(value_.ToString());
		_isFullScreen = value_;
		GameManager.Instance.SetFullScreen(value_);
	}
	
	private void OnDisplayResolutionValueChange(int value_)
	{
		switch( value_ )
		{
			// 1920 X 1080
			case 0:
				Screen.SetResolution(1920,1080,_isFullScreen);
				GameManager.Instance.SetResolution(new GameManager.Resolution(1920,1080));
				break;
			// 1600 X 900
			case 1:
				Screen.SetResolution(1600,900,_isFullScreen);
				GameManager.Instance.SetResolution(new GameManager.Resolution(1600,900));
				break;
			// 1280 X 720
			case 2:
				Screen.SetResolution(1280,720,_isFullScreen);
				GameManager.Instance.SetResolution(new GameManager.Resolution(1280,720));
				break;
		}
	}

	private int InitResolutionDropDown()
	{
		var resolution = GameManager.Instance.GetResolution();
		switch( resolution.Width )
		{
			case 1920:
				return 0;
				break;
			case 1600:
				return 1;
				break;
			case 1280:
				return 2;
				break;
			default:
				return 0;
				break;
		}
	}
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OptionSelectPanel : MonoBehaviour
{
    [SerializeField]
    private UiGridButton[] parameterPanels;
    
    [HideInInspector]
    public int selectedIndex = 0;

    private void Start()
    {
        UpdateUi();
    }
    
    /// <summary>
    /// Update Ui
    /// </summary>
    public void UpdateUi()
    {
        for( var i = 0; i < parameterPanels.Length; i++ )
        {
            if ( i == selectedIndex )
            {
                parameterPanels[i].ButtonEnable();
                continue;
            }
            parameterPanels[i].ButtonDisable();
        }
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;
using UnityEngine.UI;

public class OptionInputButton : UiGridButton
{
    [SerializeField]
    private Slider sensitivitySlider;
    [SerializeField]
    private Text sliderValueText;
    [SerializeField]
    private Text reverseButtonValue;
 
    private float _sliderValue;
    private bool _isReverse;
    private readonly string[] _reverseTexts = { "Common", "Reverse" };

    private void Start()
    {
        sensitivitySlider.value = GameManager.Instance.GetMouseSensitivity();
        _isReverse = GameManager.Instance.GetMouseReverse();
        if ( !_isReverse )
        {
            reverseButtonValue.text = _reverseTexts[0];
        }
        else
        {
            reverseButtonValue.text = _reverseTexts[1];
        }
    }

    public void OnSliderValueChange()
    {
        sliderValueText.text = (_sliderValue = sensitivitySlider.value).ToString(CultureInfo.InvariantCulture);
        GameManager.Instance.SetMouseSensitivity((int)_sliderValue);
    }

    public void OnReverseButtonDown()
    {
        DEBUG.Log("CHANGE");
        _isReverse = !_isReverse;
        GameManager.Instance.SetMouseReverse(_isReverse);
        if ( _isReverse )
        {
            reverseButtonValue.text = _reverseTexts[0];
        }
        else
        {
            reverseButtonValue.text = _reverseTexts[1];
        }
    }
}

﻿using System;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(BoxCollider))]
public class LobbyButton : MonoBehaviour
{
    [SerializeField]
    private Image selectedImage;

    private void OnMouseEnter()
    {
        selectedImage.transform.position = transform.position;
    }
}

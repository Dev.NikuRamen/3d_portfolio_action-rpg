﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


[RequireComponent(typeof(AudioSource))]
public class StartSceneController : MonoBehaviour
{
    [Header("Set Ui component")]
    // Image of any key line image
    [SerializeField]
    private Image anyKeyLineImage;
    [SerializeField]
    private Image backGroundImage;
    [SerializeField]
    private GameObject onStartUi;
    // To Size up damp text
    [SerializeField]
    private Text anyKeyDampText;
    [SerializeField]
    private Text anyKeyMainText;
    [SerializeField]
    private LobbyMenu lobbyMenu;
    [SerializeField]
    private OptionMenu optionMenu;

    [SerializeField]
    private AudioClip startButtonClip;


    [Header("Set Parameters")]
    [SerializeField] [Range(0,2f)]
    private float fadeDamp;
    [SerializeField]
    private Vector3 fromUpScaleSize;
    [SerializeField]
    private Vector3 toUpScaleSize;
    [SerializeField] [Range(0.1f,3f)]
    private float buttonFadeDuration;
    [SerializeField]
    private AnimationCurve animationCurve =
        AnimationCurve.Linear(0, 0, 1, 1);
    // --------------------------- //
    private bool _isAnyKeyInput = false;
    private bool _isBackGroundFadeInEnd = false;
    private AudioSource _audioSource;

    private void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
        lobbyMenu.gameObject.SetActive(false);
        lobbyMenu.SetOptionMenuComponent(optionMenu);
        optionMenu.gameObject.SetActive(false);
        onStartUi.gameObject.SetActive(false);
        StartCoroutine(StartFadeInBackGround());
        GameManager.Instance.OnResetUi();
    }

    private void Update()
    {
        if ( _isBackGroundFadeInEnd )
        {
            if ( !_isAnyKeyInput )
            {
                if ( Input.anyKeyDown )
                {
                    _isAnyKeyInput = true;
                    StartCoroutine(StartFadeAnyKey());
                }
            }
        }
    }

    private IEnumerator StartFadeInBackGround()
    {
        _isBackGroundFadeInEnd = false;
        var color = backGroundImage.color;
        var alpha = 0f;

        yield return Util.YieldInstructionOnCache.WaitForSeconds(.8f);
        while( alpha <= 1f )
        {
            alpha += Time.deltaTime * fadeDamp;
            color.a = alpha;
            backGroundImage.color = color;
            yield return Util.YieldInstructionOnCache.WaitForEndOfFrame;
        }

        yield return Util.YieldInstructionOnCache.WaitForSeconds(1f);
        _isBackGroundFadeInEnd = true;
        onStartUi.gameObject.SetActive(true);
        GameManager.UnLockCursor();
    }
    
    /// <summary>
    /// Fade - Out Start any key
    /// </summary>
    /// <returns></returns>
    private IEnumerator StartFadeAnyKey()
    {
        var time = 0f;
        var alpha = 1f;
        var color = anyKeyDampText.color;
        _audioSource.PlayOneShot(startButtonClip);
        while( time <= buttonFadeDuration )
        {
            var size = Vector3.Lerp(fromUpScaleSize, toUpScaleSize, animationCurve.Evaluate(time));
            alpha = (buttonFadeDuration - time) * (buttonFadeDuration * .1f);
            color.a = alpha;
            anyKeyDampText.color = color;
            time += Time.deltaTime;
            anyKeyDampText.transform.localScale = size;
            anyKeyLineImage.transform.localScale = size;
            yield return Util.YieldInstructionOnCache.WaitForEndOfFrame;
        }

        color.a = 0f;
        anyKeyDampText.color = color;
        anyKeyDampText.gameObject.SetActive(false);
        anyKeyLineImage.gameObject.SetActive(false);
        anyKeyMainText.gameObject.SetActive(false);
        lobbyMenu.gameObject.SetActive(true);
    }
}

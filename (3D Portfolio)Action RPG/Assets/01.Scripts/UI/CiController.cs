﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CiController : MonoBehaviour
{
    [SerializeField]
    private Image ciLogoImage;

    private void Start()
    {
        var color = ciLogoImage.color;
        var alpha = 0f;
        color.a = alpha;
        ciLogoImage.color = color;
        GameManager.LockCursor();
        StartCoroutine(StartCiShow());
    }

    [SerializeField] [Range(0,2f)]
    private float damp;
    private IEnumerator StartCiShow()
    {
        yield return Util.YieldInstructionOnCache.WaitForSeconds(1f);
        var color = ciLogoImage.color;
        var alpha = 0f;
        while( alpha <= 1f )
        {
            alpha += Time.deltaTime * damp;
            color.a = alpha;
            ciLogoImage.color = color;
            yield return Util.YieldInstructionOnCache.WaitForEndOfFrame;
        }

        yield return Util.YieldInstructionOnCache.WaitForSeconds(2f);
        
        while( alpha >= 0f )
        {
            alpha -= Time.deltaTime * damp;
            color.a = alpha;
            ciLogoImage.color = color;
            yield return Util.YieldInstructionOnCache.WaitForEndOfFrame;
        }

        GameManager.Instance.LoadSceneNoSync(Loading.SceneName.StartScene);
    }
}

﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(BoxCollider))]
public class UiMouseHovering : MonoBehaviour
{
	private Image _selectedImage;
#if UNITY_EDITOR || UNITY_STANDALONE
	private void Awake()
	{
		_selectedImage = GetComponent<Image>();
	}

	private void OnMouseEnter()
	{
		_selectedImage.enabled = true;
	}

	private void OnMouseExit()
	{
		_selectedImage.enabled = false;
	}

#endif
}

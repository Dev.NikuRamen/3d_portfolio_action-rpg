﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class Loading : MonoBehaviour
{
    public enum SceneName
    {
        Ci,
        StartScene,
        Level_1,
        EndScene
    }
    [Header("Set Components")]
    [SerializeField]
    private Image loadingImage;
    [SerializeField]
    private Image loadingFireImage;
    [SerializeField]
    private Image logoImage;

    [Header("Set parameters")]
    [SerializeField] [Range(0, 2f)]
    private float fadeDamp = .5f;
    // ------------------------------------- //
    private AsyncOperation _sceneAsyncOperation;
    private Sprite[] _loadingSprite;

    private const string _LOADING_IMAGE_FILE_NAME = "MENU_MapImage_";

    #region Unity Process

    private void Awake()
    {
        var color = loadingImage.color;
        color.a = 0f;
        loadingImage.color = color;
        _loadingSprite = new Sprite[15];
        for( var i = 1; i <= 15; i++ )
        {
            _loadingSprite[i - 1] = Resources.Load<Sprite>($@"LoadingImage\{_LOADING_IMAGE_FILE_NAME
                                                                              + (10000 + i)}");
        }
    }

    private void LateUpdate()
    {
        if ( _sceneAsyncOperation != null && _sceneAsyncOperation.isDone )
        {
            _sceneAsyncOperation = null;
        }
    }
    
    // End of Unity Process
    #endregion

    
    
    /// <summary>
    /// Loading Scene with no Sync and no loading image
    /// </summary>
    /// <param name="sceneName_">To loading scene</param>
    public void LoadNoSync(SceneName sceneName_)
    {
        SceneManager.LoadScene(sceneName_.ToString());
    }
    
    /// <summary>
    /// Start loading process coroutine
    /// </summary>
    /// <param name="sceneName_">Loading scene state</param>
    public IEnumerator StartLoading(SceneName sceneName_)
    {
        yield return StartCoroutine(ShowLoadingImage()); // Fade - in loading image 
        _sceneAsyncOperation = SceneManager.LoadSceneAsync(sceneName_.ToString()); // Loading start
    }
    
    /// <summary>
    /// Show loading image to fade-in
    /// </summary>
    private IEnumerator ShowLoadingImage()
    {
        var image = _loadingSprite[Random.Range(0, _loadingSprite.Length)];
        loadingImage.sprite = image;
        var backColor = loadingImage.color;
        var fireColor = loadingFireImage.color;
        var logoColor = logoImage.color;
        var alpha = 0f;
        while( alpha <= 1f )
        {
            alpha += Time.deltaTime * fadeDamp;
            backColor.a = alpha;
            fireColor.a = alpha;
            logoColor.a = alpha;
            loadingImage.color = backColor;
            loadingFireImage.color = fireColor;
            logoImage.color = logoColor;
            yield return Util.YieldInstructionOnCache.WaitForEndOfFrame;
        }
    }
    /// <summary>
    /// Release loading image with fade - out
    /// </summary>
    public IEnumerator StartFadeOutLoadingImage()
    {
        var backColor = loadingImage.color;
        var fireColor = loadingFireImage.color;
        var logoColor = logoImage.color;
        var alpha = 1f;
        while( alpha>= 0f )
        {
            alpha -= Time.deltaTime * fadeDamp;
            backColor.a = alpha;
            fireColor.a = alpha;
            logoColor.a = alpha;
            loadingImage.color = backColor;
            loadingFireImage.color = fireColor;
            logoImage.color = logoColor;
            yield return Util.YieldInstructionOnCache.WaitForEndOfFrame;
        }
    }
    
    /// <summary>
    /// Release loading image immediately
    /// </summary>
    public void ReleaseLoadingImage()
    {
        var backColor = loadingImage.color;
        var fireColor = loadingFireImage.color;
        var logoColor = logoImage.color;
        const float alpha = 0f;
        fireColor.a = alpha;
        backColor.a = alpha;
        logoColor.a = alpha;
        loadingImage.color = backColor;
        loadingFireImage.color = fireColor;
        logoImage.color = logoColor;
    }
}

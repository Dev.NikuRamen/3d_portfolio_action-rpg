﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LobbyMenu : MonoBehaviour
{
    [SerializeField]
    private Image selectLineImage;

    [SerializeField]
    private AudioSource lobbyBGMSource;
    
    // -------------------------- //
    private delegate void MenuButtonDelegate();
    private Button[] _menuButtons;
    private int _buttonIndex = 0;
    private Vector3 _selectLineLocalPosition;
    private bool _isStart = false;
    private MenuButtonDelegate[] _menuButtonEvent;
    private OptionMenu _optionMenu;

    #region Unity Process

    private void Awake()
    {
        _menuButtons = GetComponentsInChildren<Button>();
        _selectLineLocalPosition = selectLineImage.transform.localPosition;
        InitMenuButtonEvent();
    }

    private void Update()
    {
#if UNITY_EDITOR || PLATFORM_STANDALONE
        // If down key input
        if ( Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow) )
        {
            if ( _buttonIndex >= _menuButtons.Length )
                return;
            _buttonIndex++;
            _selectLineLocalPosition.y -=  40;
            selectLineImage.transform.localPosition = _selectLineLocalPosition;
        }
        else if ( Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow) )
        {
            if ( _buttonIndex <= 0 )
                return;
            _buttonIndex--;
            _selectLineLocalPosition.y += 40;
            selectLineImage.transform.localPosition = _selectLineLocalPosition;
        }

        if ( Input.GetKeyDown(KeyCode.Return) )
        {
            _menuButtonEvent[_buttonIndex].Invoke();
        }
#endif
    }
    
    // End of Unity Process
    #endregion

    #region Event Method
    
    /// <summary>
    /// When Game Start button down, Load Game scene
    /// </summary>
    public void OnStartButtonDown()
    {
        StartCoroutine(StartFadeOutBGM());
        if ( !_isStart )
        {
            _isStart = true;
            StartCoroutine(GameManager.Instance.StartLoadAsync(Loading.SceneName.Level_1));
        }
        _selectLineLocalPosition.y = -180;
        selectLineImage.transform.localPosition = _selectLineLocalPosition;
        _buttonIndex = 0;
        GameManager.LockCursor();
    }
    
    /// <summary>
    /// When Option Button down, show option panel
    /// </summary>
    public void OnOptionButtonDown()
    {
        _selectLineLocalPosition.y = -220;
        selectLineImage.transform.localPosition = _selectLineLocalPosition;
        _buttonIndex = 1;
        _optionMenu.gameObject.SetActive(true);
        gameObject.SetActive(false);
    }
    
    /// <summary>
    /// When exit button down, quit the game
    /// </summary>
    public void OnExitButtonDown()
    {
        _selectLineLocalPosition.y = -260;
        selectLineImage.transform.localPosition = _selectLineLocalPosition;
        _buttonIndex = 2;
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#elif PLATFORM_STANDALONE
        Application.Quit();
#endif
    }

    #endregion

    #region Public Method
    
    /// <summary>
    /// Get Option menu component to caching
    /// </summary>
    /// <param name="optionMenu_">Base class</param>
    public void SetOptionMenuComponent(OptionMenu optionMenu_)
    {
        _optionMenu = optionMenu_;
    }

    #endregion
    
    #region Private Method
    
    /// <summary>
    /// Initialize menu button down event
    /// </summary>
    private void InitMenuButtonEvent()
    {
        _menuButtonEvent = new MenuButtonDelegate[_menuButtons.Length];
        _menuButtonEvent[0] = OnStartButtonDown;
        _menuButtonEvent[1] = OnOptionButtonDown;
        _menuButtonEvent[2] = OnExitButtonDown;
    }
    
    /// <summary>
    /// Fade - out to mute bgm as smoothly
    /// </summary>
    private IEnumerator StartFadeOutBGM()
    {
        var volume = lobbyBGMSource.volume;
        while( volume >=0f )
        {
            volume -= Time.deltaTime;
            lobbyBGMSource.volume = volume;
            yield return Util.YieldInstructionOnCache.WaitForEndOfFrame;
        }
    }
    
    #endregion
    
}

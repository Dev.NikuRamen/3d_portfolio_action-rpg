﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StaminaBar : MonoBehaviour
{
    [SerializeField]
    private Image staminaImage;
    // ---------------------------------- //
    private float _currentStamina;
    private float _maxStamina;
    
    
    /// <summary>
    /// Set stamina max value
    /// </summary>
    /// <param name="stamina_"></param>
    public void SetUi(float stamina_)
    {
        _currentStamina = stamina_;
        _maxStamina = stamina_;
    }

    public void OnChangedUi(float stamina_)
    {
        _currentStamina = stamina_;
        staminaImage.fillAmount = _currentStamina / _maxStamina;
    }
    
    /// <summary>
    /// Reduce stamina value and image
    /// </summary>
    /// <param name="value_">Reduced value</param>
    public void ReduceStamina(float value_)
    {
        _currentStamina -= value_;
        staminaImage.fillAmount = _currentStamina / _maxStamina;
    }
}

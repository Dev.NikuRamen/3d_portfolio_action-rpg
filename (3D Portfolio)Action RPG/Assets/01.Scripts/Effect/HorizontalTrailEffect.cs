﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HorizontalTrailEffect : PoolingObject
{
	private Transform _leftTransform;

	private void Awake()
	{
		_leftTransform = GetComponentInChildren<Transform>();
	}
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitEffect : MonoBehaviour
{
    private Transform _transform;
    private Human _parentController;
    private void Awake()
    {
        _transform = transform;
        _parentController = GetComponentInParent<Human>();
    }

    public Human GetParentHuman() => _parentController;
    
    public void CreateEffect(string key_,Vector3 hitPosition_, Vector3 attackerPosition_,Vector3 size_,float showTime_)
    {
        _transform.LookAt(attackerPosition_);
        EffectManager.Instance.Create(key_, hitPosition_, _transform.rotation, size_, _transform, showTime_);
    }
}

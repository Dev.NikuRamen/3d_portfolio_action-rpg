﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;


public class HumanDataManager : DontDestroy<HumanDataManager>
{
    // ------------------------------------------------------ //
    private StringBuilder _fileName = new StringBuilder();
    private const string _ASSET_FILE_PATH = "DataTable/";

    private Dictionary<string, Dictionary<string, Data>> _playerAttackDataDictionary =
        new Dictionary<string, Dictionary<string, Data>>();
    public struct Data
    {
        public string id;
        public string damage;
        public string hitBox;
        public string effectKey;
        public string isKnockDown;
        public string attackSFX;
    }
    
    public enum DataKeyName
    {
        Player,
        OneHandSword
    }
    
    public enum TrailEffectName
    {
        NONE = 0,
        Front,
        Horizontal
    }

    protected override void OnStart()
    {
        LoadData(nameof(DataKeyName.Player),"Player_Attack");
        LoadData(nameof(DataKeyName.OneHandSword),"OneSword_Attack");
    }

    public void LoadData(string keyName_,string fileName_)
    {
        _fileName.Clear();
        _fileName.Append(_ASSET_FILE_PATH + fileName_);
        var tableLoader = TableLoader.Instance;
        var table = Resources.Load(_fileName.ToString()) as TextAsset;
        if ( table == null )
        {
            DEBUG.Log("File is NULL : " + _ASSET_FILE_PATH + fileName_, DEBUG.DebugMode.Error);
            return;
        }
        tableLoader.LoadTable(table.bytes);
        
        var headData = new Data
        {
            id = tableLoader.GetString("Id", 0),
            damage = tableLoader.GetString("Damage", 0),
            hitBox = tableLoader.GetString("HitBox", 0),
            effectKey = tableLoader.GetString("Effect_Key", 0),
            isKnockDown = tableLoader.GetString("IsKnockDown",0),
            attackSFX = tableLoader.GetString("Attack_SFX",0),
        };
        

        var dic = new Dictionary<string, Data>();
        dic.Add(headData.id, headData);
        _playerAttackDataDictionary.Add(keyName_, dic);
        
        for( var i = 1; i < tableLoader.GetLength(); i++ )
        {
            var data = new Data
            {
                id = tableLoader.GetString("Id", i),
                damage = tableLoader.GetString("Damage", i),
                hitBox = tableLoader.GetString("HitBox", i),
                effectKey = tableLoader.GetString("Effect_Key", i),
                isKnockDown = tableLoader.GetString("IsKnockDown",i),
                attackSFX = tableLoader.GetString("Attack_SFX",i),
            };

            if ( !string.IsNullOrEmpty(data.id) && !string.IsNullOrEmpty(data.damage) )
            {
                _playerAttackDataDictionary[keyName_].Add(data.id, data);
            }
        }
    }
    
    /// <summary>
    /// Get Data
    /// </summary>
    /// <param name="keyName_">Base object name</param>
    /// <param name="keyToFind_">Excel first row value</param>
    /// <returns>Data</returns>
    public Data GetData(string keyName_,string keyToFind_)
    {
        var hasKey = _playerAttackDataDictionary[keyName_].TryGetValue(keyToFind_, out var data);
        if ( hasKey )
        {
            return data;
        }
        DEBUG.Log(data.id);
        DEBUG.Log("Not Has key : " + keyName_ +" "+ keyToFind_);
        return new Data();
    }
    
    public void Unload()
    {
        _playerAttackDataDictionary.Clear();
    }
}

﻿public class EffectManager : PoolingManager
{
	public enum Effects
	{
		Common,
		Extend,
		Special,
		Guard
	}

	protected override void OnAwake()
	{
		base.OnAwake();

		objectPool.InitPool(nameof(Effects.Common),effectPrefabs[0]);	
		objectPool.InitPool(nameof(Effects.Extend),effectPrefabs[1]);
		objectPool.InitPool(nameof(Effects.Special),effectPrefabs[2]);
		objectPool.InitPool(nameof(Effects.Guard),effectPrefabs[3]);
	}
}

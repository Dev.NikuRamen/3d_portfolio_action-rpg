﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : DontDestroy<GameManager>
{
    public struct Resolution
    {
        public int Width;
        public int Height;

        public Resolution(int width_, int height_)
        {
            Width = width_;
            Height = height_;
        }
    }
    
    [Header("Set Components")]   
    [SerializeField]
    private Image fadeInImage;
    [SerializeField]
    private Image playerDieImage;
    [SerializeField]
    private Loading loading;
    [SerializeField]
    private HpGaugeBar playerHudGaugeBar;
    [SerializeField]
    private StaminaBar playerStaminaBar; 
    [SerializeField]
    private LevelShowUi levelShowUi;
    [SerializeField]
    private DefeatUi defeatUi;

    [Header("Set Parameters")]
    [Range(0,2f)] [Tooltip("Scale of game time")]
    public float timeScale = 1;
    [SerializeField]
    private Loading.SceneName currentSceneState;
    public DefeatUi getDefeatui => defeatUi;
    public LevelShowUi getLevelShowUi => levelShowUi;
    public HpGaugeBar getPlayerHpGaugeBar => playerHudGaugeBar;
    public StaminaBar getStaminaBar => playerStaminaBar;
    
    public readonly string[] OptionKeys = { "Sensitivity", "Reverse","Width","Height","Full-Screen" };
    
    public Dictionary<string,string> gameOptionDictionary = new Dictionary<string, string>();
    // -------------------------- //
    protected override void OnAwake()
    {
        OnResetUi();
        playerHudGaugeBar.gameObject.SetActive(false);
        gameOptionDictionary = XMLController.LoadData("Option", "Option", OptionKeys);
        var resolution = GetResolution();
        var fullScreen = GetFullScreen();
        Screen.SetResolution(resolution.Width,resolution.Height,fullScreen);
    }

    private void Update()
    {
        Time.timeScale = timeScale;
    }
    
    /// <summary>
    /// Game time scale restore to 1
    /// </summary>
    /// <param name="damp_">Restore damp</param>
    public void OnTimeScaleRestore(float damp_)
    {
        StartCoroutine(StartRestoreTime(damp_));
    }

    /// <summary>
    /// Fade-In Effect
    /// </summary>
    public IEnumerator StartFadeIn(float damp_ = 1f)
    {
        var alpha = 0f;
        var color = fadeInImage.color;
        while( alpha <= 1 )
        {
            alpha += Time.deltaTime * damp_;
            color.a = alpha;
            fadeInImage.color = color;
            yield return Util.YieldInstructionOnCache.WaitForEndOfFrame;
        }
        playerHudGaugeBar.gameObject.SetActive(false);
    }
    
    /// <summary>
    /// Start Fade-Out
    /// </summary>
    /// <param name="damp_">[Range(0 ~ 1f)] Release Damp</param>
    public IEnumerator StartFadeOut(float damp_ = 1f)
    {
        var alpha = 1f;
        var color = fadeInImage.color;
        playerHudGaugeBar.gameObject.SetActive(true);
        while( alpha>=0 )
        {
            alpha -= Time.deltaTime * damp_;
            color.a = alpha;
            fadeInImage.color = color;
            yield return Util.YieldInstructionOnCache.WaitForEndOfFrame;
        }
    }
    
    /// <summary>
    /// Restore timeScale
    /// </summary>
    /// <param name="damp_">Restore timeScale damp</param>
    private IEnumerator StartRestoreTime(float damp_)
    {
        if ( timeScale < 1 )
        {
            while( timeScale<=1 )
            {
                timeScale += Time.fixedDeltaTime * damp_;
                yield return Util.YieldInstructionOnCache.WaitForEndOfFrame;
            }
            timeScale = 1;
        }
        else
        {
            while( timeScale>=1 )
            {
                timeScale -= Time.fixedDeltaTime * damp_;
                yield return Util.YieldInstructionOnCache.WaitForEndOfFrame;
            }

            timeScale = 1;
        }
    }
    
    /// <summary>
    /// Slow motion effect
    /// </summary>
    /// <param name="scale_">To set timeScale value</param>
    /// <param name="damp_">Reduce damp</param>
    private IEnumerator StartReduceTimeScale(float scale_, float damp_)
    {
        while( timeScale <= scale_ )
        {
            timeScale -= Time.deltaTime * damp_;
            yield return Util.YieldInstructionOnCache.WaitForEndOfFrame;
        }

        timeScale = scale_;
    }
    /// <summary>
    /// Slow motion,use timeScale
    /// </summary>
    /// <param name="timeScale_">To set timeScale</param>
    /// <param name="returnTime_">Restore time to current timeScale</param>
    public IEnumerator StartSlowMotionTime(float timeScale_,float returnTime_)
    {
        yield return StartCoroutine(StartReduceTimeScale(timeScale_, 0.6f));
        yield return Util.YieldInstructionOnCache.WaitForSeconds(returnTime_);

        OnTimeScaleRestore(.6f);
    }

    /// <summary>
    /// When player die, show playerDieImage as fade-in
    /// </summary>
    public IEnumerator StartPlayerDieShow(float damp_)
    {
        yield return Util.YieldInstructionOnCache.WaitForSeconds(3f);
        var alpha = 0f;
        var color = playerDieImage.color;
        while( alpha<=1f )
        {
            alpha += Time.deltaTime * damp_;
            color.a = alpha;
            playerDieImage.color = color;
            yield return Util.YieldInstructionOnCache.WaitForEndOfFrame;
        }
        yield return StartCoroutine(Level.Instance.StartFadeOutBgm());
        // Load current Scene to async
        yield return StartCoroutine(StartLoadAsync(currentSceneState));
    }

    public IEnumerator StartFadeOutLoadingImage()
    {
        yield return StartCoroutine(loading.StartFadeOutLoadingImage());
    }
   
    
    /// <summary>
    /// Load Scene with async and fade-in
    /// </summary>
    /// <param name="sceneName_">Load scene name</param>
    public IEnumerator StartLoadAsync(Loading.SceneName sceneName_)
    {
        var color = playerDieImage.color;
        color.a = 0f;
        yield return StartCoroutine(StartFadeIn(.8f));
        playerDieImage.color = color;
        yield return StartCoroutine(loading.StartLoading(sceneName_));
        currentSceneState = sceneName_;
    }
    
    /// <summary>
    /// Loading Scene with no Sync and no loading image
    /// </summary>
    /// <param name="sceneName_">To loading scene</param>
    public void LoadSceneNoSync(Loading.SceneName sceneName_)
    {
        loading.LoadNoSync(sceneName_);
        currentSceneState = sceneName_;
    }
    
    /// <summary>
    /// Reset managed ui
    /// </summary>
    public void OnResetUi()
    {
        var fadeColor = fadeInImage.color;
        var dieColor = playerDieImage.color;
        dieColor.a = 0f;
        fadeColor.a = 0f;
        fadeInImage.color = fadeColor;
        playerDieImage.color = dieColor;
        defeatUi.ResetUi();
        loading.ReleaseLoadingImage();
        levelShowUi.ResetUi();
    }
    
    /// <summary>
    /// Lock and hide cursor
    /// </summary>
    public static void LockCursor()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }
    
    /// <summary>
    /// Show and unlock cursor
    /// </summary>
    public static void UnLockCursor()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }
    
    /// <summary>
    /// Get Mouse Sensitivity value
    /// </summary>
    /// <returns>Value</returns>
    public int GetMouseSensitivity() => int.Parse(gameOptionDictionary[OptionKeys[0]]);

    public void SetMouseSensitivity(int value_)
    {
        gameOptionDictionary[OptionKeys[0]] = value_.ToString();
        XMLController.CreateXmlFile("Option",gameOptionDictionary ,"Option", OptionKeys);
    }
    /// <summary>
    /// Get is Mouse Reverse
    /// </summary>
    /// <returns>Value</returns>
    public bool GetMouseReverse() => bool.Parse(gameOptionDictionary[OptionKeys[1]]);

    public void SetMouseReverse(bool value_)
    {
        gameOptionDictionary[OptionKeys[1]] = value_.ToString();
        XMLController.CreateXmlFile("Option",gameOptionDictionary ,"Option", OptionKeys);
    }

    public void SetResolution(Resolution value_)
    {
        gameOptionDictionary[OptionKeys[2]] = value_.Width.ToString();
        gameOptionDictionary[OptionKeys[3]] = value_.Height.ToString();
        XMLController.CreateXmlFile("Option",gameOptionDictionary,"Option",OptionKeys);
    }
    
    /// <summary>
    /// Get Resolution data
    /// </summary>
    /// <returns>Data</returns>
    public Resolution GetResolution()
    {
        var width = int.Parse(gameOptionDictionary[OptionKeys[2]]);
        var height = int.Parse(gameOptionDictionary[OptionKeys[3]]);
        return new Resolution(width,height);
    }

    public void SetFullScreen(bool value_)
    {
        gameOptionDictionary[OptionKeys[4]] = value_.ToString();
        XMLController.CreateXmlFile("Option",gameOptionDictionary,"Option",OptionKeys);
    }

    public bool GetFullScreen() => bool.Parse(gameOptionDictionary[OptionKeys[4]]);
}

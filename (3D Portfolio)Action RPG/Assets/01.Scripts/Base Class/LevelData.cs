﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelData : MonoBehaviour
{
	[SerializeField]
	protected Door enterDoor;
	[SerializeField]
	protected Door exitDoor;
	[SerializeField]
	private string levelName;
	[SerializeField]
	private Loading.SceneName nextLevelName;
	public Loading.SceneName getNextLevelName => nextLevelName;
	public string getLevelName => levelName;

	public Door getEnterDoor => enterDoor;

	// -------------------------------------------------------------- //
	protected readonly Queue<ActionCamera.CameraEventDelegate>
		CutSceneEventQueue = new Queue<ActionCamera.CameraEventDelegate>();

	private void Start()
	{
		StartCoroutine(StartOnLevelLoaded());
	}

	protected virtual IEnumerator StartOnLevelLoaded()
	{
		yield return StartCoroutine(GameManager.Instance.StartFadeOut());
		yield return StartCoroutine(GameManager.Instance.StartFadeOutLoadingImage());
		yield return StartCoroutine(GameManager.Instance.getLevelShowUi.StartShowLevel(Level.Instance.getLevelName));
	}

	/// <summary>
	/// When player arrive at enter door, play event
	/// </summary>
	/// <returns>Door Event</returns>
	public ActionCamera.CameraEventDelegate OnDequeueDoorEvent()
	{
		return CutSceneEventQueue.Dequeue();
	}
}

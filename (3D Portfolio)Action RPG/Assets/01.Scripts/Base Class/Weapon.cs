﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    [SerializeField]
    protected Transform unequippedTransform; // Unequipped weapon's transform

    private void Awake()
    {
        // When Initialized, set idle
        if ( unequippedTransform )
        {
            UnEquip();
        }
    }

    /// <summary>
    /// Weapon Equip
    /// </summary>
    public virtual void Equip()
    {
        gameObject.SetActive(true);
        unequippedTransform.gameObject.SetActive(false);
    }
    
    /// <summary>
    /// Weapon Unequipped
    /// </summary>
    public virtual void UnEquip()
    {
        gameObject.SetActive(false);
        unequippedTransform.gameObject.SetActive(true);
    }
}

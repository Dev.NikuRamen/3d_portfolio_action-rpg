﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(AudioSource))]
public class Door : MonoBehaviour
{
    public enum AnimationName
    {
        Open,
        Close
    }

    [SerializeField]
    protected AudioClip doorOpenClip;

    [SerializeField]
    protected AudioClip doorCloseClip;
    // --------------------------- //
    protected ActionCamera ActionCamera;
    protected Animator Animator;
    protected bool AlReadySawCutScene = false;
    protected AudioSource AudioSource;
    private void Awake()
    {
        Animator = GetComponent<Animator>();
        ActionCamera = FindObjectOfType<ActionCamera>();
        AudioSource = GetComponent<AudioSource>();
    }

    #region Event Method

    public virtual void OnOpen()
    {
        Animator.SetTrigger(nameof( AnimationName.Open ));
        
    }

    public virtual void OnClose()
    {

        Animator.SetTrigger(nameof( AnimationName.Close ));
    }
    
    public void OnPlayDoorOpen()
    {
        AudioSource.PlayOneShot(doorOpenClip);
    }
    
    // End of Event Method
    #endregion
}

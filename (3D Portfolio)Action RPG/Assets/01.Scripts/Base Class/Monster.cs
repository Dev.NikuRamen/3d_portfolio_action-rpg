﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
[RequireComponent(typeof(CapsuleCollider))]
public class Monster : Human
{
	/// <summary>
	/// Ai FSM State
	/// </summary>
	public enum AiState
	{
		Nothing = 0,
		Attacking = 1,
		Offensive,
		Defensive,
		MAX,
	}
	protected enum EscapeDirection
	{
		Left,
		Right,
		Front,
		Back
	}
	
	[Header("Monster Parameter")]
	
	[SerializeField][Range(0,10f)]
	protected float rotateSpeed;
	[SerializeField]
	protected AiState aiState;
	// --------------------------------------- //
	protected PlayerController PlayerController;
	private Vector3 _beforeTargetPosition;
	protected Quaternion StartRotation; // When rotating start, before rotating rotation
	protected Quaternion EndRotation; // Final rotated rotation
	protected NavMeshAgent NavMeshAgent;

	protected override void OnAwake()
	{
		base.OnAwake();
		PlayerController = GameObject.FindWithTag("Player").GetComponent<PlayerController>();
		NavMeshAgent = GetComponent<NavMeshAgent>();
		aiState = AiState.Nothing;
		var hpGauge = FindObjectsOfType<HpGaugeBar>();
		foreach( var gauge in hpGauge )
		{
			if ( gauge.gaugeType == HpGaugeBar.HpGaugeType.Monster )
			{
				hpProgressBar = gauge;
				break;
			}
		}
		hpProgressBar.SetHpGauge(CurrentHp,MaxHp, HumanName);
	}

	protected override void OnUpdate()
	{
		AiProcess();
	}

	/// <summary>
	/// Get distance between player and monster.
	/// </summary>
	/// <returns>Distance to float.</returns>
	protected float GetDistanceBetweenPlayer()
	{
		var distance = Vector3.Distance(transform.position, PlayerController.transform.position);
		return distance;
	}
	
	/// <summary>
	/// LookAt target use transform.
	/// </summary>
	/// <param name="targetTransform_">Look target transform.</param>
	protected void LookAtTarget(Transform targetTransform_)
	{
		LookAtTarget(targetTransform_.position);
	}
	/// <summary>
	/// LookAt target use vector3.
	/// </summary>
	/// <param name="targetPosition_">Look target Position.</param>
	protected void LookAtTarget(Vector3 targetPosition_)
	{
		// Caching
		var myTransform = transform;
		var position = myTransform.position;
		StartRotation = myTransform.rotation;
		// Player direction
		var targetDirection = new Vector3(targetPosition_.x, position.y, targetPosition_.z) - position;
		if ( targetDirection == Vector3.zero )
		{
			targetDirection = transform.forward;
		}
		EndRotation = Quaternion.LookRotation(targetDirection);
		transform.rotation = Quaternion.Lerp(StartRotation, EndRotation, Time.deltaTime * rotateSpeed);
	}

	protected virtual void AiProcess()
	{
		
	}

	protected void SetAiState(AiState state_)
	{
		aiState = state_;
	}
	

	protected virtual void OnEscapeMove(EscapeDirection escapeDirection_)
	{
		
	}

}

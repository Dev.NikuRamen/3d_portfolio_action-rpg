﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(AudioSource))]
public class UiGridButton : MonoBehaviour
{
	[Header("Component Cache")]
	[SerializeField]
	protected Image selectedButton;
	[SerializeField]
	protected GameObject optionParameterGameObject;
	[SerializeField]
	private OptionSelectPanel optionSelectPanel;
	[SerializeField]
	private AudioClip selectClip;
	[SerializeField][Tooltip("Sequence of buttons")]
	protected int buttonIndex;
	// ----------------------------- //
	private AudioSource _audioSource;

	private void Awake()
	{
		_audioSource = GetComponent<AudioSource>();
	}

	/// <summary>
	/// Show selected image 
	/// </summary>
	public void ButtonEnable()
	{
		selectedButton.gameObject.SetActive(true);
	}
	
	/// <summary>
	/// Hide selected image
	/// </summary>
	public void ButtonDisable()
	{
		selectedButton.gameObject.SetActive(false);
		optionParameterGameObject.SetActive(false);
	}
	
	/// <summary>
	/// Button down event
	/// </summary>
	public void OnButtonDown()
	{
		_audioSource.PlayOneShot(selectClip);
		selectedButton.gameObject.SetActive(true);
        optionParameterGameObject.SetActive(true);
        ButtonEnable();
        optionSelectPanel.selectedIndex = buttonIndex;
        optionSelectPanel.UpdateUi();
	}
}

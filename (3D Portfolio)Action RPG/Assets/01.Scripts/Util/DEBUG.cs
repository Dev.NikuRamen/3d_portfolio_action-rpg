﻿using System;
using UnityEngine;

public class DEBUG
{
	public enum DebugMode
	{
		genenral,
		warning,
		Error
	}
	
	public static void Log(string message,DebugMode mode = DebugMode.genenral)
	{
#if UNITY_EDITOR
		switch( mode )
		{
			case DebugMode.genenral:
				Debug.Log(message);
				break;
			case DebugMode.warning:
				Debug.LogWarning(message);
				break;
			case DebugMode.Error:
				Debug.LogError(message);
				break;
		}
#endif
	}
}

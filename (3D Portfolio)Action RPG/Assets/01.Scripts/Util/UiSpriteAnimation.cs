﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
[RequireComponent(typeof(SpriteRenderer))]
[RequireComponent(typeof(Animator))]
public class UiSpriteAnimation : MonoBehaviour
{
	private SpriteRenderer _renderer;
	private Image _image;
	private void Awake()
	{
		_image = GetComponent<Image>();
		_renderer = GetComponent<SpriteRenderer>();
	}

	private void LateUpdate()
	{
		_image.sprite = _renderer.sprite;
	}
}

﻿using System.Collections.Generic;
using System.IO;

public class TableLoader : Singleton<TableLoader>
{
    List<Dictionary<string, string>> m_table = new List<Dictionary<string,string>>();

    public string GetData(string key_, int index_)
    {
        return m_table[index_][key_];
    }

    public string GetString(string key_, int index_)
    {
        return GetData(key_, index_);
    }

    public T GetEnum<T>(string key_, int index_)
    {
        return Util.ToEnum<T>(GetData(key_, index_));
    }

    public byte GetByte(string key_, int index_)
    {
        return byte.Parse(GetData(key_, index_));
    }

    public int GetInt(string key_, int index_)
    {
        return int.Parse(GetData(key_, index_));
    }

    public float GetFloat(string key_, int index_)
    {
        var value = 0f;
        try
        {
            value = float.Parse(GetData(key_, index_));
        }
        catch
        {
            
        }
        return value;
    }

    public int GetLength()
    {
        return m_table.Count;
    }

    public void LoadTable(byte[] buff_)
    {
        MemoryStream ms = new MemoryStream(buff_);
        BinaryReader br = new BinaryReader(ms);        
        int rowCount = br.ReadInt32();
        int colCount = br.ReadInt32();

        string[] stringData = br.ReadString().Split('\t');

        List<string> listKey = new List<string>();
        m_table.Clear();

        int offset = 1;
        for (int i = 0; i < rowCount; i++)
        {
            offset++;
            if (i == 0)
            {
                for (int j = 0; j < colCount - 1; j++)
                {
                    var splitData = stringData[offset].Split('\r')[0];
                    listKey.Add(splitData);
                    offset++;
                }
            }
            else
            {
                Dictionary<string, string> dicData = new Dictionary<string, string>();
                for (int j = 0; j < colCount - 1; j++)
                {
                    string data = stringData[offset].Replace("\\n", "\n");
                    data = data.Split('\r')[0];
                    if(!string.IsNullOrWhiteSpace(listKey[j]))
                        dicData.Add(listKey[j], data);
                    offset++;
                }
                m_table.Add(dicData);
            }
        }

        ms.Close();
        br.Close();
    }

    public void Clear()
    {
        m_table.Clear();
    }
  
}
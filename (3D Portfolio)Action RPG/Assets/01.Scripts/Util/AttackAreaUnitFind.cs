﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class AttackAreaUnitFind : MonoBehaviour
{
    [SerializeField]
    private string checkUnityTypeText = "Player";
    
    private List<HitEffect> _findUnitList = new List<HitEffect>();
    private Dictionary<HitEffect,Vector3> _unitHitPositionDictionary = new Dictionary<HitEffect, Vector3>();

    public List<HitEffect> GetFindUnits()
    {
        return _findUnitList;
    }

    public Vector3 GetHitPosition(HitEffect object_)
    {
        return _unitHitPositionDictionary[object_];
    }

    public bool RemoveUnit(HitEffect unit_)
    {
        _unitHitPositionDictionary.Remove(unit_);
        return _findUnitList.Remove(unit_);
    }

    private void OnTriggerEnter(Collider other)
    {
        if ( other.tag.Equals(checkUnityTypeText) )
        {
            var hitEffect = other.GetComponentInChildren<HitEffect>();
            _unitHitPositionDictionary.Add(hitEffect, other.ClosestPoint(transform.position));
            _findUnitList.Add(hitEffect);

        }
    }

    private void OnTriggerExit(Collider other)
    {
        if ( other.tag.Equals(checkUnityTypeText) )
        {
            var hitEffect = other.GetComponentInChildren<HitEffect>();
            _unitHitPositionDictionary.Remove(hitEffect);
            _findUnitList.Remove(hitEffect);
        }
    }
}

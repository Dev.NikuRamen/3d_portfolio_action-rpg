﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolingObject : MonoBehaviour
{
    private Transform _managerTransform;
    protected float showTime; // Object show time
    private bool _isCanPlay = true;
    public bool isCanPlay => _isCanPlay;

    private void Awake()
    {
        gameObject.SetActive(false);
    }

    /// <summary>
    /// Pooling object setting
    /// </summary>
    /// <param name="createPosition_">Spawn Position</param>
    /// <param name="createRotation_">Spawn Rotation</param>
    /// <param name="managerTransform_">Pooling Manager Transform to return base trnasform</param>
    /// <param name="showTime_">Object show time</param>
    public virtual void Set(Vector3 createPosition_,Quaternion createRotation_,Transform managerTransform_,float showTime_ 
    = 0)
    {
        _isCanPlay = false;
        showTime = showTime_;
        gameObject.SetActive(true);
        var myTransform = transform;
        _managerTransform = managerTransform_;
        myTransform.position = createPosition_;
        myTransform.rotation = createRotation_;
        
        if ( showTime_ > 0 )
        {
            StartCoroutine(StartEffect());
        }
    }

    private IEnumerator StartStayWhileEffectReset()
    {
        yield return Util.YieldInstructionOnCache.WaitForSeconds(1f);
        _isCanPlay = true;
    }
    
    private IEnumerator StartEffect()
    {
        yield return Util.YieldInstructionOnCache.WaitForSeconds(showTime);
        yield return Util.YieldInstructionOnCache.WaitForEndOfFrame;
        StartCoroutine(StartStayWhileEffectReset());
        gameObject.SetActive(false);
        transform.SetParent(_managerTransform);
    }
}

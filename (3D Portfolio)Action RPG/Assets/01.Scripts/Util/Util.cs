﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public static class Util
{
	/// <summary>
	/// Coroutine caching
	/// </summary>
	internal static class YieldInstructionOnCache
	{
		class FloatCompare : IEqualityComparer<float>
		{
			public bool Equals(float x_, float y_)
			{
				// Float의 안정성
				return Math.Abs(x_ - y_) < .0001f;
			}

			public int GetHashCode(float obj_)
			{
				return obj_.GetHashCode();
			}
		}
		public static readonly Dictionary<float,WaitForSeconds> TimeInterval = new Dictionary<float, WaitForSeconds>
		(new FloatCompare());
		public static readonly WaitForEndOfFrame WaitForEndOfFrame = new WaitForEndOfFrame();
		public static readonly WaitForFixedUpdate WaitForFixedUpdate = new WaitForFixedUpdate();

		public static WaitForSeconds WaitForSeconds(float time_)
		{
			if ( !TimeInterval.TryGetValue(time_, out var waitForSeconds) )
			{
				TimeInterval.Add(time_,waitForSeconds = new WaitForSeconds(time_));
			}

			return waitForSeconds;
		}
	}
	
	public static T ToEnum<T>(string str)
	{
		System.Array A = System.Enum.GetValues(typeof(T));
		foreach (T t in A)
		{
			if (t.ToString() == str)
				return t;
		}
		return default(T);
	}
	public static GameObject FindChildObject(GameObject go, string name)
	{
		foreach (Transform tr in go.transform)
		{
			if (tr.name.Equals(name))
			{
				return tr.gameObject;
			}
			else
			{
				GameObject find = FindChildObject(tr.gameObject, name);
				if (find != null)
				{
					return find;
				}
			}
		}
		return null;
	}
	
	/// <summary>
	/// Get Random range, when random value is in success, return true
	/// </summary>
	/// <param name="successRange_"></param>
	/// <returns></returns>
	public static bool IsSuccessInRandom(float successRange_)
	{
		var isSuccess = Random.Range(0, 101) <= successRange_;
		return isSuccess;
	}
}
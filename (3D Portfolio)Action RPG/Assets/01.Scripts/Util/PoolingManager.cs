﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(GameObjectPool))]
public class PoolingManager : DontDestroy<PoolingManager>
{
    [SerializeField]
    protected PoolingObject[] effectPrefabs;

    // -------------------------------------------- //
    protected GameObjectPool objectPool;

    protected override void OnAwake()
    {
        base.OnAwake();
        objectPool = GetComponent<GameObjectPool>();
    }

    public virtual void Create(string key_,Vector3 createPosition_, Quaternion createRotation_,Vector3 size_,Transform createParent_,float showTime_)
    {
        objectPool.Create(key_, createPosition_, createRotation_, size_, showTime_, createParent_);
    }
    public virtual void Create(string key_,Vector3 createPosition_, Quaternion createRotation_,Vector3 size_,float showTime_)
    {
        objectPool.Create(key_, createPosition_, createRotation_,size_,showTime_);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameObjectPool : MonoBehaviour
{
    private readonly Dictionary<string, List<PoolingObject>> _container 
        = new Dictionary<string, List<PoolingObject>>();
    private Dictionary<string,int> _count = new Dictionary<string, int>();
    private readonly Dictionary<string,PoolingObject> _prefabDictionary = new Dictionary<string, PoolingObject>();


    /// <summary>
    /// Initialize Object Pool
    /// </summary>
    /// <param name="key_">To initialized Key</param>
    /// <param name="resource_">Resource of Pool</param>
    public void InitPool(string key_,PoolingObject resource_)
    {
        _prefabDictionary.Add(key_,resource_);

        var obj = Instantiate(resource_).GetComponent<PoolingObject>();
        obj.transform.SetParent(transform);
        var list = new List<PoolingObject> { obj };
        _container.Add(key_,list);
        _count.Add(key_,0);
    }

    /// <summary>
    /// Pushing Pool
    /// </summary>
    /// <returns>Pushed Object</returns>
    public PoolingObject Create(string key_,Vector3 createPosition_,Quaternion createRotation_,Vector3 size_,float showTime_,Transform createParent_)
    {
        if(_count[key_] > _container[key_].Count -1)
        {
            if(_container[key_][0].isCanPlay)
            {
                CreateNewObject(key_);
            }
            else
            {
                _count[key_] = 0;
            }
        }
        _container[key_][_count[key_]].Set(createPosition_,createRotation_,transform,showTime_);
        _container[key_][_count[key_]].transform.localScale = size_;
        _container[key_][_count[key_]].transform.SetParent(createParent_);
        _count[key_]++;
        return _container[key_][_count[key_] - 1];
    }
    /// <summary>
    /// Pushing Pool
    /// </summary>
    /// <returns>Pushed Object</returns>
    public PoolingObject Create(string key_,Vector3 createPosition_,Quaternion createRotation_,Vector3 size_,float showTime_)
    {
        if(_count[key_] > _container[key_].Count -1)
        {
            if(_container[key_][0].gameObject.activeSelf)
            {
                CreateNewObject(key_);
            }
            else
            {
                _count[key_] = 0;
            }
        }

        _container[key_][_count[key_]].Set(createPosition_, createRotation_,transform ,showTime_);
        _container[key_][_count[key_]].transform.localScale = size_;
        _count[key_]++;
        return _container[key_][_count[key_] - 1];
    }

    private void CreateNewObject(string key_)
    {
        var obj = Instantiate(_prefabDictionary[key_]).GetComponent<PoolingObject>();
        obj.transform.SetParent(transform);
        _container[key_].Add(obj);
    }
}
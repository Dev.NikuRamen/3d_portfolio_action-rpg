﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class TweenMove : MonoBehaviour
{
    [SerializeField]
    private AnimationCurve curve = AnimationCurve.Linear(0, 0, 1, 1);

    public Vector3 from;
    public Vector3 to;
    public float duration;
    private float _time = 0f;
    private float _endTime = 0f;
    private bool _isMove;
    private CharacterController _controller;
    private IEnumerator _tweenCoroutine; // Tween coroutine hooking

    private NavMeshAgent _navAgent;
    // Start is called before the first frame update
    private void Start()
    {
        _navAgent = GetComponent<NavMeshAgent>();
        _controller = GetComponent<CharacterController>();
        _endTime = curve.keys[curve.keys.Length - 1].time;
        _isMove = false;
    }
    /// <summary>
    /// Start Tween move
    /// </summary>
    public void PlayForward()
    {
        if ( _tweenCoroutine != null )
        {
            StopCoroutine(_tweenCoroutine);
        }
        if ( gameObject.activeSelf )
        {
            _tweenCoroutine = StartTweenProcess();
            StartCoroutine(_tweenCoroutine);
        }
    }
    
    /// <summary>
    /// Tween move Process
    /// </summary>
    private IEnumerator StartTweenProcess()
    {       
        _isMove = true;
        _time = 0f;
        while (true)
        {
            if (_isMove)
            {
                _time += Time.deltaTime / duration;
                var moveDir = Vector3.Lerp(from, to, curve.Evaluate(_time));
                if (_controller != null)
                {
                    _controller.Move(moveDir - transform.position);
                }
                else if(_navAgent != null && _navAgent.enabled)
                {
                    _navAgent.Move(moveDir - transform.position);
                }
                else
                {
                    transform.position = moveDir;
                }
                if (_time >= _endTime)
                {
                    _isMove = false;
                    _time = 0f;                    
                    yield break;
                }
            }
            yield return null;
        }
    }
}

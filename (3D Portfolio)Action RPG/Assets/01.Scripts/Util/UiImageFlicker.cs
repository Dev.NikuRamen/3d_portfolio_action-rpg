﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class UiImageFlicker : MonoBehaviour
{
    [SerializeField] [Range(0,2f)]
    private float flickingSpeed;
    
    private Image _flickingImage;
    private Color _imageColor;
    private float _alphaValue;
    private bool _isIncrease;

    private void Awake()
    {
        _flickingImage = GetComponent<Image>();
        _imageColor = _flickingImage.color;
        _alphaValue = _imageColor.a;
        if ( _alphaValue >= .5f )
            _isIncrease = false;
    }

    private void Update()
    {
        if ( _isIncrease )
        {
            _alphaValue += Time.deltaTime * flickingSpeed;
            _imageColor.a = _alphaValue;
            _flickingImage.color = _imageColor;
            if ( _alphaValue >= 1f )
            {
                _isIncrease = false;
            }
        }
        else
        {
            _alphaValue -= Time.deltaTime * flickingSpeed;
            _imageColor.a = _alphaValue;
            _flickingImage.color = _imageColor;
            if ( _alphaValue <= 0f )
            {
                _isIncrease = true;
            }
        }
    }
}

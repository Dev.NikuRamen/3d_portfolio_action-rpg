﻿using System.Collections.Generic;
using System.Text;
using System.Xml;
using UnityEngine;

public class XMLController
{
    /// <summary>
    /// Load XML
    /// </summary>
    /// <param name="filePath_">file exist path</param>
    /// <param name="strStartElement_">Xml header string</param>
    /// <param name="strKeys_">Search key</param>
    /// <returns>dictionary data</returns>
    public static Dictionary<string, string> LoadData(string filePath_,string strStartElement_,string[] strKeys_)
    {
        var dataDictionary = new Dictionary<string, string>(); // Load Data
        var stringBuilder = new StringBuilder();
#if UNITY_EDITOR || UNITY_STANDALONE_WIN
        var fileName = Application.streamingAssetsPath + '/' + filePath_+".xml"; // Use StreamingAsset folder
#endif
        using (var reader = XmlReader.Create(fileName))
        {
            // Read all data
            while( reader.Read() )
            {
                if ( reader.Name.Equals(strStartElement_) )
                {
                    reader.Read();
                    foreach( var key in strKeys_ )
                    {
                        stringBuilder.Clear();
                        stringBuilder.Append(reader.ReadElementString(key, ""));
                        dataDictionary.Add(key, stringBuilder.ToString());
                    }

                }
            }
        }

        return dataDictionary;
    }

    /// <summary>
    /// Create Xml file 
    /// </summary>
    /// <param name="fileName_">Save file name</param>
    /// <param name="xmlDictionary_">Save Data</param>
    /// <param name="strStartElement_">Xml start element</param>
    /// <param name="strKeys_">Save data's key</param>
    /// <param name="strAttribute_">Xml attribute</param>
    /// <param name="strAttributeValue_">Xml attribute value</param>
    public static void CreateXmlFile(string fileName_, Dictionary<string, string> xmlDictionary_,
                                     string strStartElement_,  string[] strKeys_,string strAttribute_ = "", string strAttributeValue_ = "")
    {
        // None file Exception
        if ( fileName_ == null || xmlDictionary_.Count <= 0 )
        {
            DEBUG.Log("FileName : " + (string.IsNullOrEmpty(fileName_) ? fileName_ : "NULL") +
                      "Data : " + (xmlDictionary_.Count <= 0 ? xmlDictionary_.ToString() : "Empty"),DEBUG.DebugMode.Error);
            return;
        }
#if ( UNITY_EDITOR || UNITY_STANDALONE_WIN )
        // Set save file path
        var filePath = Application.streamingAssetsPath + "/" + fileName_ + ".xml";
#endif
        // instancing xml writer
        using (var writer = XmlWriter.Create(filePath))
        {
            // Write start
            writer.WriteStartDocument();

            writer.WriteStartElement(strStartElement_);
            
            // Attribute option
            if(!string.IsNullOrEmpty(strAttribute_) && !string.IsNullOrEmpty(strAttributeValue_))
                writer.WriteAttributeString(strAttribute_,strAttributeValue_);

            foreach( var key in strKeys_ )
            {
                // Write data
                writer.WriteElementString(key, xmlDictionary_[key]);
            }
            writer.WriteEndElement();
            
            // Write end
            writer.WriteEndDocument();
        }
    }
}


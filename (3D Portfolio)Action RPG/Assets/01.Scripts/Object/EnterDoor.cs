﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider))]
[RequireComponent(typeof(Rigidbody))]
public class EnterDoor : Door
{
	private void OnTriggerEnter(Collider other)
	{
		if ( other.tag.Equals("Player") )
		{
			if ( !AlReadySawCutScene )
			{
				StartCoroutine(other.gameObject
				                    .GetComponent<PlayerController>().StartOnCutScene(
					                     ()=>
						                     ActionCamera.
							                     ActiveCutScene(
								                     nameof(ActionCamera.CutSceneIndex.OnEnterDoorEnter))));
				AlReadySawCutScene = true;
			}
		}
	}
	
}

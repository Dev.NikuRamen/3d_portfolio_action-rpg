﻿using System;
using UnityEngine;

public class ExitDoor : Door
{
	private void OnTriggerEnter(Collider other)
	{
		if(other.CompareTag("Player"))
		{
			StartCoroutine(GameManager.Instance.StartLoadAsync(Level.Instance.getNextLevelSceneName));
		}
	}
}

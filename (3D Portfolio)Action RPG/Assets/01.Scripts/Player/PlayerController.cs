﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]
public class PlayerController : Human
{
    /// <summary>
    /// Animation parameter name
    /// </summary>
    private enum AnimationName
    {
        SpeedBlend,
        AttackEnd,
        Attack1,
        Attack2,
        AttackStart,
        Attack2Start,
        Death,
        IsBattle,
        StartJump,
        Land,
        Hit_int,
        Hit,
        Evasion,
        EvasionFinish
    }
    private enum InputCode
    {
        NONE = -1,
        Attack1 = 0,
        Attack2 = 1,
        Evasion = 2,
        Move = 3
    }

    public State PlayerState => state;

    [Header("Player Cache Component")]
    [SerializeField]
    private StaminaBar staminaBar;
    [Header("Player value Parameter")]
    [SerializeField] [Range(0, 100)]
    private int staminaRepairDamp = 10;
    [Header("Player Audio Clip")]
    [SerializeField]
    private AudioClip evasionSFX;

    // ------------------------------------------------- //
    private CharacterController _characterController;
    private Vector3 _moveDirection = Vector3.zero; // Character move direction
    private readonly Stack<InputCode> _inputStack = new Stack<InputCode>();
    private IEnumerator _repairCoroutine; // Hooking repair coroutine
    private Ray _groundRay;
    private const string _SFX_FILE_PATH = "SFX/Player";
    private StringBuilder _filePath = new StringBuilder();

    #region Unity Process

    protected override void OnAwake()
    {
        StateData = XMLController.LoadData("PlayerData", "Status", StatusName);
        base.OnAwake();
        // Init component
        _characterController = GetComponent<CharacterController>();
        DEBUG.Log("PLAYER-AWAKE");
        hpProgressBar = GameManager.Instance.getPlayerHpGaugeBar;
        staminaBar = GameManager.Instance.getStaminaBar;
        hpProgressBar.SetHpGauge(CurrentHp, MaxHp);
        staminaBar.SetUi(MaxStamina);
    }
    
    /// <summary>
    /// When Game start, wait the fade - out
    /// </summary>
    /// <returns>.6f Sec</returns>
    protected override IEnumerator StartOnStart()
    {
        SetState(State.CutScene);
        yield return StartCoroutine(GameManager.Instance.StartFadeOut(.6f));
        SetState(State.Idle);
    }

    protected override void OnUpdate()
    {
        base.OnUpdate();
        switch( state )
        {
            case State.Idle:
                Move();
                if ( IsBattle )
                {
                    SetState(State.Battle);
                }
                break;
            case State.CutScene:
                break;
            case State.Battle:
                if ( CurrentHp <= 0 )
                {
                    SetDie();
                }
                if ( IsBattle )
                {
                    if ( Input.GetMouseButtonDown(0) )
                    {
                        AttackStart(nameof( HumanDataManager.DataKeyName.Player ), (int) InputCode.Attack1);
                        return;
                    }
                    else if ( Input.GetMouseButtonDown(1) )
                    {
                        AttackStart(nameof( HumanDataManager.DataKeyName.Player ), (int) InputCode.Attack2);
                        return;
                    }
                }
                else
                {
                    IsBattle = true;
                    Animator.SetBool(nameof(AnimationName.IsBattle),true);
                }

                Move();
                break;
            case State.Attack:
                if ( Input.GetMouseButtonDown(0) )
                {
                    _inputStack.Push(InputCode.Attack1);
                }
                else if ( Input.GetMouseButtonDown(1) )
                {
                    _inputStack.Push(InputCode.Attack2);
                }
                else if ( Input.GetButtonDown("Jump") )
                {
                    _inputStack.Push(InputCode.Evasion);
                }
                break;
            case State.Evasion:
                break;
            case State.Hit:
                break;
            case State.Die:
                break;
            default:
                break;
        }

        var myTransform = transform;
        _groundRay = new Ray(myTransform.position,Vector3.down);
        if ( Physics.Raycast(_groundRay, out var hit, 10, LayerMask.NameToLayer("Ground")) )
        {
            var position = myTransform.position;
            position.y = hit.point.y;
            myTransform.position = position;
        }
    }

    // End of Unity Process
    #endregion
    
    #region Action Process

    
    /// <summary>
    /// Character move
    /// Use character controller
    /// </summary>
    protected override void Move()
    {
        // If input jump key, evasion
        if ( Input.GetButtonDown("Jump") )
        {
            Evade();
            return;
        }
        var viewDirection = GetViewDirection();
        // Character is ground
        if ( _characterController.isGrounded )
        {
            var axis = GetMoveAxisToAbs();
            _moveDirection = viewDirection;
            // Translate local pos 2 world pos
            _moveDirection *= movementSpeed;
            Animator.SetFloat(nameof(AnimationName.SpeedBlend), axis);
        }
        // Set gravity
        _moveDirection += GRAVITY_SCALE * Time.deltaTime * Vector3.down;
        
        // Rotate player to move direction
        if ( viewDirection != Vector3.zero )
        {
            transform.rotation = Quaternion.LookRotation(viewDirection);
        }
        
        // Character Move
        _characterController.Move(_moveDirection * Time.deltaTime);
    }

    protected override void Evade()
    {
        if(state == State.Evasion)
            return;
        if ( CurrentStamina - evadeStamina <= 0 )
        {
            return;
        }
        if ( _repairCoroutine != null ) // Null Function pointer exception
        {
            StopCoroutine(_repairCoroutine); // Stop last hooked coroutine
        }

        
        CurrentStamina -= evadeStamina;
        // Set UI
        staminaBar.ReduceStamina(evadeStamina);
        // Evasion move
        var viewDirection = GetViewDirection();
        if ( viewDirection == Vector3.zero )
        {
            viewDirection = transform.forward;
        }

        transform.rotation = Quaternion.LookRotation(viewDirection);
        Animator.SetTrigger(nameof( AnimationName.Evasion )); // Play evasion animation
        SetState(State.Evasion);
        AudioSource.PlayOneShot(evasionSFX);
        _repairCoroutine = StartRepairStamina(); 
        StartCoroutine(_repairCoroutine); // Repair stamina after 2 sec
    }
    
    /// <summary>
    /// When evasion, move at view direction
    /// </summary>
    protected override void OnEvasionMove()
    {
        var viewDirection = GetViewDirection();
        var position = transform.position;
        if ( viewDirection == Vector3.zero )
            viewDirection = transform.forward;
        // Evasion move
        TweenMove.from = position;
        TweenMove.to = position + viewDirection * evasionSpeedmult;
        TweenMove.duration = 1f;
        TweenMove.PlayForward();
        SetState(State.Evasion);
    }

    protected override void OnEvasionFinish()
    {
        base.OnEvasionFinish();
        Animator.SetTrigger(nameof(AnimationName.EvasionFinish));
    }

    public override void OnEscape()
    {
        
    }

    protected override void AttackStart(string objectName_,int attackIndex_)
    {
        if ( attackIndex_ == (int) InputCode.Attack1 )
        {
            Animator.SetTrigger(nameof( AnimationName.AttackStart ));
            AttackIndex = 1; // Left First
        }
        else if ( attackIndex_ == (int) InputCode.Attack2 )
        {
            Animator.SetTrigger(nameof(AnimationName.Attack2Start));
            AttackIndex = 11;
        }
        IsBattle = true;
        Animator.SetBool(nameof(AnimationName.IsBattle),IsBattle);
        Animator.SetFloat(nameof(AnimationName.SpeedBlend),0f);
        SetState(State.Attack);
        var data = HumanDataManager.Instance.GetData(objectName_,AttackIndex.ToString());
        var boxIndex = int.Parse(data.hitBox);
        var hitUnit = hitBoxAreas[boxIndex].GetFindUnits();
        foreach( var unit in hitUnit )
        {
            unit.GetParentHuman().OnAttackerAttackStart();
        }
    }

    protected override void SetDie()
    {
        base.SetDie();
        AudioSource.PlayOneShot(dieSfx);
        Animator.SetTrigger(nameof(AnimationName.Death));
        StartCoroutine(GameManager.Instance.StartPlayerDieShow(.4f));
    }

    // End of Action Process 
    #endregion
    
    #region Event Method

    
    private void OnChangeView()
    {
        var direction = GetViewDirection();
        if(direction != Vector3.zero)
            transform.rotation = Quaternion.LookRotation(direction);
    }

    private void OnInputEnd()
    {
        if ( state != State.Attack )
            return;
        if ( _inputStack.Count <= 0 )
            return;
        var firstInput = _inputStack.Pop(); 
        switch( firstInput )
        {
            case InputCode.NONE:
                break;
            case InputCode.Attack1:
                Animator.SetTrigger(nameof(AnimationName.Attack1));
                break;
            case InputCode.Attack2:
                Animator.SetTrigger(nameof(AnimationName.Attack2));
                break;
            case InputCode.Evasion:
                Evade();
                break;
            case InputCode.Move:
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
        _inputStack.Clear();
    }

    public override void OnHit(Transform attackerTransform_, int attackIndex_,float damage_)
    {
        base.OnHit(attackerTransform_, attackIndex_,damage_);
        if ( state == State.Hit )
        {
            Animator.SetTrigger(nameof( AnimationName.Hit ));
            var data = HumanDataManager.Instance.GetData(nameof( HumanDataManager.DataKeyName.OneHandSword ),
                                                          attackIndex_.ToString());
            if ( int.Parse(data.isKnockDown) != 0 )
            {
                Animator.SetInteger(nameof( AnimationName.Hit_int ), (int) HitIndex.FallDown);
                AudioSource.PlayOneShot(fallDonwClip);
            }
            else
            {
                Animator.SetInteger(nameof( AnimationName.Hit_int ), (int) GetDirectionAsTarget(attackerTransform_));
                AudioSource.PlayOneShot(hitSfx);
            }
        }
    }

    protected override void OnAttack()
    {
       AttackProcess(nameof( HumanDataManager.DataKeyName.Player ));
    }


    protected override void OnAttackFinish()
    {
        Animator.SetFloat(nameof(AnimationName.SpeedBlend),0);
        base.OnAttackFinish();
        Animator.SetTrigger(nameof(AnimationName.AttackEnd));
        Move();
    }

    public void OnEndOfEnterCutScene()
    {
        Animator.SetBool(nameof(AnimationName.IsBattle),true);
    }
    
    public IEnumerator StartOnCutScene(Level.CutSceneDelegate event_)
    {
        SetState(State.CutScene);
        Animator.SetFloat(nameof(AnimationName.SpeedBlend),0);
        yield return StartCoroutine(Level.Instance.OnCutScene(event_));
    }
    
    protected override void OnAttackStart(int attackIndex_)
    {
        base.OnAttackStart(attackIndex_);
        var data = HumanDataManager.Instance.GetData(nameof( HumanDataManager.DataKeyName.Player ), attackIndex_.ToString());
        if ( !AttackAudioClips.ContainsKey(data.attackSFX) )
        {
            _filePath.Append(_SFX_FILE_PATH + "/Attack/" + data.attackSFX);
            var clip = Resources.Load<AudioClip>(_filePath.ToString());
            AttackAudioClips.Add(data.attackSFX,clip);
            _filePath.Clear();
        }
        AudioSource.PlayOneShot(AttackAudioClips[data.attackSFX]);
    }

    // End of event method
    #endregion

    #region Public Method

    public void SetNonBattle()
    {
        SetState(State.Idle);
        IsBattle = false;
        Animator.SetBool(nameof(AnimationName.IsBattle),false);
    }

    
    // End of Public Method
    #endregion
    
    
    #region Private Method

    /// <summary>
    /// Get player view direction
    /// </summary>
    /// <returns>Normalized direction</returns>
    private Vector3 GetViewDirection()
    {
        var hor = Input.GetAxisRaw("Horizontal");
        var ver = Input.GetAxisRaw("Vertical");
        var forward = actionCamera.transform.forward;
        forward.y = 0;
        forward.Normalize();
        var right = actionCamera.transform.right;
        right.y = 0;
        right.Normalize();
        var viewDirection = (hor * right + ver * forward).normalized;
        return viewDirection;
    }


    private float GetMoveAxisToAbs()
    {
        var hor = Input.GetAxisRaw("Horizontal");
        var ver = Input.GetAxisRaw("Vertical");
        var axis = hor > 0 ? hor : -hor;
        axis += ver > 0 ? ver : -ver;
        return axis;
    }
    
    /// <summary>
    /// Stamina repair coroutine
    /// </summary>
    /// <returns>Every end of Frame</returns>
    private IEnumerator StartRepairStamina()
    {
        yield return Util.YieldInstructionOnCache.WaitForSeconds(2f);
        while( CurrentStamina <= MaxStamina )
        {
            CurrentStamina += Time.deltaTime * staminaRepairDamp;
            staminaBar.OnChangedUi(CurrentStamina);
            yield return Util.YieldInstructionOnCache.WaitForEndOfFrame;
        }
    }
    
    // End of Private Method
    #endregion
}

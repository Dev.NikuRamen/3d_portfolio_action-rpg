﻿using UnityEngine;

public class Level01Data : LevelData
{
	
	// ------------------------ //
	
	
	private void Awake()
	{
		CutSceneEventQueue.Enqueue(() => enterDoor.OnOpen());
		CutSceneEventQueue.Enqueue(() => exitDoor.OnOpen());
	}
}

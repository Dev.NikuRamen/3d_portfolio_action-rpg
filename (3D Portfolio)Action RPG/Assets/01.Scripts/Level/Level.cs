﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LevelData))]
[RequireComponent(typeof(AudioSource))]
public class Level : Singleton<Level>
{
    public delegate void CutSceneDelegate();
    [Header("View Camera's Component")]
    [SerializeField] 
    private ActionCamera actionCamera;
    [Header("SFX Audio Clip")]
    [SerializeField]
    private AudioClip bgmClip;

    [SerializeField]
    private InGameQuitUi inGameQuitUi;
    public string getLevelName => _levelData.getLevelName;
    public Loading.SceneName getNextLevelSceneName => _levelData.getNextLevelName;
    // ------------------------------------------ //
    private LevelData _levelData;
    private AudioSource _audioSource;
    private bool _isPlayingBgm;
    private bool _isGameQuitUiOpen;
    
    #region Unity Process
    protected override void OnAwake()
    {
        _levelData = GetComponent<LevelData>();
        _audioSource = GetComponent<AudioSource>();
        _isGameQuitUiOpen = false;
    }

    private void Update()
    {
#if UNITY_EDITOR || UNITY_STANDALONE
        // Exit Game
        if ( Input.GetButtonDown("Cancel") )
        {
            _isGameQuitUiOpen = !_isGameQuitUiOpen;
            inGameQuitUi.gameObject.SetActive(_isGameQuitUiOpen);
            if ( _isGameQuitUiOpen )
            {
                GameManager.UnLockCursor();
            }
            else
            {
                GameManager.LockCursor();
            }
        }
#endif
    }

    // End of Unity Process
    #endregion

    #region Public Method
    
    /// <summary>
    /// Event, when player encounter the enemy
    /// </summary>
    public void OnBattleStart()
    {
        OnCloseEnterDoor();
        PlayBattleBGM();
    }
    
    /// <summary>
    /// When CutScene, Fade-Out and play CutScene animation
    /// </summary>
    public IEnumerator OnCutScene(CutSceneDelegate event_)
    {
        // Set cutScene event
        actionCamera.onCutSceneEvent = _levelData.OnDequeueDoorEvent();
        // Fade - In
        yield return StartCoroutine(GameManager.Instance.StartFadeIn());
        // Camera move to animation start transform
        yield return Util.YieldInstructionOnCache.WaitForSeconds(1f);
        actionCamera.SetState(ActionCamera.CameraState.CutScene);
        event_.Invoke();
        yield return StartCoroutine(GameManager.Instance.StartFadeOut());
    }
    
    /// <summary>
    /// Fade - out Bgm
    /// </summary>
    public IEnumerator StartFadeOutBgm()
    {
        var volume = _audioSource.volume;
        while( volume>=0 )
        {
            volume -= Time.deltaTime;
            _audioSource.volume = volume;
            yield return Util.YieldInstructionOnCache.WaitForEndOfFrame;
        }
    }
    
    // End of Public Method
    #endregion

    #region Private Method

    private void OnCloseEnterDoor()
    {
        _levelData.getEnterDoor.OnClose();
    }
    
    private void PlayBattleBGM()
    {
        if ( !_isPlayingBgm ) 
        {
                _isPlayingBgm = true;
                _audioSource.loop = true;
                _audioSource.clip = bgmClip;
                _audioSource.Play();
        }
    }

    #endregion
}

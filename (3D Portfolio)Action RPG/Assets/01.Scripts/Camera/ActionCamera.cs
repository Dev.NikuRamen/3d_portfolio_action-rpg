﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class ActionCamera : MonoBehaviour
{
	public enum CameraState
	{
		FollowTarget,
		Shake,
		CutScene
	}
    
    public enum CutSceneIndex
    {
	    OnEnterDoorEnter = 0,
	    OnMonsterDie_CutScene,
    }

	public delegate void CameraEventDelegate();
	
    #region Members

    [Header("Camera movement parameters")]
    
	[Tooltip("X roll rotate max value")]
	[SerializeField] [Range(0,90)]
	private float xRotationMax;
	[Tooltip("Mouse sensitivity")]
	[SerializeField] [Range(10,100)]
	private float rotateSpeed;
	[Tooltip("Camera position offset")]
	[SerializeField] 
	private Vector2 viewDamp;
	[SerializeField] [Range(0,100)]
	private int cameraSpeed = 3;
	[SerializeField][Range(1,10)]
	private float distance; // Distance between camera and view target
	
	[Header("Transform Cache")]
	[SerializeField]
	private Transform groundCheckerTransform;
	[SerializeField]
	private Transform playerTransform;
	[Space]
	public CameraState state;
	public CameraEventDelegate onCutSceneEvent;
	// ---------------------------------------- // 
	private Vector2 _rotate; // Camera rotation as view target
	private Vector3 _viewPosition; // Camera look point
	private Vector3 _viewDirection; // Target direction 
	private Vector3 _currentPosition;
	private PlayerController _playerController;
	private Animator _animator;
	private bool _isReverse;
	
	// End of Members
	#endregion

	#region Unity Process
	
	private void Awake()
	{
		_currentPosition = transform.position;
		_playerController = playerTransform.GetComponent<PlayerController>();
		_animator = GetComponent<Animator>();
		_animator.enabled = false;
		GameManager.LockCursor();
		SetCameraSensitivity(GameManager.Instance.GetMouseSensitivity());
		SetReverse(GameManager.Instance.GetMouseReverse());
	}

	private void Update()
	{
		switch( state )
		{
			case CameraState.FollowTarget:
#if UNITY_EDITOR || UNITY_STANDALONE
				CameraMoveWithMouse();
#endif
				CheckGround();
				break;
			case CameraState.Shake:
				break;
			case CameraState.CutScene:
				break;
			default:
				throw new ArgumentOutOfRangeException();
		}

	}

	private void LateUpdate()
	{
		switch( state )
		{
			case CameraState.FollowTarget:
				transform.position = _currentPosition;
				transform.LookAt(playerTransform.position + (Vector3)viewDamp);
				break;
			case CameraState.Shake:
				break;
			case CameraState.CutScene:
				break;
			default:
				throw new ArgumentOutOfRangeException();
		}
	}
	
	// End of Unity Process
	#endregion

	#region Event Method

	public void OnEndOfCutScene()
	{
		_playerController.OnEndOfEnterCutScene();
		SetState(CameraState.FollowTarget);
		_animator.enabled = false;
	}

	private void OnEndOfMonsterDieCutScene()
	{
		_playerController.SetNonBattle();
		SetState(CameraState.FollowTarget);
	}
	
	/// <summary>
	/// CutScene event
	/// </summary>
	public void OnActionCameraEvent()
	{
		onCutSceneEvent.Invoke();
	}

	#endregion
	
	#region Public Method

	/// <summary>
	/// Start CutScene Camera
	/// </summary>
	/// <param name="animationName_">CutScene animation trigger name</param>
	public void ActiveCutScene(string animationName_)
	{
		SetState(CameraState.CutScene);
		_animator.enabled = true;
		_animator.SetTrigger(animationName_);
	}

	
	public void SetState(CameraState state_)
	{
		state = state_;
	}
	
	/// <summary>
	/// Set Mouse sensitivity
	/// </summary>
	/// <param name="value_">Sensitivity</param>
	public void SetCameraSensitivity(int value_)
	{
		cameraSpeed = value_;
	}
	
	/// <summary>
	/// Set Mouse Reverse
	/// </summary>
	/// <param name="value_">Reverse</param>
	public void SetReverse(bool value_)
	{
		_isReverse = value_;
	}
	
	// End of Public Method
	#endregion

	#region Private Method

	/// <summary>
	/// Camera move in PC.
	/// </summary>
	private void CameraMoveWithMouse()
	{
		_viewPosition = playerTransform.localPosition + new Vector3(viewDamp.x, viewDamp.y);

		// Get User input
		_rotate.x += Input.GetAxis("Mouse Y") * rotateSpeed * Time.deltaTime;
		// If camera option, reverse is true
		if ( _isReverse )
		{
			_rotate.y -= Input.GetAxis("Mouse X") * rotateSpeed * Time.deltaTime;
		}
		else
		{
			_rotate.y += Input.GetAxis("Mouse X") * rotateSpeed * Time.deltaTime;
		}
		
		if ( _playerController.PlayerState == Human.State.Idle )
		{
			_rotate.y -= Input.GetAxis("Horizontal") * .5f;
		}

		// Set max value
		_rotate.x = Mathf.Clamp(_rotate.x, -xRotationMax, xRotationMax);

		// Calculate
		_viewDirection = Quaternion.Euler(-_rotate.x, _rotate.y, 0f) * Vector3.forward;
		//transform.position = _viewPosition + _viewDirection * -_distance;
		_currentPosition = Vector3.Lerp(_currentPosition, _viewPosition + _viewDirection * -distance,
		                                cameraSpeed * Time.deltaTime);
	}

	private void CheckGround()
	{
		// Set horizontal as ground
		var cameraRotation = transform.rotation;
		groundCheckerTransform.rotation = Quaternion.Euler(-cameraRotation.x, -cameraRotation.y,
		                                                   -cameraRotation.z);
		
		var groundRay = new Ray(transform.position,Vector3.down);
		if ( Physics.Raycast(groundRay,out var groundHit, 10f, 1 << LayerMask.NameToLayer("Ground")) )
		{
			// If is close at ground, lock camera y position
			if ( groundHit.distance <= .1f )
			{
				_currentPosition.y += .1f;
			}
		}
	}

	// End of Private Method
	#endregion
}

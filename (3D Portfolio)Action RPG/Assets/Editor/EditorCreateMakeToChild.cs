﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class EditorCreateMakeToChild : EditorWindow
{
	private GameObject _addObjectPrefab;
	private GameObject _selectObject;

	[MenuItem("Edit Object/Hitable object")]
	private static void OnEditorShow()
	{
		var window = GetWindowWithRect<EditorCreateMakeToChild>(new Rect(600,800,600,200));
		window.ShowPopup();
	}

	private void OnGUI()
	{
		_addObjectPrefab = (GameObject)EditorGUILayout.ObjectField("Create to child Object", _addObjectPrefab, typeof
			                                                           (GameObject), true);
		_selectObject = (GameObject) EditorGUILayout.ObjectField("To Create Object", _selectObject, typeof(GameObject),
		                                                         true);

		if ( GUILayout.Button("Create") )
		{
			Create();
		}
	}

	private void Create()
	{
		if(!_addObjectPrefab || !_selectObject)
			return;
		//Instantiate(_addObjectPrefab, _selectObject.transform);
		PrefabUtility.InstantiatePrefab(_addObjectPrefab, _selectObject.transform);	// 자식연결을 끊지않고 동적생성
	}
}

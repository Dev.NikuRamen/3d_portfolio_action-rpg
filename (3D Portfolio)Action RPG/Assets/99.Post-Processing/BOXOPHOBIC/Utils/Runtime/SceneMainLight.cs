﻿// Utils
// Cristian Pop - https://boxophobic.com/

using UnityEngine;
using Boxophobic;
#if UNITY_EDITOR
using UnityEditor;
#endif

#if UNITY_EDITOR
[HelpURL("https://docs.google.com/document/d/1PG_9bb0iiFGoi_yQd8IX0K3sMohuWqyq6_qa4AyVpFw/edit#heading=h.b6n5ylhvvzzk")]
[DisallowMultipleComponent]
[ExecuteInEditMode]
#endif
public class SceneMainLight : MonoBehaviour
{
    

    public enum UpdateMode
    {
        Static = 0,
        Dynamic = 1,
    };

    [BCategory("Update")]
    public int category_Update;

    public UpdateMode updateMode = UpdateMode.Static;

    [BCategory("Main Light")]
    public int category_Motion;

    public Light mainLight;

    void Start()
    {
        // Set gameobject name to be searchable
        gameObject.name = "Scene Main Light";

        // Send global information to shaders
        SetGlobalShaderProperties();

    }


    void Update()
    {

    }
    // Send global information to shaders
    void SetGlobalShaderProperties()
    {

        // Send Motion parameters to shaders
        Shader.SetGlobalVector("B_SceneMainLightDirection", gameObject.transform.forward);
        Shader.SetGlobalColor("B_SceneMainLightColor", gameObject.GetComponent<Light>().color);

    }
}

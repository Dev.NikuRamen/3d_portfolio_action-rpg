// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Hidden/BOXOPHOBIC/Height Fog Volume"
{
	Properties
	{
		[BBanner(Volume Height Fog)]_ADSSimpleLitGrass("< ADS Simple Lit Grass >", Float) = 1
		[HideInInspector]_FogIntensity("Fog Intensity", Range( 0 , 1)) = 1
		_FogColor("Fog Color", Color) = (0.4411765,0.722515,1,0)
		_FogDistanceStart("Fog Distance Start", Float) = 0
		_FogDistanceEnd("Fog Distance End", Float) = 30
		_FogHeightStart("Fog Height Start", Float) = 5
		_FogHeightEnd("Fog Height End", Float) = 0
		_SkyboxFogHeight("Skybox Fog Height", Range( 0 , 1)) = 1
		_SkyboxFogFill("Skybox Fog Fill", Range( 0 , 1)) = 0
		_FogDirectionalColor("Fog Directional Color", Color) = (1,0.6300203,0.1617647,0)
		_NoiseIntensity("Noise Intensity", Range( 0 , 1)) = 0.5
		_NoiseDistanceEnd("Noise Distance End", Float) = 10
		_NoiseScale("Noise Scale", Float) = 6
		_NoiseSpeed("Noise Speed", Vector) = (0.5,0.5,0,0)
		_ZTest("_ZTest", Int) = 0
		_ZWrite("_ZWrite", Int) = 0
	}
	
	SubShader
	{
		
		
		Tags { "RenderType"="Overlay" "Queue"="Overlay" }
		LOD 100

		CGINCLUDE
		#pragma target 3.0
		ENDCG
		Blend SrcAlpha OneMinusSrcAlpha
		Cull Off
		ColorMask RGBA
		ZWrite Off
		ZTest Always
		
		
		
		Pass
		{
			Name "Unlit"
			
			CGPROGRAM

#ifndef UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX
		//only defining to not throw compilation error over Unity 5.5
		#define UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input)
#endif
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_instancing
			#include "UnityCG.cginc"
			#include "UnityShaderVariables.cginc"
			#pragma multi_compile _FOGDIRECTIONALMODE_OFF _FOGDIRECTIONALMODE_ON
			#pragma multi_compile _FOGNOISEMODE_OFF _FOGNOISEMODE_PROCEDURAL3D


			struct appdata
			{
				float4 vertex : POSITION;
				float4 color : COLOR;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				
			};
			
			struct v2f
			{
				float4 vertex : SV_POSITION;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
				float4 ase_texcoord : TEXCOORD0;
			};

			uniform int _ZWrite;
			uniform half _ADSSimpleLitGrass;
			uniform int _ZTest;
			uniform half4 _FogColor;
			uniform half4 _FogDirectionalColor;
			uniform sampler2D _CameraDepthTexture;
			uniform half3 _FogDirectionalDirection;
			uniform half _FogDistanceStart;
			uniform half _FogDistanceEnd;
			uniform half _FogHeightEnd;
			uniform half _FogHeightStart;
			uniform half _NoiseScale;
			uniform half3 _NoiseSpeed;
			uniform half _NoiseDistanceEnd;
			uniform half _NoiseIntensity;
			uniform half _SkyboxFogHeight;
			uniform half _SkyboxFogFill;
			uniform half _FogIntensity;
			float3 mod3D289( float3 x ) { return x - floor( x / 289.0 ) * 289.0; }
			float4 mod3D289( float4 x ) { return x - floor( x / 289.0 ) * 289.0; }
			float4 permute( float4 x ) { return mod3D289( ( x * 34.0 + 1.0 ) * x ); }
			float4 taylorInvSqrt( float4 r ) { return 1.79284291400159 - r * 0.85373472095314; }
			float snoise( float3 v )
			{
				const float2 C = float2( 1.0 / 6.0, 1.0 / 3.0 );
				float3 i = floor( v + dot( v, C.yyy ) );
				float3 x0 = v - i + dot( i, C.xxx );
				float3 g = step( x0.yzx, x0.xyz );
				float3 l = 1.0 - g;
				float3 i1 = min( g.xyz, l.zxy );
				float3 i2 = max( g.xyz, l.zxy );
				float3 x1 = x0 - i1 + C.xxx;
				float3 x2 = x0 - i2 + C.yyy;
				float3 x3 = x0 - 0.5;
				i = mod3D289( i);
				float4 p = permute( permute( permute( i.z + float4( 0.0, i1.z, i2.z, 1.0 ) ) + i.y + float4( 0.0, i1.y, i2.y, 1.0 ) ) + i.x + float4( 0.0, i1.x, i2.x, 1.0 ) );
				float4 j = p - 49.0 * floor( p / 49.0 );  // mod(p,7*7)
				float4 x_ = floor( j / 7.0 );
				float4 y_ = floor( j - 7.0 * x_ );  // mod(j,N)
				float4 x = ( x_ * 2.0 + 0.5 ) / 7.0 - 1.0;
				float4 y = ( y_ * 2.0 + 0.5 ) / 7.0 - 1.0;
				float4 h = 1.0 - abs( x ) - abs( y );
				float4 b0 = float4( x.xy, y.xy );
				float4 b1 = float4( x.zw, y.zw );
				float4 s0 = floor( b0 ) * 2.0 + 1.0;
				float4 s1 = floor( b1 ) * 2.0 + 1.0;
				float4 sh = -step( h, 0.0 );
				float4 a0 = b0.xzyw + s0.xzyw * sh.xxyy;
				float4 a1 = b1.xzyw + s1.xzyw * sh.zzww;
				float3 g0 = float3( a0.xy, h.x );
				float3 g1 = float3( a0.zw, h.y );
				float3 g2 = float3( a1.xy, h.z );
				float3 g3 = float3( a1.zw, h.w );
				float4 norm = taylorInvSqrt( float4( dot( g0, g0 ), dot( g1, g1 ), dot( g2, g2 ), dot( g3, g3 ) ) );
				g0 *= norm.x;
				g1 *= norm.y;
				g2 *= norm.z;
				g3 *= norm.w;
				float4 m = max( 0.6 - float4( dot( x0, x0 ), dot( x1, x1 ), dot( x2, x2 ), dot( x3, x3 ) ), 0.0 );
				m = m* m;
				m = m* m;
				float4 px = float4( dot( x0, g0 ), dot( x1, g1 ), dot( x2, g2 ), dot( x3, g3 ) );
				return 42.0 * dot( m, px);
			}
			
			
			v2f vert ( appdata v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);
				UNITY_TRANSFER_INSTANCE_ID(v, o);

				float4 ase_clipPos = UnityObjectToClipPos(v.vertex);
				float4 screenPos = ComputeScreenPos(ase_clipPos);
				o.ase_texcoord = screenPos;
				
				float3 vertexValue =  float3(0,0,0) ;
				#if ASE_ABSOLUTE_VERTEX_POS
				v.vertex.xyz = vertexValue;
				#else
				v.vertex.xyz += vertexValue;
				#endif
				o.vertex = UnityObjectToClipPos(v.vertex);
				return o;
			}
			
			fixed4 frag (v2f i ) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID(i);
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(i);
				fixed4 finalColor;
				float4 screenPos = i.ase_texcoord;
				float4 ase_screenPosNorm = screenPos / screenPos.w;
				ase_screenPosNorm.z = ( UNITY_NEAR_CLIP_VALUE >= 0 ) ? ase_screenPosNorm.z : ase_screenPosNorm.z * 0.5 + 0.5;
				float4 tex2DNode159 = tex2D( _CameraDepthTexture, ase_screenPosNorm.xy );
				half Depth209 = tex2DNode159.r;
				#ifdef UNITY_REVERSED_Z
				float staticSwitch265 = ( 1.0 - Depth209 );
				#else
				float staticSwitch265 = Depth209;
				#endif
				float3 appendResult304 = (float3(ase_screenPosNorm.x , ase_screenPosNorm.y , staticSwitch265));
				float4 appendResult307 = (float4((appendResult304*2.0 + -1.0) , 1.0));
				float4 temp_output_303_0 = mul( unity_CameraInvProjection, appendResult307 );
				float4 appendResult314 = (float4(( ( (temp_output_303_0).xyz / (temp_output_303_0).w ) * half3(1,1,-1) ) , 1.0));
				float3 WorldPosition144 = (mul( unity_CameraToWorld, appendResult314 )).xyz;
				float3 normalizeResult131 = normalize( ( WorldPosition144 - _WorldSpaceCameraPos ) );
				float dotResult119 = dot( normalizeResult131 , _FogDirectionalDirection );
				float temp_output_7_0_g277 = -1.0;
				half DirectionalMask134 = ( ( dotResult119 - temp_output_7_0_g277 ) / ( 1.0 - temp_output_7_0_g277 ) );
				float4 lerpResult135 = lerp( _FogColor , _FogDirectionalColor , DirectionalMask134);
				#if defined(_FOGDIRECTIONALMODE_OFF)
				float4 staticSwitch241 = _FogColor;
				#elif defined(_FOGDIRECTIONALMODE_ON)
				float4 staticSwitch241 = lerpResult135;
				#else
				float4 staticSwitch241 = _FogColor;
				#endif
				float temp_output_7_0_g269 = _FogDistanceStart;
				half FogDistanceMask186 = saturate( ( ( distance( WorldPosition144 , _WorldSpaceCameraPos ) - temp_output_7_0_g269 ) / ( _FogDistanceEnd - temp_output_7_0_g269 ) ) );
				float temp_output_238_0 = (WorldPosition144).y;
				float temp_output_7_0_g270 = _FogHeightEnd;
				half FogHeightMask193 = saturate( ( ( temp_output_238_0 - temp_output_7_0_g270 ) / ( _FogHeightStart - temp_output_7_0_g270 ) ) );
				float temp_output_115_0 = ( FogDistanceMask186 * FogHeightMask193 );
				float simplePerlin3D225 = snoise( ( ( WorldPosition144 * _NoiseScale ) + ( -_NoiseSpeed * _Time.y ) ) );
				float temp_output_7_0_g272 = -1.0;
				float depthToLinear350 = LinearEyeDepth(Depth209);
				float temp_output_7_0_g218 = _NoiseDistanceEnd;
				half NoiseDistanceMask353 = saturate( ( ( depthToLinear350 - temp_output_7_0_g218 ) / ( 0.0 - temp_output_7_0_g218 ) ) );
				float lerpResult344 = lerp( 1.0 , ( ( simplePerlin3D225 - temp_output_7_0_g272 ) / ( 1.0 - temp_output_7_0_g272 ) ) , ( NoiseDistanceMask353 * _NoiseIntensity ));
				half NoiseSimplex3D234 = lerpResult344;
				#if defined(_FOGNOISEMODE_OFF)
				float staticSwitch242 = temp_output_115_0;
				#elif defined(_FOGNOISEMODE_PROCEDURAL3D)
				float staticSwitch242 = ( temp_output_115_0 * NoiseSimplex3D234 );
				#else
				float staticSwitch242 = temp_output_115_0;
				#endif
				float3 normalizeResult262 = normalize( WorldPosition144 );
				float temp_output_7_0_g274 = _SkyboxFogHeight;
				float lerpResult267 = lerp( saturate( ( ( abs( (normalizeResult262).y ) - temp_output_7_0_g274 ) / ( 0.0 - temp_output_7_0_g274 ) ) ) , 1.0 , _SkyboxFogFill);
				half SkyboxFogHeightMask197 = lerpResult267;
				half SkyboxMask83 = ( 1.0 - ceil( Depth209 ) );
				float lerpResult85 = lerp( staticSwitch242 , SkyboxFogHeightMask197 , SkyboxMask83);
				float temp_output_277_0 = ( lerpResult85 * _FogIntensity );
				float4 appendResult384 = (float4((staticSwitch241).rgb , temp_output_277_0));
				
				
				finalColor = appendResult384;
				return finalColor;
			}
			ENDCG
		}
	}
	
	
	
}
/*ASEBEGIN
Version=16805
1927;29;1906;1014;6474.463;3465.108;5.752423;True;False
Node;AmplifyShaderEditor.ScreenPosInputsNode;207;-3328,-2688;Float;False;0;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;159;-3072,-2688;Float;True;Global;_CameraDepthTexture;_CameraDepthTexture;1;0;Create;True;0;0;False;0;None;;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RegisterLocalVarNode;209;-2432,-2688;Half;False;Depth;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;325;-3328,-1616;Float;False;209;Depth;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;264;-3072,-1664;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.StaticSwitch;265;-2880,-1632;Float;False;Property;_Keyword1;Keyword 1;3;0;Fetch;True;0;0;False;0;0;0;0;False;UNITY_REVERSED_Z;Toggle;2;Key0;Key1;Fetch;False;9;1;FLOAT;0;False;0;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT;0;False;7;FLOAT;0;False;8;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ScreenPosInputsNode;310;-3328,-1792;Float;False;0;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.FunctionNode;322;-2560,-1600;Float;False;const;-1;;169;5b64729fb717c5f49a1bc2dab81d5e1c;1,3,2;0;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;304;-2560,-1792;Float;False;FLOAT3;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;1;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;321;-2560,-1680;Half;False;Constant;_Float6;Float 6;15;0;Create;True;0;0;False;0;2;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ScaleAndOffsetNode;317;-2304,-1792;Float;False;3;0;FLOAT3;0,0,0;False;1;FLOAT;2;False;2;FLOAT;-1;False;1;FLOAT3;0
Node;AmplifyShaderEditor.FunctionNode;320;-2304,-1664;Float;False;const;-1;;170;5b64729fb717c5f49a1bc2dab81d5e1c;1,3,1;0;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;307;-2048,-1664;Float;False;FLOAT4;4;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;1;False;1;FLOAT4;0
Node;AmplifyShaderEditor.CameraProjectionNode;316;-2048,-1792;Float;False;unity_CameraInvProjection;0;1;FLOAT4x4;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;303;-1728,-1792;Float;False;2;2;0;FLOAT4x4;0,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1;False;1;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SwizzleNode;327;-1536,-1712;Float;False;FLOAT;3;1;2;3;1;0;FLOAT4;0,0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SwizzleNode;326;-1536,-1792;Float;False;FLOAT3;0;1;2;3;1;0;FLOAT4;0,0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.Vector3Node;323;-1344,-1680;Half;False;Constant;_Vector1;Vector 1;9;0;Create;True;0;0;False;0;1,1,-1;0,0,0;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SimpleDivideOpNode;311;-1344,-1792;Float;False;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;312;-1152,-1792;Float;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT3;1,1,-1;False;1;FLOAT3;0
Node;AmplifyShaderEditor.FunctionNode;324;-1152,-1664;Float;False;const;-1;;171;5b64729fb717c5f49a1bc2dab81d5e1c;1,3,1;0;1;FLOAT;0
Node;AmplifyShaderEditor.CameraToWorldMatrix;318;-960,-1792;Float;False;0;1;FLOAT4x4;0
Node;AmplifyShaderEditor.DynamicAppendNode;314;-960,-1696;Float;False;FLOAT4;4;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;1;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;313;-704,-1792;Float;False;2;2;0;FLOAT4x4;0,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1;False;1;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SwizzleNode;328;-544,-1792;Float;False;FLOAT3;0;1;2;3;1;0;FLOAT4;0,0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;347;-3328,2048;Float;False;209;Depth;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;144;-320,-1792;Float;False;WorldPosition;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.FunctionNode;355;-3328,2208;Float;False;const;-1;;216;5b64729fb717c5f49a1bc2dab81d5e1c;1,3,0;0;1;FLOAT;0
Node;AmplifyShaderEditor.Vector3Node;227;-3328,1536;Half;False;Property;_NoiseSpeed;Noise Speed;16;0;Create;True;0;0;False;0;0.5,0.5,0;1,0,0;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.RangedFloatNode;349;-3328,2128;Half;False;Property;_NoiseDistanceEnd;Noise Distance End;12;0;Create;True;0;0;False;0;10;263.28;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.LinearDepthNode;350;-3072,2048;Float;False;0;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.NegateNode;337;-3136,1536;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;230;-3328,1376;Half;False;Property;_NoiseScale;Noise Scale;15;0;Create;True;0;0;False;0;6;22.9;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;217;-3328,1280;Float;False;144;WorldPosition;1;0;OBJECT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.FunctionNode;351;-2816,2048;Float;False;Remap To 0-1;-1;;218;5eda8a2bb94ebef4ab0e43d19291cd8b;0;3;6;FLOAT;0;False;7;FLOAT;0;False;8;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleTimeNode;224;-3328,1696;Float;False;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;191;-3328,768;Float;False;144;WorldPosition;1;0;OBJECT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;189;-3328,256;Float;False;144;WorldPosition;1;0;OBJECT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;222;-3008,1280;Float;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;573;-3328,-384;Float;False;144;WorldPosition;1;0;OBJECT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WorldSpaceCameraPos;575;-3328,-304;Float;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SaturateNode;352;-2560,2048;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;221;-3008,1536;Float;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;107;-3328,-64;Half;False;Property;_FogDistanceEnd;Fog Distance End;5;0;Create;True;0;0;False;0;30;57.48;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.NormalizeNode;262;-3104,768;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SwizzleNode;238;-3104,256;Float;False;FLOAT;1;1;2;3;1;0;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DistanceOpNode;574;-3008,-384;Float;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;106;-3328,-144;Half;False;Property;_FogDistanceStart;Fog Distance Start;4;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;74;-2688,368;Half;False;Property;_FogHeightEnd;Fog Height End;7;0;Create;True;0;0;False;0;0;5.84;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;353;-2368,2048;Half;False;NoiseDistanceMask;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;223;-2688,1280;Float;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;103;-2688,448;Half;False;Property;_FogHeightStart;Fog Height Start;6;0;Create;True;0;0;False;0;5;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;179;-3328,-1152;Float;False;144;WorldPosition;1;0;OBJECT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WorldSpaceCameraPos;139;-3328,-1056;Float;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SwizzleNode;263;-2944,768;Float;False;FLOAT;1;1;2;3;1;0;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;345;-2304,1552;Half;False;Property;_NoiseIntensity;Noise Intensity;11;0;Create;True;0;0;False;0;0.5;0.54;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;195;-2432,256;Float;False;Remap To 0-1;-1;;270;5eda8a2bb94ebef4ab0e43d19291cd8b;0;3;6;FLOAT;0;False;7;FLOAT;0;False;8;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;187;-2816,-384;Float;False;Remap To 0-1;-1;;269;5eda8a2bb94ebef4ab0e43d19291cd8b;0;3;6;FLOAT;0;False;7;FLOAT;0;False;8;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;358;-2304,1472;Float;False;353;NoiseDistanceMask;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;141;-3008,-1152;Float;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.NoiseGeneratorNode;225;-2560,1280;Float;False;Simplex3D;1;0;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;232;-2560,1376;Float;False;const;-1;;268;5b64729fb717c5f49a1bc2dab81d5e1c;1,3,2;0;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;233;-2560,1456;Float;False;const;-1;;267;5b64729fb717c5f49a1bc2dab81d5e1c;1,3,1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;274;-2176,256;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;273;-2560,-384;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;365;-1968,1488;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.Vector3Node;625;-3328,-896;Half;False;Global;_FogDirectionalDirection;_FogDirectionalDirection;24;0;Create;True;0;0;False;0;0,0,0;-0.8093418,0.3029979,-0.5031483;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.FunctionNode;346;-2304,1280;Float;False;const;-1;;273;5b64729fb717c5f49a1bc2dab81d5e1c;1,3,1;0;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;201;-2816,896;Float;False;const;-1;;271;5b64729fb717c5f49a1bc2dab81d5e1c;1,3,0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;88;-3328,896;Half;False;Property;_SkyboxFogHeight;Skybox Fog Height;8;0;Create;True;0;0;False;0;1;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;226;-2304,1344;Float;False;Remap To 0-1;-1;;272;5eda8a2bb94ebef4ab0e43d19291cd8b;0;3;6;FLOAT;0;False;7;FLOAT;-1;False;8;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.AbsOpNode;279;-2800,768;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.NormalizeNode;131;-2816,-1152;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.LerpOp;344;-1728,1280;Float;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DotProductOpNode;119;-2624,-1152;Float;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;186;-2400,-384;Half;False;FogDistanceMask;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;193;-2000,256;Half;False;FogHeightMask;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;200;-2624,768;Float;False;Remap To 0-1;-1;;274;5eda8a2bb94ebef4ab0e43d19291cd8b;0;3;6;FLOAT;0;False;7;FLOAT;0;False;8;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;177;-3328,-2176;Float;False;209;Depth;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;182;-2624,-1024;Float;False;const;-1;;276;5b64729fb717c5f49a1bc2dab81d5e1c;1,3,2;0;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;183;-2624,-960;Float;False;const;-1;;275;5b64729fb717c5f49a1bc2dab81d5e1c;1,3,1;0;1;FLOAT;0
Node;AmplifyShaderEditor.CeilOpNode;571;-3072,-2176;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;184;-2432,-1152;Float;False;Remap To 0-1;-1;;277;5eda8a2bb94ebef4ab0e43d19291cd8b;0;3;6;FLOAT;0;False;7;FLOAT;0;False;8;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;234;-1536,1280;Half;False;NoiseSimplex3D;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;268;-2448,832;Float;False;const;-1;;278;5b64729fb717c5f49a1bc2dab81d5e1c;1,3,1;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;188;-3328,-3840;Float;False;186;FogDistanceMask;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;275;-2304,768;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;266;-2624,960;Half;False;Property;_SkyboxFogFill;Skybox Fog Fill;9;0;Create;True;0;0;False;0;0;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;194;-3328,-3760;Float;False;193;FogHeightMask;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;115;-3072,-3840;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;267;-2112,848;Float;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;250;-3328,-3680;Float;False;234;NoiseSimplex3D;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;134;-2192,-1152;Half;False;DirectionalMask;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;572;-2944,-2176;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;276;-2880,-3712;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;197;-1920,768;Half;False;SkyboxFogHeightMask;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;137;-3328,-4352;Half;False;Property;_FogColor;Fog Color;3;0;Create;True;0;0;False;0;0.4411765,0.722515,1,0;0.4604461,1,0.4411756,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.GetLocalVarNode;136;-3328,-3968;Float;False;134;DirectionalMask;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;102;-3328,-4160;Half;False;Property;_FogDirectionalColor;Fog Directional Color;10;0;Create;True;0;0;False;0;1,0.6300203,0.1617647,0;1,0.6300203,0.1617639,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RegisterLocalVarNode;83;-2624,-2176;Half;False;SkyboxMask;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;198;-2688,-3712;Float;False;197;SkyboxFogHeightMask;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;175;-2624,-3632;Float;False;83;SkyboxMask;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;135;-3008,-4224;Float;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.StaticSwitch;242;-2688,-3840;Float;False;Property;_FogNoiseMode;Fog Noise Mode;14;0;Create;True;0;0;False;0;1;0;0;False;;KeywordEnum;2;Off;Procedural3D;Create;False;9;1;FLOAT;0;False;0;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT;0;False;7;FLOAT;0;False;8;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;278;-2304,-3712;Half;False;Property;_FogIntensity;Fog Intensity;2;1;[HideInInspector];Create;True;0;0;False;0;1;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.StaticSwitch;241;-2816,-4352;Float;False;Property;_FogDirectionalMode;Fog Directional Mode;11;0;Create;True;0;0;False;0;1;0;0;False;;KeywordEnum;2;Off;On;Create;False;9;1;COLOR;0,0,0,0;False;0;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;3;COLOR;0,0,0,0;False;4;COLOR;0,0,0,0;False;5;COLOR;0,0,0,0;False;6;COLOR;0,0,0,0;False;7;COLOR;0,0,0,0;False;8;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;85;-2304,-3840;Float;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;277;-1984,-3840;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SwizzleNode;385;-2528,-4352;Float;False;FLOAT3;0;1;2;3;1;0;COLOR;0,0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.FunctionNode;591;-2752,3456;Float;False;Remap To 0-1;-1;;280;5eda8a2bb94ebef4ab0e43d19291cd8b;0;3;6;FLOAT;0;False;7;FLOAT;0;False;8;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DistanceOpNode;587;-2944,3456;Float;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.StaticSwitch;341;-2717.484,-2677.926;Float;False;Property;_RENDERPIPELINE;RENDERPIPELINE;9;0;Create;True;0;0;False;0;0;0;0;False;;KeywordEnum;2;STD_LW;HD;Create;False;9;1;FLOAT;0;False;0;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT;0;False;7;FLOAT;0;False;8;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;548;-2752,3072;Float;False;Remap To 0-1;-1;;281;5eda8a2bb94ebef4ab0e43d19291cd8b;0;3;6;FLOAT;0;False;7;FLOAT;0;False;8;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;547;-2560,3072;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WorldSpaceCameraPos;583;-3328,3584;Float;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.FunctionNode;600;-3328,384;Float;False;Object Position;-1;;279;b9555b68a3d67c54f91597a05086026a;0;0;4;FLOAT3;7;FLOAT;0;FLOAT;4;FLOAT;5
Node;AmplifyShaderEditor.OneMinusNode;582;-2432,2688;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SwitchByFaceNode;596;-2944,2688;Float;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WorldPosInputsNode;584;-3328,3456;Float;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.WorldPosInputsNode;544;-3328,3072;Float;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.StaticSwitch;605;-1536,-3840;Float;False;Property;_Keyword0;Keyword 0;14;0;Create;True;0;0;False;0;0;0;0;True;;KeywordEnum;2;Off;On;Reference;570;False;9;1;FLOAT;0;False;0;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT;0;False;7;FLOAT;0;False;8;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;589;-2560,3456;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;586;-2944,3552;Float;False;const;-1;;282;5b64729fb717c5f49a1bc2dab81d5e1c;1,3,0;0;1;FLOAT;0
Node;AmplifyShaderEditor.FresnelNode;480;-2688,2688;Float;False;Standard;WorldNormal;ViewDir;True;5;0;FLOAT3;0,0,1;False;4;FLOAT3;0,0,0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;576;-2944,3168;Float;False;const;-1;;283;5b64729fb717c5f49a1bc2dab81d5e1c;1,3,0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;623;-1152,3072;Float;False;Constant;_Float0;Float 0;25;0;Create;True;0;0;False;0;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;481;-2288,2688;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;549;-2944,3264;Half;False;Property;_VolumeExitFade;Volume Exit Fade;18;0;Create;True;0;0;False;0;0;20;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.IntNode;595;-2896,-4736;Float;False;Property;_ZTest;_ZTest;20;0;Create;True;0;0;True;0;0;0;0;1;INT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;602;-2944,384;Float;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;384;-1280,-4352;Float;False;FLOAT4;4;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.GetLocalVarNode;609;-2304,-3504;Float;False;577;VolumeExitFade;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;608;-1984,-3552;Float;False;3;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;590;-2944,3648;Half;False;Property;_VolumeDistanceFade;Volume Distance Fade;19;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ConditionalIfNode;622;-896,2688;Float;False;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;611;-2304,-3584;Float;False;604;VolumeEdgeFade;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.StaticSwitch;570;-2688,256;Float;False;Property;_IsGlobalMode;IsGlobalMode;13;0;Create;True;0;0;False;0;0;0;0;True;;KeywordEnum;2;Off;On;Create;False;9;1;FLOAT;0;False;0;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT;0;False;7;FLOAT;0;False;8;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;624;-1152,3152;Float;False;Constant;_Float1;Float 1;25;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.CustomExpressionNode;150;-3072,-2496;Float;False;SampleCameraDepth( UV )$;1;False;1;True;UV;FLOAT2;0,0;In;;Float;False;CallSampleCameraDepth;True;False;0;1;0;FLOAT2;0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DistanceOpNode;620;-1216,2944;Float;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;607;-1760,-3680;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DistanceOpNode;616;-1216,2688;Float;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;610;-2304,-3424;Float;False;603;VolumeDistanceFade;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WorldSpaceCameraPos;617;-1536,2768;Float;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.RegisterLocalVarNode;604;-2128,2688;Half;False;VolumeEdgeFade;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WorldSpaceCameraPos;545;-3328,3200;Float;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.RegisterLocalVarNode;603;-2416,3456;Half;False;VolumeDistanceFade;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WorldPosInputsNode;621;-1536,2944;Float;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.RangedFloatNode;483;-2944,2816;Half;False;Property;_VolumeShapeFade;Volume Shape Fade;17;0;Create;True;0;0;False;0;0;4;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.NegateNode;599;-3120,2752;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.DistanceOpNode;543;-2944,3072;Float;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;558;-3328,-4736;Half;False;Property;_ADSSimpleLitGrass;< ADS Simple Lit Grass >;0;0;Create;True;0;0;True;1;BBanner(Volume Height Fog);1;0;1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.WorldSpaceCameraPos;618;-1536,3088;Float;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.RegisterLocalVarNode;577;-2400,3072;Half;False;VolumeExitFade;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.IntNode;594;-3056,-4736;Float;False;Property;_ZWrite;_ZWrite;21;0;Create;True;0;0;True;0;0;0;0;1;INT;0
Node;AmplifyShaderEditor.GetLocalVarNode;615;-1536,2688;Float;False;144;WorldPosition;1;0;OBJECT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WorldNormalVector;597;-3328,2688;Float;False;False;1;0;FLOAT3;0,0,1;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.TemplateMultiPassMasterNode;383;-1088,-4352;Float;False;True;2;Float;;0;1;Hidden/BOXOPHOBIC/Height Fog Volume;0770190933193b94aaa3065e307002fa;True;Unlit;0;0;Unlit;2;True;2;5;False;-1;10;False;-1;0;5;False;-1;10;False;-1;True;0;False;-1;0;False;-1;True;False;True;2;False;-1;True;True;True;True;True;0;False;-1;True;False;255;False;-1;255;False;-1;255;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;True;2;False;594;True;7;False;595;True;False;0;False;500;0;False;500;True;2;RenderType=Overlay=RenderType;Queue=Overlay=Queue=0;True;2;0;False;False;False;False;False;False;False;False;False;True;0;False;0;;0;0;Standard;1;Vertex Position,InvertActionOnDeselection;1;0;1;True;False;2;0;FLOAT4;0,0,0,0;False;1;FLOAT3;0,0,0;False;0
Node;AmplifyShaderEditor.CommentaryNode;354;-3328,1920;Float;False;1346.805;100;Noise Distance Mask;0;;0.7529412,0.7529412,0.7529412,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;174;-3328,-1920;Float;False;3199.657;100;World Position from Depth;0;;0,1,0,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;612;-3328,-4480;Float;False;2427.742;100;Final Pass;0;;0.497,1,0,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;196;-3328,128;Float;False;1535.592;100;Fog Height;0;;0,0.5882353,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;235;-3328,1152;Float;False;2004.805;100;Noise;0;;0.7529412,0.7529412,0.7529412,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;215;-3328,-2816;Float;False;1089.231;100;Depth Texture;0;;0.5,0.5,0.5,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;178;-3328,-2304;Float;False;896.071;100;Skybox Mask;0;;1,0.234,0,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;557;-3328,-4864;Float;False;559.27;100;Drawers / Settings;0;;1,0.4980392,0,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;199;-3328,640;Float;False;1664.083;100;Skybox Fog Height;0;;0.5220588,0.8032007,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;190;-3328,-512;Float;False;1153.136;100;Fog Distance;0;;0,0.5882353,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;180;-3328,-1280;Float;False;1344.071;100;Directional Light Support;0;;1,0.634,0.1617647,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;606;-3328,2560;Float;False;2585;100;UNUSED;0;;0.9448276,0,0,1;0;0
WireConnection;159;1;207;0
WireConnection;209;0;159;1
WireConnection;264;0;325;0
WireConnection;265;1;325;0
WireConnection;265;0;264;0
WireConnection;304;0;310;1
WireConnection;304;1;310;2
WireConnection;304;2;265;0
WireConnection;317;0;304;0
WireConnection;317;1;321;0
WireConnection;317;2;322;0
WireConnection;307;0;317;0
WireConnection;307;3;320;0
WireConnection;303;0;316;0
WireConnection;303;1;307;0
WireConnection;327;0;303;0
WireConnection;326;0;303;0
WireConnection;311;0;326;0
WireConnection;311;1;327;0
WireConnection;312;0;311;0
WireConnection;312;1;323;0
WireConnection;314;0;312;0
WireConnection;314;3;324;0
WireConnection;313;0;318;0
WireConnection;313;1;314;0
WireConnection;328;0;313;0
WireConnection;144;0;328;0
WireConnection;350;0;347;0
WireConnection;337;0;227;0
WireConnection;351;6;350;0
WireConnection;351;7;349;0
WireConnection;351;8;355;0
WireConnection;222;0;217;0
WireConnection;222;1;230;0
WireConnection;352;0;351;0
WireConnection;221;0;337;0
WireConnection;221;1;224;0
WireConnection;262;0;191;0
WireConnection;238;0;189;0
WireConnection;574;0;573;0
WireConnection;574;1;575;0
WireConnection;353;0;352;0
WireConnection;223;0;222;0
WireConnection;223;1;221;0
WireConnection;263;0;262;0
WireConnection;195;6;238;0
WireConnection;195;7;74;0
WireConnection;195;8;103;0
WireConnection;187;6;574;0
WireConnection;187;7;106;0
WireConnection;187;8;107;0
WireConnection;141;0;179;0
WireConnection;141;1;139;0
WireConnection;225;0;223;0
WireConnection;274;0;195;0
WireConnection;273;0;187;0
WireConnection;365;0;358;0
WireConnection;365;1;345;0
WireConnection;226;6;225;0
WireConnection;226;7;232;0
WireConnection;226;8;233;0
WireConnection;279;0;263;0
WireConnection;131;0;141;0
WireConnection;344;0;346;0
WireConnection;344;1;226;0
WireConnection;344;2;365;0
WireConnection;119;0;131;0
WireConnection;119;1;625;0
WireConnection;186;0;273;0
WireConnection;193;0;274;0
WireConnection;200;6;279;0
WireConnection;200;7;88;0
WireConnection;200;8;201;0
WireConnection;571;0;177;0
WireConnection;184;6;119;0
WireConnection;184;7;182;0
WireConnection;184;8;183;0
WireConnection;234;0;344;0
WireConnection;275;0;200;0
WireConnection;115;0;188;0
WireConnection;115;1;194;0
WireConnection;267;0;275;0
WireConnection;267;1;268;0
WireConnection;267;2;266;0
WireConnection;134;0;184;0
WireConnection;572;0;571;0
WireConnection;276;0;115;0
WireConnection;276;1;250;0
WireConnection;197;0;267;0
WireConnection;83;0;572;0
WireConnection;135;0;137;0
WireConnection;135;1;102;0
WireConnection;135;2;136;0
WireConnection;242;1;115;0
WireConnection;242;0;276;0
WireConnection;241;1;137;0
WireConnection;241;0;135;0
WireConnection;85;0;242;0
WireConnection;85;1;198;0
WireConnection;85;2;175;0
WireConnection;277;0;85;0
WireConnection;277;1;278;0
WireConnection;385;0;241;0
WireConnection;591;6;587;0
WireConnection;591;7;590;0
WireConnection;591;8;586;0
WireConnection;587;0;584;0
WireConnection;587;1;583;0
WireConnection;341;1;159;1
WireConnection;341;0;150;0
WireConnection;548;6;543;0
WireConnection;548;7;576;0
WireConnection;548;8;549;0
WireConnection;547;0;548;0
WireConnection;582;0;480;0
WireConnection;596;0;597;0
WireConnection;596;1;599;0
WireConnection;605;1;277;0
WireConnection;605;0;607;0
WireConnection;589;0;591;0
WireConnection;480;0;596;0
WireConnection;480;2;483;0
WireConnection;481;0;582;0
WireConnection;602;0;238;0
WireConnection;602;1;600;4
WireConnection;384;0;385;0
WireConnection;384;3;277;0
WireConnection;608;0;611;0
WireConnection;608;1;609;0
WireConnection;608;2;610;0
WireConnection;622;0;616;0
WireConnection;622;1;620;0
WireConnection;622;2;623;0
WireConnection;622;4;624;0
WireConnection;570;1;238;0
WireConnection;570;0;602;0
WireConnection;150;0;207;0
WireConnection;620;0;621;0
WireConnection;620;1;618;0
WireConnection;607;0;277;0
WireConnection;607;1;608;0
WireConnection;616;0;615;0
WireConnection;616;1;617;0
WireConnection;604;0;481;0
WireConnection;603;0;589;0
WireConnection;599;0;597;0
WireConnection;543;0;544;0
WireConnection;543;1;545;0
WireConnection;577;0;547;0
WireConnection;383;0;384;0
ASEEND*/
//CHKSM=83CB817119A4EC6319839655A1BF1ABE9855EEDF
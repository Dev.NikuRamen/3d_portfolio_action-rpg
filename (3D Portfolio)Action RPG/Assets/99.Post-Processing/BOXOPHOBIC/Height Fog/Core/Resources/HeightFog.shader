// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Hidden/BOXOPHOBIC/HeightFog"
{
	Properties
	{
		_FogColor("Fog Color", Color) = (1,1,1,0)
		_FogDistanceStart("Fog Distance Start", Float) = 0
		_NoiseDistanceEnd("Noise Distance End", Float) = 0
		_FogDistanceEnd("Fog Distance End", Float) = 0
		_FogHeightStart("Fog Height Start", Float) = 0
		_FogHeightEnd("Fog Height End", Float) = 0
		_SkyboxFogHeight("Skybox Fog Height", Range( 0 , 1)) = 0.0001
		_SkyboxFogFill("Skybox Fog Fill", Range( 0 , 1)) = 0.8371459
		_SkyboxFogIntensity("Skybox Fog Intensity", Range( 0 , 1)) = 1
		[KeywordEnum(Off,On)] _DirectionalMode("Directional Mode", Float) = 0
		_FogDirectionalColor("Fog Directional Color", Color) = (1,1,1,0)
		[KeywordEnum(Off,Procedural3D)] _NoiseMode("Noise Mode", Float) = 0
		_NoiseScale("Noise Scale", Float) = 0.2
		_NoiseSpeed("Noise Speed", Vector) = (0,0,0,0)
		_FogIntensity("Fog Intensity", Range( 0 , 1)) = 1
		_NoiseIntensity("Noise Intensity", Range( 0 , 1)) = 0
		_FogDirectionalDirection("Fog Directional Direction", Vector) = (0,0,0,0)
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
	}

	SubShader
	{
		Cull Off
		ZWrite Off
		ZTest Always
		
		Pass
		{
			CGPROGRAM

			#pragma vertex Vert
			#pragma fragment Frag
			#pragma target 3.0

			#include "UnityCG.cginc"
			#include "UnityShaderVariables.cginc"
			#pragma multi_compile _DIRECTIONALMODE_OFF _DIRECTIONALMODE_ON
			#pragma multi_compile _NOISEMODE_OFF _NOISEMODE_PROCEDURAL3D

		
			struct ASEAttributesDefault
			{
				float3 vertex : POSITION;
				float2 texcoord : TEXCOORD0;
				
			};

			struct ASEVaryingsDefault
			{
				float4 vertex : SV_POSITION;
				float2 texcoord : TEXCOORD0;
				float2 texcoordStereo : TEXCOORD1;
			#if STEREO_INSTANCING_ENABLED
				uint stereoTargetEyeIndex : SV_RenderTargetArrayIndex;
			#endif
				
			};

			uniform sampler2D _MainTex;
			uniform half4 _MainTex_TexelSize;
			uniform half4 _MainTex_ST;
			
			uniform half4 _FogColor;
			uniform half4 _FogDirectionalColor;
			uniform sampler2D _CameraDepthTexture;
			uniform half3 _FogDirectionalDirection;
			uniform half _FogDistanceStart;
			uniform half _FogDistanceEnd;
			uniform half _FogHeightEnd;
			uniform half _FogHeightStart;
			uniform half _NoiseScale;
			uniform half3 _NoiseSpeed;
			uniform half _NoiseDistanceEnd;
			uniform half _NoiseIntensity;
			uniform half _SkyboxFogHeight;
			uniform half _SkyboxFogFill;
			uniform half _SkyboxFogIntensity;
			uniform half _FogIntensity;

			float3 mod3D289( float3 x ) { return x - floor( x / 289.0 ) * 289.0; }
			float4 mod3D289( float4 x ) { return x - floor( x / 289.0 ) * 289.0; }
			float4 permute( float4 x ) { return mod3D289( ( x * 34.0 + 1.0 ) * x ); }
			float4 taylorInvSqrt( float4 r ) { return 1.79284291400159 - r * 0.85373472095314; }
			float snoise( float3 v )
			{
				const float2 C = float2( 1.0 / 6.0, 1.0 / 3.0 );
				float3 i = floor( v + dot( v, C.yyy ) );
				float3 x0 = v - i + dot( i, C.xxx );
				float3 g = step( x0.yzx, x0.xyz );
				float3 l = 1.0 - g;
				float3 i1 = min( g.xyz, l.zxy );
				float3 i2 = max( g.xyz, l.zxy );
				float3 x1 = x0 - i1 + C.xxx;
				float3 x2 = x0 - i2 + C.yyy;
				float3 x3 = x0 - 0.5;
				i = mod3D289( i);
				float4 p = permute( permute( permute( i.z + float4( 0.0, i1.z, i2.z, 1.0 ) ) + i.y + float4( 0.0, i1.y, i2.y, 1.0 ) ) + i.x + float4( 0.0, i1.x, i2.x, 1.0 ) );
				float4 j = p - 49.0 * floor( p / 49.0 );  // mod(p,7*7)
				float4 x_ = floor( j / 7.0 );
				float4 y_ = floor( j - 7.0 * x_ );  // mod(j,N)
				float4 x = ( x_ * 2.0 + 0.5 ) / 7.0 - 1.0;
				float4 y = ( y_ * 2.0 + 0.5 ) / 7.0 - 1.0;
				float4 h = 1.0 - abs( x ) - abs( y );
				float4 b0 = float4( x.xy, y.xy );
				float4 b1 = float4( x.zw, y.zw );
				float4 s0 = floor( b0 ) * 2.0 + 1.0;
				float4 s1 = floor( b1 ) * 2.0 + 1.0;
				float4 sh = -step( h, 0.0 );
				float4 a0 = b0.xzyw + s0.xzyw * sh.xxyy;
				float4 a1 = b1.xzyw + s1.xzyw * sh.zzww;
				float3 g0 = float3( a0.xy, h.x );
				float3 g1 = float3( a0.zw, h.y );
				float3 g2 = float3( a1.xy, h.z );
				float3 g3 = float3( a1.zw, h.w );
				float4 norm = taylorInvSqrt( float4( dot( g0, g0 ), dot( g1, g1 ), dot( g2, g2 ), dot( g3, g3 ) ) );
				g0 *= norm.x;
				g1 *= norm.y;
				g2 *= norm.z;
				g3 *= norm.w;
				float4 m = max( 0.6 - float4( dot( x0, x0 ), dot( x1, x1 ), dot( x2, x2 ), dot( x3, x3 ) ), 0.0 );
				m = m* m;
				m = m* m;
				float4 px = float4( dot( x0, g0 ), dot( x1, g1 ), dot( x2, g2 ), dot( x3, g3 ) );
				return 42.0 * dot( m, px);
			}
			

			float2 TransformTriangleVertexToUV (float2 vertex)
			{
				float2 uv = (vertex + 1.0) * 0.5;
				return uv;
			}

			ASEVaryingsDefault Vert( ASEAttributesDefault v  )
			{
				ASEVaryingsDefault o;
				o.vertex = float4(v.vertex.xy, 0.0, 1.0);
				o.texcoord = TransformTriangleVertexToUV (v.vertex.xy);
#if UNITY_UV_STARTS_AT_TOP
				o.texcoord = o.texcoord * float2(1.0, -1.0) + float2(0.0, 1.0);
#endif
				o.texcoordStereo = TransformStereoScreenSpaceTex (o.texcoord, 1.0);

				v.texcoord = o.texcoordStereo;
				float4 ase_ppsScreenPosVertexNorm = float4(o.texcoordStereo,0,1);

				

				return o;
			}

			float4 Frag (ASEVaryingsDefault i  ) : SV_Target
			{
				float4 ase_ppsScreenPosFragNorm = float4(i.texcoordStereo,0,1);

				float2 uv_MainTex = i.texcoord.xy * _MainTex_ST.xy + _MainTex_ST.zw;
				half3 MainTex202 = (tex2D( _MainTex, uv_MainTex )).rgb;
				float4 tex2DNode159 = tex2D( _CameraDepthTexture, ase_ppsScreenPosFragNorm.xy );
				half Depth209 = tex2DNode159.r;
				#ifdef UNITY_REVERSED_Z
				float staticSwitch265 = ( 1.0 - Depth209 );
				#else
				float staticSwitch265 = Depth209;
				#endif
				float3 appendResult304 = (float3(ase_ppsScreenPosFragNorm.x , ase_ppsScreenPosFragNorm.y , staticSwitch265));
				float4 appendResult307 = (float4((appendResult304*2.0 + -1.0) , 1.0));
				float4 temp_output_303_0 = mul( unity_CameraInvProjection, appendResult307 );
				float4 appendResult314 = (float4(( ( (temp_output_303_0).xyz / (temp_output_303_0).w ) * half3(1,1,-1) ) , 1.0));
				float3 WorldPosition144 = (mul( unity_CameraToWorld, appendResult314 )).xyz;
				float3 normalizeResult131 = normalize( ( WorldPosition144 - _WorldSpaceCameraPos ) );
				float dotResult119 = dot( normalizeResult131 , _FogDirectionalDirection );
				float temp_output_7_0_g204 = -1.0;
				half DirectionalMask134 = ( ( dotResult119 - temp_output_7_0_g204 ) / ( 1.0 - temp_output_7_0_g204 ) );
				float4 lerpResult135 = lerp( _FogColor , _FogDirectionalColor , DirectionalMask134);
				#if defined(_DIRECTIONALMODE_OFF)
				float4 staticSwitch241 = _FogColor;
				#elif defined(_DIRECTIONALMODE_ON)
				float4 staticSwitch241 = lerpResult135;
				#else
				float4 staticSwitch241 = _FogColor;
				#endif
				float depthToLinear237 = LinearEyeDepth(Depth209);
				float temp_output_7_0_g196 = _FogDistanceStart;
				half FogDistanceMask186 = saturate( ( ( depthToLinear237 - temp_output_7_0_g196 ) / ( _FogDistanceEnd - temp_output_7_0_g196 ) ) );
				float temp_output_7_0_g194 = _FogHeightEnd;
				half FogHeightMask193 = saturate( ( ( (WorldPosition144).y - temp_output_7_0_g194 ) / ( _FogHeightStart - temp_output_7_0_g194 ) ) );
				float temp_output_115_0 = ( FogDistanceMask186 * FogHeightMask193 );
				float simplePerlin3D225 = snoise( ( ( WorldPosition144 * _NoiseScale ) + ( -_NoiseSpeed * _Time.y ) ) );
				float temp_output_7_0_g199 = -1.0;
				float depthToLinear350 = LinearEyeDepth(Depth209);
				float temp_output_7_0_g174 = _NoiseDistanceEnd;
				half NoiseDistanceMask353 = saturate( ( ( depthToLinear350 - temp_output_7_0_g174 ) / ( 0.0 - temp_output_7_0_g174 ) ) );
				float lerpResult344 = lerp( 1.0 , ( ( simplePerlin3D225 - temp_output_7_0_g199 ) / ( 1.0 - temp_output_7_0_g199 ) ) , ( NoiseDistanceMask353 * _NoiseIntensity ));
				half NoiseSimplex3D234 = lerpResult344;
				#if defined(_NOISEMODE_OFF)
				float staticSwitch242 = temp_output_115_0;
				#elif defined(_NOISEMODE_PROCEDURAL3D)
				float staticSwitch242 = ( temp_output_115_0 * NoiseSimplex3D234 );
				#else
				float staticSwitch242 = temp_output_115_0;
				#endif
				float3 normalizeResult262 = normalize( WorldPosition144 );
				float temp_output_7_0_g198 = _SkyboxFogHeight;
				float lerpResult267 = lerp( saturate( ( ( abs( (normalizeResult262).y ) - temp_output_7_0_g198 ) / ( 0.0 - temp_output_7_0_g198 ) ) ) , 1.0 , _SkyboxFogFill);
				float lerpResult367 = lerp( 0.0 , lerpResult267 , _SkyboxFogIntensity);
				half SkyboxFogHeightMask197 = lerpResult367;
				float depthToLinear208 = Linear01Depth(Depth209);
				half SkyboxMask83 = floor( depthToLinear208 );
				float lerpResult85 = lerp( staticSwitch242 , SkyboxFogHeightMask197 , SkyboxMask83);
				float4 lerpResult92 = lerp( float4( MainTex202 , 0.0 ) , staticSwitch241 , ( lerpResult85 * _FogIntensity ));
				

				float4 color = float4( (lerpResult92).rgb , 0.0 );
				
				return color;
			}
			ENDCG
		}
	}
	CustomEditor "ASEMaterialInspector"
	
	
}
/*ASEBEGIN
Version=16805
1927;29;1906;1014;4122.039;2882.612;2.661488;True;False
Node;AmplifyShaderEditor.ScreenPosInputsNode;207;-3328,-2688;Float;False;0;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;159;-3072,-2688;Float;True;Global;_CameraDepthTexture;_CameraDepthTexture;0;0;Create;True;0;0;False;0;None;;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RegisterLocalVarNode;209;-2432,-2688;Half;False;Depth;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;325;-3328,-1616;Float;False;209;Depth;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;264;-3072,-1664;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ScreenPosInputsNode;310;-3328,-1792;Float;False;0;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.StaticSwitch;265;-2880,-1632;Float;False;Property;_Keyword1;Keyword 1;3;0;Fetch;True;0;0;False;0;0;0;0;False;UNITY_REVERSED_Z;Toggle;2;Key0;Key1;Fetch;False;9;1;FLOAT;0;False;0;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT;0;False;7;FLOAT;0;False;8;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;322;-2560,-1600;Float;False;const;-1;;169;5b64729fb717c5f49a1bc2dab81d5e1c;1,3,2;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;321;-2560,-1680;Half;False;Constant;_Float6;Float 6;15;0;Create;True;0;0;False;0;2;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;304;-2560,-1792;Float;False;FLOAT3;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;1;False;1;FLOAT3;0
Node;AmplifyShaderEditor.FunctionNode;320;-2304,-1664;Float;False;const;-1;;170;5b64729fb717c5f49a1bc2dab81d5e1c;1,3,1;0;1;FLOAT;0
Node;AmplifyShaderEditor.ScaleAndOffsetNode;317;-2304,-1792;Float;False;3;0;FLOAT3;0,0,0;False;1;FLOAT;2;False;2;FLOAT;-1;False;1;FLOAT3;0
Node;AmplifyShaderEditor.DynamicAppendNode;307;-2048,-1664;Float;False;FLOAT4;4;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;1;False;1;FLOAT4;0
Node;AmplifyShaderEditor.CameraProjectionNode;316;-2048,-1792;Float;False;unity_CameraInvProjection;0;1;FLOAT4x4;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;303;-1728,-1792;Float;False;2;2;0;FLOAT4x4;0,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1;False;1;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SwizzleNode;327;-1536,-1712;Float;False;FLOAT;3;1;2;3;1;0;FLOAT4;0,0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SwizzleNode;326;-1536,-1792;Float;False;FLOAT3;0;1;2;3;1;0;FLOAT4;0,0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleDivideOpNode;311;-1344,-1792;Float;False;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.Vector3Node;323;-1344,-1680;Half;False;Constant;_Vector1;Vector 1;9;0;Create;True;0;0;False;0;1,1,-1;0,0,0;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.FunctionNode;324;-1152,-1664;Float;False;const;-1;;171;5b64729fb717c5f49a1bc2dab81d5e1c;1,3,1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;312;-1152,-1792;Float;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT3;1,1,-1;False;1;FLOAT3;0
Node;AmplifyShaderEditor.DynamicAppendNode;314;-960,-1696;Float;False;FLOAT4;4;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;1;False;1;FLOAT4;0
Node;AmplifyShaderEditor.CameraToWorldMatrix;318;-960,-1792;Float;False;0;1;FLOAT4x4;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;313;-704,-1792;Float;False;2;2;0;FLOAT4x4;0,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1;False;1;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SwizzleNode;328;-544,-1792;Float;False;FLOAT3;0;1;2;3;1;0;FLOAT4;0,0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;347;-3328,2048;Float;False;209;Depth;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;144;-320,-1792;Float;False;WorldPosition;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.LinearDepthNode;350;-3072,2048;Float;False;0;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.Vector3Node;227;-3328,1600;Half;False;Property;_NoiseSpeed;Noise Speed;14;0;Create;True;0;0;False;0;0,0,0;0,0,0;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.RangedFloatNode;349;-3328,2128;Half;False;Property;_NoiseDistanceEnd;Noise Distance End;3;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;355;-3328,2208;Float;False;const;-1;;172;5b64729fb717c5f49a1bc2dab81d5e1c;1,3,0;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;217;-3328,1408;Float;False;144;WorldPosition;1;0;OBJECT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.NegateNode;337;-3152,1600;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleTimeNode;224;-3168,1760;Float;False;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;230;-3328,1504;Half;False;Property;_NoiseScale;Noise Scale;13;0;Create;True;0;0;False;0;0.2;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;351;-2816,2048;Float;False;Remap To 0-1;-1;;174;5eda8a2bb94ebef4ab0e43d19291cd8b;0;3;6;FLOAT;0;False;7;FLOAT;0;False;8;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;191;-3328,768;Float;False;144;WorldPosition;1;0;OBJECT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;236;-3328,-384;Float;False;209;Depth;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;189;-3328,128;Float;False;144;WorldPosition;1;0;OBJECT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.NormalizeNode;262;-3104,768;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;221;-3008,1600;Float;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SaturateNode;352;-2560,2048;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;222;-3008,1408;Float;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;74;-3328,240;Half;False;Property;_FogHeightEnd;Fog Height End;6;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SwizzleNode;238;-3072,128;Float;False;FLOAT;1;1;2;3;1;0;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;107;-3328,-192;Half;False;Property;_FogDistanceEnd;Fog Distance End;4;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SwizzleNode;263;-2944,768;Float;False;FLOAT;1;1;2;3;1;0;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;103;-3328,320;Half;False;Property;_FogHeightStart;Fog Height Start;5;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;106;-3328,-272;Half;False;Property;_FogDistanceStart;Fog Distance Start;2;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.LinearDepthNode;237;-3072,-384;Float;False;0;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;353;-2368,2048;Half;False;NoiseDistanceMask;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;223;-2688,1408;Float;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;345;-2304,1680;Half;False;Property;_NoiseIntensity;Noise Intensity;16;0;Create;True;0;0;False;0;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;187;-2816,-384;Float;False;Remap To 0-1;-1;;196;5eda8a2bb94ebef4ab0e43d19291cd8b;0;3;6;FLOAT;0;False;7;FLOAT;0;False;8;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;179;-3328,-1152;Float;False;144;WorldPosition;1;0;OBJECT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;358;-2304,1600;Float;False;353;NoiseDistanceMask;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WorldSpaceCameraPos;139;-3328,-1056;Float;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.NoiseGeneratorNode;225;-2560,1408;Float;False;Simplex3D;1;0;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;201;-2816,896;Float;False;const;-1;;193;5b64729fb717c5f49a1bc2dab81d5e1c;1,3,0;0;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;233;-2560,1584;Float;False;const;-1;;191;5b64729fb717c5f49a1bc2dab81d5e1c;1,3,1;0;1;FLOAT;0
Node;AmplifyShaderEditor.AbsOpNode;279;-2800,768;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;232;-2560,1504;Float;False;const;-1;;195;5b64729fb717c5f49a1bc2dab81d5e1c;1,3,2;0;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;195;-2816,128;Float;False;Remap To 0-1;-1;;194;5eda8a2bb94ebef4ab0e43d19291cd8b;0;3;6;FLOAT;0;False;7;FLOAT;0;False;8;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;88;-3328,896;Half;False;Property;_SkyboxFogHeight;Skybox Fog Height;7;0;Create;True;0;0;False;0;0.0001;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;200;-2624,768;Float;False;Remap To 0-1;-1;;198;5eda8a2bb94ebef4ab0e43d19291cd8b;0;3;6;FLOAT;0;False;7;FLOAT;0;False;8;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;274;-2560,128;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;226;-2304,1472;Float;False;Remap To 0-1;-1;;199;5eda8a2bb94ebef4ab0e43d19291cd8b;0;3;6;FLOAT;0;False;7;FLOAT;-1;False;8;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;346;-2304,1408;Float;False;const;-1;;197;5b64729fb717c5f49a1bc2dab81d5e1c;1,3,1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;141;-3008,-1152;Float;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;365;-1968,1616;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;273;-2560,-384;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;344;-1728,1408;Float;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;193;-2384,128;Half;False;FogHeightMask;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;186;-2400,-384;Half;False;FogDistanceMask;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;177;-3328,-2176;Float;False;209;Depth;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.NormalizeNode;131;-2816,-1152;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.FunctionNode;268;-2448,832;Float;False;const;-1;;200;5b64729fb717c5f49a1bc2dab81d5e1c;1,3,1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;266;-2624,960;Half;False;Property;_SkyboxFogFill;Skybox Fog Fill;8;0;Create;True;0;0;False;0;0.8371459;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.Vector3Node;382;-3328,-896;Half;False;Property;_FogDirectionalDirection;Fog Directional Direction;17;0;Create;True;0;0;False;0;0,0,0;0,0,0;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SaturateNode;275;-2304,768;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;183;-2624,-960;Float;False;const;-1;;201;5b64729fb717c5f49a1bc2dab81d5e1c;1,3,1;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;194;-3328,-4272;Float;False;193;FogHeightMask;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;267;-2112,848;Float;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;182;-2624,-1024;Float;False;const;-1;;203;5b64729fb717c5f49a1bc2dab81d5e1c;1,3,2;0;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;366;-2112,768;Float;False;const;-1;;202;5b64729fb717c5f49a1bc2dab81d5e1c;1,3,0;0;1;FLOAT;0
Node;AmplifyShaderEditor.LinearDepthNode;208;-3072,-2176;Float;False;1;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DotProductOpNode;119;-2624,-1152;Float;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;368;-2112,960;Half;False;Property;_SkyboxFogIntensity;Skybox Fog Intensity;9;0;Create;True;0;0;False;0;1;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;188;-3328,-4352;Float;False;186;FogDistanceMask;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;234;-1536,1408;Half;False;NoiseSimplex3D;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TemplateShaderPropertyNode;99;-3328,-3200;Float;False;0;0;_MainTex;Pass;0;5;SAMPLER2D;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.LerpOp;367;-1792,768;Float;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;250;-3328,-4192;Float;False;234;NoiseSimplex3D;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;184;-2432,-1152;Float;False;Remap To 0-1;-1;;204;5eda8a2bb94ebef4ab0e43d19291cd8b;0;3;6;FLOAT;0;False;7;FLOAT;0;False;8;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;115;-3072,-4352;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.FloorOpNode;214;-2816,-2176;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;276;-2880,-4224;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;197;-1616,768;Half;False;SkyboxFogHeightMask;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;100;-3072,-3200;Float;True;Property;_MainTexInstance;MainTexInstance;3;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RegisterLocalVarNode;83;-2624,-2176;Half;False;SkyboxMask;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;134;-2192,-1152;Half;False;DirectionalMask;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;198;-2688,-4224;Float;False;197;SkyboxFogHeightMask;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;175;-2624,-4144;Float;False;83;SkyboxMask;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.StaticSwitch;242;-2688,-4352;Float;False;Property;_NoiseMode;Noise Mode;12;0;Create;True;0;0;False;0;1;0;0;True;;KeywordEnum;2;Off;Procedural3D;Create;False;9;1;FLOAT;0;False;0;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT;0;False;7;FLOAT;0;False;8;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SwizzleNode;205;-2752,-3200;Float;False;FLOAT3;0;1;2;3;1;0;COLOR;0,0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.ColorNode;102;-3328,-4672;Half;False;Property;_FogDirectionalColor;Fog Directional Color;11;0;Create;True;0;0;False;0;1,1,1,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;137;-3328,-4864;Half;False;Property;_FogColor;Fog Color;1;0;Create;True;0;0;False;0;1,1,1,0;1,1,1,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.GetLocalVarNode;136;-3328,-4480;Float;False;134;DirectionalMask;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;278;-2304,-4224;Half;False;Property;_FogIntensity;Fog Intensity;15;0;Create;True;0;0;False;0;1;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;85;-2304,-4352;Float;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;135;-3008,-4736;Float;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;202;-2560,-3200;Half;False;MainTex;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.StaticSwitch;241;-2816,-4864;Float;False;Property;_DirectionalMode;Directional Mode;10;0;Create;True;0;0;False;0;1;0;0;True;;KeywordEnum;2;Off;On;Create;False;9;1;COLOR;0,0,0,0;False;0;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;3;COLOR;0,0,0,0;False;4;COLOR;0,0,0,0;False;5;COLOR;0,0,0,0;False;6;COLOR;0,0,0,0;False;7;COLOR;0,0,0,0;False;8;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;204;-2048,-4992;Float;False;202;MainTex;1;0;OBJECT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;277;-1984,-4352;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;92;-1792,-4864;Float;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.CustomExpressionNode;150;-3072,-2496;Float;False;SampleCameraDepth( UV )$;1;False;1;True;UV;FLOAT2;0,0;In;;Float;False;CallSampleCameraDepth;True;False;0;1;0;FLOAT2;0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.StaticSwitch;341;-2717.484,-2677.926;Float;False;Property;_RENDERPIPELINE;RENDERPIPELINE;9;0;Create;True;0;0;False;0;0;0;0;False;;KeywordEnum;2;STD_LW;HD;Create;False;9;1;FLOAT;0;False;0;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT;0;False;7;FLOAT;0;False;8;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;228;-3328,1760;Half;False;Constant;_Float0;Float 0;8;0;Create;True;0;0;False;0;10;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SwizzleNode;143;-1600,-4864;Float;False;FLOAT3;0;1;2;3;1;0;COLOR;0,0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.TemplateMultiPassMasterNode;380;-1408,-4864;Float;False;True;2;Float;ASEMaterialInspector;0;2;Hidden/BOXOPHOBIC/HeightFog;32139be9c1eb75640a847f011acf3bcf;True;SubShader 0 Pass 0;0;0;SubShader 0 Pass 0;1;False;False;False;True;2;False;-1;False;False;True;2;False;-1;True;7;False;-1;False;False;False;0;False;False;False;False;False;False;False;False;False;False;True;2;0;;0;0;Standard;0;0;1;True;False;1;0;FLOAT4;0,0,0,0;False;0
Node;AmplifyShaderEditor.CommentaryNode;216;-3328,-3328;Float;False;963.6731;100;Main Texture;0;;0.7843137,1,0,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;174;-3328,-1920;Float;False;3199.657;100;World Position from Depth;0;;0,1,0,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;215;-3328,-2816;Float;False;1089.231;100;Depth Texture;0;;0.5,0.5,0.5,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;178;-3328,-2304;Float;False;896.071;100;Skybox Mask;0;;1,0.234,0,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;180;-3328,-1280;Float;False;1344.071;100;Directional Light Support;0;;1,0.634,0.1617647,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;190;-3328,-512;Float;False;1153.136;100;Fog Distance;0;;0,0.5882353,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;199;-3328,640;Float;False;1961.564;100;Skybox Fog Height;0;;0.5220588,0.8032007,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;196;-3328,0;Float;False;1153.592;100;Fog Height;0;;0,0.5882353,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;235;-3328,1280;Float;False;2004.805;100;Noise;0;;0.7529412,0.7529412,0.7529412,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;354;-3328,1920;Float;False;1346.805;100;Noise Distance Mask;0;;0.7529412,0.7529412,0.7529412,1;0;0
WireConnection;159;1;207;0
WireConnection;209;0;159;1
WireConnection;264;0;325;0
WireConnection;265;1;325;0
WireConnection;265;0;264;0
WireConnection;304;0;310;1
WireConnection;304;1;310;2
WireConnection;304;2;265;0
WireConnection;317;0;304;0
WireConnection;317;1;321;0
WireConnection;317;2;322;0
WireConnection;307;0;317;0
WireConnection;307;3;320;0
WireConnection;303;0;316;0
WireConnection;303;1;307;0
WireConnection;327;0;303;0
WireConnection;326;0;303;0
WireConnection;311;0;326;0
WireConnection;311;1;327;0
WireConnection;312;0;311;0
WireConnection;312;1;323;0
WireConnection;314;0;312;0
WireConnection;314;3;324;0
WireConnection;313;0;318;0
WireConnection;313;1;314;0
WireConnection;328;0;313;0
WireConnection;144;0;328;0
WireConnection;350;0;347;0
WireConnection;337;0;227;0
WireConnection;351;6;350;0
WireConnection;351;7;349;0
WireConnection;351;8;355;0
WireConnection;262;0;191;0
WireConnection;221;0;337;0
WireConnection;221;1;224;0
WireConnection;352;0;351;0
WireConnection;222;0;217;0
WireConnection;222;1;230;0
WireConnection;238;0;189;0
WireConnection;263;0;262;0
WireConnection;237;0;236;0
WireConnection;353;0;352;0
WireConnection;223;0;222;0
WireConnection;223;1;221;0
WireConnection;187;6;237;0
WireConnection;187;7;106;0
WireConnection;187;8;107;0
WireConnection;225;0;223;0
WireConnection;279;0;263;0
WireConnection;195;6;238;0
WireConnection;195;7;74;0
WireConnection;195;8;103;0
WireConnection;200;6;279;0
WireConnection;200;7;88;0
WireConnection;200;8;201;0
WireConnection;274;0;195;0
WireConnection;226;6;225;0
WireConnection;226;7;232;0
WireConnection;226;8;233;0
WireConnection;141;0;179;0
WireConnection;141;1;139;0
WireConnection;365;0;358;0
WireConnection;365;1;345;0
WireConnection;273;0;187;0
WireConnection;344;0;346;0
WireConnection;344;1;226;0
WireConnection;344;2;365;0
WireConnection;193;0;274;0
WireConnection;186;0;273;0
WireConnection;131;0;141;0
WireConnection;275;0;200;0
WireConnection;267;0;275;0
WireConnection;267;1;268;0
WireConnection;267;2;266;0
WireConnection;208;0;177;0
WireConnection;119;0;131;0
WireConnection;119;1;382;0
WireConnection;234;0;344;0
WireConnection;367;0;366;0
WireConnection;367;1;267;0
WireConnection;367;2;368;0
WireConnection;184;6;119;0
WireConnection;184;7;182;0
WireConnection;184;8;183;0
WireConnection;115;0;188;0
WireConnection;115;1;194;0
WireConnection;214;0;208;0
WireConnection;276;0;115;0
WireConnection;276;1;250;0
WireConnection;197;0;367;0
WireConnection;100;0;99;0
WireConnection;83;0;214;0
WireConnection;134;0;184;0
WireConnection;242;1;115;0
WireConnection;242;0;276;0
WireConnection;205;0;100;0
WireConnection;85;0;242;0
WireConnection;85;1;198;0
WireConnection;85;2;175;0
WireConnection;135;0;137;0
WireConnection;135;1;102;0
WireConnection;135;2;136;0
WireConnection;202;0;205;0
WireConnection;241;1;137;0
WireConnection;241;0;135;0
WireConnection;277;0;85;0
WireConnection;277;1;278;0
WireConnection;92;0;204;0
WireConnection;92;1;241;0
WireConnection;92;2;277;0
WireConnection;150;0;207;0
WireConnection;341;1;159;1
WireConnection;341;0;150;0
WireConnection;143;0;92;0
WireConnection;380;0;143;0
ASEEND*/
//CHKSM=117D4D716767706954ACFACF0240D335302CAD1F
﻿// Height Fog Post Processing
// Cristian Pop - https://boxophobic.com/

using UnityEngine;
using Boxophobic;

[ImageEffectAllowedInSceneView]
[ExecuteInEditMode]
[RequireComponent(typeof(MeshRenderer))]
[RequireComponent(typeof(MeshFilter))]
public class HeightFogGlobal : MonoBehaviour
{
    [BCategory("Update")]
    public bool categotyUpdate;

    public FogUpdateMode updateMode = FogUpdateMode.OnLoad;

    [BCategory("Fog")]
    public bool categotyFog;

    [Range(0f, 1f)]
    public float fogIntensity = 1.0f;
    [ColorUsage(false, true, 0f, 8f, 0f, 1f)]
    public Color fogColor = new Color(0.5f, 0.75f, 1.0f, 1.0f);

    public float fogDistanceStart = 0.0f;
    public float fogDistanceEnd = 30.0f;
    public float fogHeightStart = 0.0f;
    public float fogHeightEnd = 5.0f;

    [BCategory("Skybox")]
    public bool categotySkybox;

    [Range(0f, 1f)]
    public float skyboxFogHeight = 0.5f;
    [Range(0f, 1f)]
    public float skyboxFogFill = 0.0f;

    [BCategory("Directional")]
    public bool categotyDirectional;

    public FogDirectionalMode directionalMode = FogDirectionalMode.Off;
    public Transform directionalLight;
    [ColorUsage(false, true, 0f, 8f, 0f, 1f)]
    public Color directionalColor = new Color(1f, 0.75f, 0.5f, 1f);

    [BCategory("Noise")]
    public bool categotyNoise;

    public FogNoiseMode noiseMode = FogNoiseMode.Off;
    [Range(0f, 1f)]
    public float noiseIntensity = 1.0f;
    public float noiseDistanceEnd = 60.0f;
    public float noiseScale = 1.0f;
    public Vector3 noiseSpeed = new Vector3(0.0f, 0.0f, 0.0f);

    [HideInInspector]
    public Material masterMaterial;
    [HideInInspector]
    public Material globalMaterial;
    [HideInInspector]
    public Material overrideMaterial;
    [HideInInspector]
    public float overrideCamToVolumeDistance = 1.0f;
    [HideInInspector]
    public float overrideVolumeDistanceFade = 0.0f;

    private Camera cam;

    void Awake()
    {
        gameObject.name = "Height Fog Global";

        gameObject.transform.position = Vector3.zero;
        gameObject.transform.rotation = Quaternion.identity;

        GetCamera();

        if (cam != null)
        {
            SetSphereSize();
            SetSpherePosition();
            cam.depthTextureMode = DepthTextureMode.Depth;
        }        

        var sphereMeshGO = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        var spherMesh = sphereMeshGO.GetComponent<MeshFilter>().sharedMesh;
        DestroyImmediate(sphereMeshGO);

        gameObject.GetComponent<MeshFilter>().sharedMesh = spherMesh;

        globalMaterial = new Material(Shader.Find("Hidden/BOXOPHOBIC/Height Fog Volume"));
        globalMaterial.name = "Fog Global";
        globalMaterial.renderQueue = 3999;

        SetFogMaterial();

        masterMaterial = new Material(globalMaterial);
        masterMaterial.name = "Fog Master";

        overrideMaterial = new Material(globalMaterial);
        overrideMaterial.name = "Fog Override";

        gameObject.GetComponent<MeshRenderer>().sharedMaterial = masterMaterial;
    }

    void Update()
    {

        if (gameObject.name != "Height Fog Global")
        {
            gameObject.name = "Height Fog Global";
        }

        if (cam != null)
        {
            SetSphereSize();
            SetSpherePosition();
        }

        if (Application.isPlaying == false || updateMode == FogUpdateMode.Realtime)
        {
            SetFogMaterial();
        }

        if (overrideCamToVolumeDistance > overrideVolumeDistanceFade)
        {
            masterMaterial.CopyPropertiesFromMaterial(globalMaterial);
        }
        else if (overrideCamToVolumeDistance < overrideVolumeDistanceFade)
        {
            var lerp = 1 - (overrideCamToVolumeDistance / overrideVolumeDistanceFade);
            masterMaterial.Lerp(globalMaterial, overrideMaterial, lerp);
        }       
    }

    void SetFogMaterial()
    {
        //globalMaterial.DisableKeyword("_ISGLOBALMODE_ON");
        //globalMaterial.EnableKeyword("_ISGLOBALMODE_OFF");

        globalMaterial.SetFloat("_FogIntensity", fogIntensity);

        globalMaterial.SetColor("_FogColor", fogColor);
        globalMaterial.SetFloat("_FogDistanceStart", fogDistanceStart);
        globalMaterial.SetFloat("_FogDistanceEnd", fogDistanceEnd);
        globalMaterial.SetFloat("_FogHeightStart", fogHeightStart);
        globalMaterial.SetFloat("_FogHeightEnd", fogHeightEnd);

        globalMaterial.SetFloat("_SkyboxFogHeight", skyboxFogHeight);
        globalMaterial.SetFloat("_SkyboxFogFill", skyboxFogFill);

        if (directionalMode == FogDirectionalMode.On)
        {
            Shader.DisableKeyword("_FOGDIRECTIONALMODE_OFF");
            Shader.EnableKeyword("_FOGDIRECTIONALMODE_ON");

            globalMaterial.SetColor("_FogDirectionalColor", directionalColor);
        }
        else
        {
            Shader.DisableKeyword("_FOGDIRECTIONALMODE_ON");
            Shader.EnableKeyword("_FOGDIRECTIONALMODE_OFF");
        }

        if (directionalLight == null)
        {
            Shader.SetGlobalVector("_FogDirectionalDirection", new Vector3(0, 0, 0));
        }
        else
        {
            Shader.SetGlobalVector("_FogDirectionalDirection", -directionalLight.transform.forward);
        }

        if (noiseMode == FogNoiseMode.Procedural3D)
        {
            Shader.DisableKeyword("_FOGNOISEMODE_OFF");
            Shader.EnableKeyword("_FOGNOISEMODE_PROCEDURAL3D");

            globalMaterial.SetFloat("_NoiseIntensity", noiseIntensity);
            globalMaterial.SetFloat("_NoiseDistanceEnd", noiseDistanceEnd);
            globalMaterial.SetFloat("_NoiseScale", 1 / noiseScale);
            globalMaterial.SetVector("_NoiseSpeed", noiseSpeed);
        }
        else
        {
            Shader.DisableKeyword("_FOGNOISEMODE_PROCEDURAL3D");
            Shader.EnableKeyword("_FOGNOISEMODE_OFF");
        }
    }

    void GetCamera()
    {
        cam = null;

        if (Camera.current != null)
        {
            cam = Camera.current;
        }

        if (Camera.main != null)
        {
            cam = Camera.main;
        }
    }

    void SetSphereSize()
    {
        var cameraFar = cam.farClipPlane - 1;
        gameObject.transform.localScale = new Vector3(cameraFar, cameraFar, cameraFar);
    }

    void SetSpherePosition()
    {
        transform.position = cam.transform.position;
    }


}


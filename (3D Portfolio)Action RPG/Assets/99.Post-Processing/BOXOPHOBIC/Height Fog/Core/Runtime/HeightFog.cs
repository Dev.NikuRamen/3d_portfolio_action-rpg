#if UNITY_POST_PROCESSING_STACK_V2

using System;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;
using MinAttribute = UnityEngine.Rendering.PostProcessing.MinAttribute;

//public enum RenderPipelineMode
//{
//    Standard = 0,
//    Lightweight = 1,
//    HighDefinition = 2
//}

[Serializable]
public sealed class FogDirectionalModeParameter : ParameterOverride<FogDirectionalMode> { }

[Serializable]
public sealed class FogNoiseModeParameter : ParameterOverride<FogNoiseMode> { }

[Serializable]
[PostProcess(typeof(HeightFogPPSRenderer), PostProcessEvent.BeforeStack, "BOXOPHOBIC/Height Fog")]
public sealed class HeightFog : PostProcessEffectSettings
{
    //[Tooltip("Render Pipeline")]
    //public RenderPipelineModeParameter renderPipeline = new RenderPipelineModeParameter { value = RenderPipelineMode.Standard };
    [Range(0f, 1f), Tooltip("Fog Intensity")]
    public FloatParameter fogIntensity = new FloatParameter { value = 1.0f };
    [Tooltip("Fog Color")]
    public ColorParameter fogColor = new ColorParameter { value = new Color(0.0f, 0.5f, 1.0f, 1.0f) };

    [Tooltip("Fog Distance Start")]
    public FloatParameter fogDistanceStart = new FloatParameter { value = 0.0f };
    [Tooltip("Fog Distance End")]
    public FloatParameter fogDistanceEnd = new FloatParameter { value = 30.0f };
    [Tooltip("Fog Height Start")]
    public FloatParameter fogHeightStart = new FloatParameter { value = 0.0f };
    [Tooltip("Fog Height End")]
    public FloatParameter fogHeightEnd = new FloatParameter { value = 30.0f };

    [Range(0.0f, 1f), Tooltip("Skybox Fog Intensity")]
    public FloatParameter skyboxFogIntensity = new FloatParameter { value = 1.0f };
    [Range(0.0001f, 1f), Tooltip("Skybox Fog Height")]
    public FloatParameter skyboxFogHeight = new FloatParameter { value = 0.5f };
    [Range(0f, 1f), Tooltip("Skybox Fog Fill")]
    public FloatParameter skyboxFogFill = new FloatParameter { value = 0.0f };

    [Tooltip("Directional Mode")]
    public FogDirectionalModeParameter directionalMode = new FogDirectionalModeParameter { value = FogDirectionalMode.Off };
    [Tooltip("Directional Color")]
    public ColorParameter directionalColor = new ColorParameter { value = new Color(1f, 0.75f, 0.5f, 1f) };
    public Vector3Parameter directionalDir = new Vector3Parameter { value = new Vector3(0.0f, 0.0f, 0.0f) };

    [Tooltip("Noise Mode")]
    public FogNoiseModeParameter noiseMode = new FogNoiseModeParameter { value = FogNoiseMode.Off };
    [Range(0f, 1f), Tooltip("Noise Intensity")]
    public FloatParameter noiseIntensity = new FloatParameter { value = 1.0f };
    [Min(0.0f),Tooltip("Noise Distance End")]
    public FloatParameter noiseDistanceEnd = new FloatParameter { value = 60.0f };
    [Tooltip("Noise Scale")]
    public FloatParameter noiseScale = new FloatParameter { value = 1.0f };
    [Tooltip("Noise Speed")]
    public Vector3Parameter noiseSpeed = new Vector3Parameter { value = new Vector3(0.0f, 0.0f, 0.0f) };
}

public sealed class HeightFogPPSRenderer : PostProcessEffectRenderer<HeightFog>
{
    public Shader shader;
    GameObject directionalLight;
    
    public override void Init()
    {
        shader = Shader.Find("Hidden/BOXOPHOBIC/HeightFog");

        if (GameObject.Find("Directional Light"))
        {
            directionalLight = GameObject.Find("Directional Light");
        }
        else if (GameObject.Find("Directional light"))
        {
            directionalLight = GameObject.Find("Directional light");
        }
    }

    public override void Render(PostProcessRenderContext context)
    {
        var sheet = context.propertySheets.Get(shader);

        //if (settings.renderPipeline.value == RenderPipelineMode.Standard || settings.renderPipeline.value == RenderPipelineMode.Lightweight)
        //{
        //    sheet.DisableKeyword("_RENDERPIPELINE_HD");
        //    sheet.EnableKeyword("_RENDERPIPELINE_STD_LW");
        //}
        //else
        //{
        //    sheet.DisableKeyword("_RENDERPIPELINE_STD_LW");
        //    sheet.EnableKeyword("_RENDERPIPELINE_HD");
        //}

        sheet.properties.SetFloat("_FogIntensity", settings.fogIntensity);

        sheet.properties.SetColor("_FogColor", settings.fogColor);
        sheet.properties.SetFloat("_FogDistanceStart", settings.fogDistanceStart);
        sheet.properties.SetFloat("_FogDistanceEnd", settings.fogDistanceEnd);
        sheet.properties.SetFloat("_FogHeightStart", settings.fogHeightStart);
        sheet.properties.SetFloat("_FogHeightEnd", settings.fogHeightEnd);

        sheet.properties.SetFloat("_SkyboxFogIntensity", settings.skyboxFogIntensity);
        sheet.properties.SetFloat("_SkyboxFogHeight", settings.skyboxFogHeight);
        sheet.properties.SetFloat("_SkyboxFogFill", settings.skyboxFogFill);

        if (settings.directionalMode.value == FogDirectionalMode.On)
        {
            sheet.DisableKeyword("_DIRECTIONALMODE_OFF");
            sheet.EnableKeyword("_DIRECTIONALMODE_ON");
        }
        else
        {
            sheet.DisableKeyword("_DIRECTIONALMODE_ON");
            sheet.EnableKeyword("_DIRECTIONALMODE_OFF");
        }        

        sheet.properties.SetColor("_FogDirectionalColor", settings.directionalColor);

        if (directionalLight == null)
        {
            sheet.properties.SetVector("_FogDirectionalDirection", new Vector3(0, 0, 0));
        }
        else
        {
            sheet.properties.SetVector("_FogDirectionalDirection", -directionalLight.transform.forward);
        }        

        if (settings.noiseMode.value == FogNoiseMode.Procedural3D)
        {
            sheet.DisableKeyword("_NOISEMODE_OFF");
            sheet.EnableKeyword("_NOISEMODE_PROCEDURAL3D");
        }
        else
        {
            sheet.DisableKeyword("_NOISEMODE_PROCEDURAL3D");
            sheet.EnableKeyword("_NOISEMODE_OFF");
        }

        sheet.properties.SetFloat("_NoiseIntensity", settings.noiseIntensity);
        sheet.properties.SetFloat("_NoiseDistanceEnd", settings.noiseDistanceEnd);
        sheet.properties.SetFloat("_NoiseScale", 1 / settings.noiseScale);
        sheet.properties.SetVector("_NoiseSpeed", settings.noiseSpeed);
        context.command.BlitFullscreenTriangle(context.source, context.destination, sheet, 0);
    }

    public override DepthTextureMode GetCameraFlags()
    {
        return DepthTextureMode.Depth;
    }
}
#endif

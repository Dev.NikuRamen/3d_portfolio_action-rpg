//Height Fog Post Processing
//Cristian Pop - www.boxophobic.com

public enum FogUpdateMode
{
    OnLoad = 0,
    Realtime = 1
}

public enum FogShapeMode
{
    Sphere = 1,
    Box = 2
}

public enum FogHeightMode
{
    Planar = 1,
    Spherical = 2
}

public enum FogDirectionalMode
{
    Off = 0,
    On = 1
}

public enum FogNoiseMode
{
    Off = 0,
    Procedural3D = 3
}

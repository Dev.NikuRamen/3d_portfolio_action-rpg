﻿// Height Fog Post Processing
// Cristian Pop - https://boxophobic.com/

using UnityEngine;
using Boxophobic;

[ExecuteInEditMode]
[RequireComponent(typeof(BoxCollider))]
public class HeightFogOverride : MonoBehaviour
{
    [BCategory("Update")]
    public bool categotyUpdate;

    public FogUpdateMode updateMode = FogUpdateMode.OnLoad;

    [BCategory("Volume")]
    public bool categotyVolume;

    //public float volumeShapeFade = 1.0f;
    //public float volumeExitFade = 10.0f;
    public float volumeDistanceFade = 3.0f;
    [Range(0f, 1f)]
    public float volumeVisibility = 0.2f;

    [BCategory("Fog")]
    public bool categotyFog;

    [Range(0f, 1f)]
    public float fogIntensity = 1.0f;
    [ColorUsage(false, true, 0f, 8f, 0f, 1f)]
    public Color fogColor = new Color(0.0f, 1.0f, 0.0f, 1.0f);

    public float fogDistanceStart = 0.0f;
    public float fogDistanceEnd = 30.0f;
    public float fogHeightStart = 0.0f;
    public float fogHeightEnd = 5.0f;

    [BCategory("Skybox")]
    public bool categotySkybox;

    [Range(0f, 1f)]
    public float skyboxFogHeight = 0.5f;
    [Range(0f, 1f)]
    public float skyboxFogFill = 0.0f;

    [BCategory("Directional")]
    public bool categotyDirectional;

    //public FogDirectionalMode directionalMode = FogDirectionalMode.Off;
    //public Transform directionalLight;
    [ColorUsage(false, true, 0f, 8f, 0f, 1f)]
    public Color directionalColor = new Color(1f, 0.75f, 0.5f, 1f);

    [BCategory("Noise")]
    public bool categotyNoise;

    //public FogNoiseMode noiseMode = FogNoiseMode.Off;
    [Range(0f, 1f)]
    public float noiseIntensity = 1.0f;
    public float noiseDistanceEnd = 60.0f;
    public float noiseScale = 1.0f;
    public Vector3 noiseSpeed = new Vector3(0.0f, 0.0f, 0.0f);

    [HideInInspector]
    public bool firstTime = true;

    private Material overrideMaterial;
    private Collider volumeCollider;
    private GameObject globalFogGO;
    private Camera cam;
    private bool distanceSent = false;

    void Start()
    { 
        if (firstTime == true)
        {
            gameObject.name = "Height Fog Override";
            firstTime = false;
        }

        volumeCollider = GetComponent<Collider>();
        volumeCollider.isTrigger = true;

        if (GameObject.Find("Height Fog Global") != null)
        {
            globalFogGO = GameObject.Find("Height Fog Global");
        }

        overrideMaterial = new Material(Shader.Find("Hidden/BOXOPHOBIC/Height Fog Volume"));
        overrideMaterial.name = "Fog Override";

        SetFogMaterial();
    }

    void LateUpdate()
    {
        if (Application.isPlaying == false || updateMode == FogUpdateMode.Realtime)
        {
            SetFogMaterial();
        }

        GetCamera();

        if (cam != null)
        {
            Vector3 camPos = cam.transform.position;
            Vector3 closestPos = volumeCollider.ClosestPoint(camPos);

            float dist = Vector3.Distance(camPos, closestPos);

            if (dist > volumeDistanceFade && distanceSent == false)
            {
                globalFogGO.GetComponent<HeightFogGlobal>().overrideCamToVolumeDistance = Mathf.Infinity;
                distanceSent = true;
            }
            else if (dist < volumeDistanceFade)
            {
                globalFogGO.GetComponent<HeightFogGlobal>().overrideMaterial = overrideMaterial;
                globalFogGO.GetComponent<HeightFogGlobal>().overrideCamToVolumeDistance = dist;
                globalFogGO.GetComponent<HeightFogGlobal>().overrideVolumeDistanceFade = volumeDistanceFade;
                distanceSent = false;
            }
        }
    }

    void OnDrawGizmos()
    {
        Gizmos.color = new Color(fogColor.r, fogColor.g, fogColor.b, volumeVisibility);
        Gizmos.DrawCube(transform.position, new Vector3(transform.lossyScale.x, transform.lossyScale.y, transform.lossyScale.z));
        Gizmos.DrawCube(transform.position, new Vector3(transform.lossyScale.x + volumeDistanceFade, transform.lossyScale.y + volumeDistanceFade, transform.lossyScale.z + volumeDistanceFade));
    }

    void SetFogMaterial()
    {
        overrideMaterial.SetFloat("_FogIntensity", fogIntensity);

        overrideMaterial.SetColor("_FogColor", fogColor);
        overrideMaterial.SetFloat("_FogDistanceStart", fogDistanceStart);
        overrideMaterial.SetFloat("_FogDistanceEnd", fogDistanceEnd);
        overrideMaterial.SetFloat("_FogHeightStart", fogHeightStart);
        overrideMaterial.SetFloat("_FogHeightEnd", fogHeightEnd);

        overrideMaterial.SetFloat("_SkyboxFogHeight", skyboxFogHeight);
        overrideMaterial.SetFloat("_SkyboxFogFill", skyboxFogFill);

        overrideMaterial.SetColor("_FogDirectionalColor", directionalColor);

        overrideMaterial.SetFloat("_NoiseIntensity", noiseIntensity);
        overrideMaterial.SetFloat("_NoiseDistanceEnd", noiseDistanceEnd);
        overrideMaterial.SetFloat("_NoiseScale", 1 / noiseScale);
        overrideMaterial.SetVector("_NoiseSpeed", noiseSpeed);
    }

    void GetCamera()
    {
        cam = null;

        if (Camera.current != null)
        {
            cam = Camera.current;
        }

        if (Camera.main != null)
        {
            cam = Camera.main;
        }
    }
}


﻿// Height Fog Post Processing
// Cristian Pop - https://boxophobic.com/

using UnityEngine;
using UnityEditor;
using Boxophobic;

[CustomEditor(typeof(HeightFogOverride))]
public class HeightFogOverrideInspector : Editor
{
	//private ADSGlobalMotion targetScript;

    private string[] excludeProps = new string[] { "m_Script"};

    private Color guiColor;
    private string bannerText;
    private string helpURL;

    void OnEnable()
    {
        //targetScript = (ADSGlobalMotion)target;

        bannerText = "Height Fog Override";
        helpURL = "https://docs.google.com/document/d/18UzML_pgYjRns9tppT3317-Wk6bYB7NpV-oPz4XrFhY/edit#heading=h.l787mm4wnx1a";

        // Check if Light or Dark Unity Skin
        // Set the Banner and Logo Textures
        if (EditorGUIUtility.isProSkin) 
		{
            guiColor = new Color(0.5f, 0.9f, 0.2f);
        } 
		else 
		{
            guiColor = BConst.ColorDarkGray;
		}
	}

	public override void OnInspectorGUI()
    {
        BEditorGUI.DrawBanner(guiColor, bannerText, helpURL);        
        DrawInspector();
        //DrawWarnings ();
        BEditorGUI.DrawLogo();
	}

	void DrawInspector()
    {
		serializedObject.Update ();

        excludeProps = new string[] { "m_Script"};

        DrawPropertiesExcluding(serializedObject, excludeProps);

        serializedObject.ApplyModifiedProperties ();

		GUILayout.Space (20);
	} 

//	void DrawWarnings(){
//
//	}
}

#if UNITY_POST_PROCESSING_STACK_V2

using UnityEngine;
using Boxophobic;

using UnityEditor.Rendering.PostProcessing;
using UnityEditor;

//namespace UnityEditor.Rendering.PostProcessing
//{

[PostProcessEditor(typeof(HeightFog))]
public class BOXOPHOBICHeightFogEditor : PostProcessEffectEditor<HeightFog>
{
    //SerializedParameterOverride renderPipeline;
    SerializedParameterOverride fogIntensity;

    SerializedParameterOverride fogColor;
    SerializedParameterOverride fogDistanceStart;
    SerializedParameterOverride fogDistanceEnd;
    SerializedParameterOverride fogHeightStart;
    SerializedParameterOverride fogHeightEnd;

    SerializedParameterOverride skyboxFogIntensity;
    SerializedParameterOverride skyboxFogHeight;
    SerializedParameterOverride skyboxFogFill;

    SerializedParameterOverride directionalMode;
    SerializedParameterOverride directionalColor;

    SerializedParameterOverride noiseMode;
    SerializedParameterOverride noiseIntensity;
    SerializedParameterOverride noiseDistanceEnd;
    SerializedParameterOverride noiseScale;
    SerializedParameterOverride noiseSpeed;

    //BOXOPHOBICHeightFog heightFogScript;
    GameObject directionalLight;

    public override void OnEnable()
    {
        //heightFogScript = (BOXOPHOBICHeightFog)target;

        //renderPipeline = FindParameterOverride(x => x.renderPipeline);

        fogIntensity = FindParameterOverride(x => x.fogIntensity);
        fogColor = FindParameterOverride(x => x.fogColor);

        fogDistanceStart = FindParameterOverride(x => x.fogDistanceStart);
        fogDistanceEnd = FindParameterOverride(x => x.fogDistanceEnd);
        fogHeightStart = FindParameterOverride(x => x.fogHeightStart);
        fogHeightEnd = FindParameterOverride(x => x.fogHeightEnd);

        skyboxFogIntensity = FindParameterOverride(x => x.skyboxFogIntensity);
        skyboxFogHeight = FindParameterOverride(x => x.skyboxFogHeight);
        skyboxFogFill = FindParameterOverride(x => x.skyboxFogFill);

        directionalMode = FindParameterOverride(x => x.directionalMode);
        directionalColor = FindParameterOverride(x => x.directionalColor);

        noiseMode = FindParameterOverride(x => x.noiseMode);
        noiseIntensity = FindParameterOverride(x => x.noiseIntensity);
        noiseDistanceEnd = FindParameterOverride(x => x.noiseDistanceEnd);
        noiseScale = FindParameterOverride(x => x.noiseScale);
        noiseSpeed = FindParameterOverride(x => x.noiseSpeed);
    }

    public override void OnInspectorGUI()
    {
        //PropertyField(renderPipeline);            

        EditorGUILayout.HelpBox("The Height Fog Post Processing will be deprecated in the next update. Please add a Height Fog Global gameobject to your scene instead!", MessageType.Warning);

        PropertyField(fogIntensity);
        PropertyField(fogColor);

        GUILayout.Space(10);

        PropertyField(fogDistanceStart);
        PropertyField(fogDistanceEnd);

        GUILayout.Space(10);

        PropertyField(fogHeightStart);
        PropertyField(fogHeightEnd);

        GUILayout.Space(10);

        PropertyField(skyboxFogIntensity);
        PropertyField(skyboxFogHeight);
        PropertyField(skyboxFogFill);

        GUILayout.Space(10);

        int dMode = directionalMode.value.intValue;

        PropertyField(directionalMode);

        if (dMode == 1)
        {
            PropertyField(directionalColor);

            // Only executed in editor when the voulme is selected
            if (GameObject.Find("Directional Light"))
            {
                directionalLight = GameObject.Find("Directional Light");
            }
            else if (GameObject.Find("Directional light"))
            {
                directionalLight = GameObject.Find("Directional light");
            }

            if (directionalLight == null || directionalLight.activeInHierarchy == false)
            {
                EditorGUILayout.HelpBox("The Directional Light is missing or disabled in the scene. Please add or enable the Directional Light and press Play!", MessageType.Warning);
            }
        }

        GUILayout.Space(10);

        int nMode = noiseMode.value.intValue;

        PropertyField(noiseMode);

        if (nMode > 1)
        {
            PropertyField(noiseIntensity);
            PropertyField(noiseDistanceEnd);
            PropertyField(noiseScale);
            PropertyField(noiseSpeed);
        }

        GUILayout.Space(20);

        BEditorGUI.DrawLogo();

    }
}
#endif

Fantasy Loading Bars 1 for UGUI (v1.0)
June 22, 2016

Artist: EvilSystem (evilsystem.eu / evil-s.deviantart.com )
Developer: ChoMPi (chompibg@gmail.com)
Contact: evilsystem@duloclan.com 

The perfect solution for your game's loading screen. Designed to fit various art styles, you can choose the best fitting bar for your background art. To make it even better, we have 4 different fill colors for each bar. 

- 5 Loading Bars
- 4 Color variations of each bar.
- 20 Prefabs
- Demo Scene
- PSD File (Vector & Smart Objects)
- Mobile Ready

The demo scenes are setup to scale with the screen size relative to 2560x1440.
The images are sliced from PSD files scaled up to 200%.

Contact me if you need any assistance or have a question. 
With best regards, Evil.
